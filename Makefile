CC=gcc
STD=c
MAKE=gmake
CFLAGS=-Wall -Wextra -Werror -std=$(STD)99 -pedantic -I $(INCLUDE) -ggdb3
SRC=$(SRCPARSER) $(SRCLEXER) $(SRCRULES) $(SRCASTPRINTER) $(SRCSTR)\
$(SRCHIS) $(SRCEXEC) $(SRCOPT) $(SRCRDL) $(SRCENV) $(SRCBUILT) $(SRCPRPT)\
$(SRCJOBS) $(SRCEVALEXPR) $(SRCEXP)
SRCLEXER=$(addprefix src/lexer/, lexer.c lexertools.c)
SRCASTPRINTER=$(addprefix src/astprinter/, astprinter.c astgetstr.c)
SRCPARSER=$(addprefix src/parser/, ast2.c ast.c tokenize.c tokenize2.c)
SRCPRPT=$(addprefix src/prompt/, prompt.c prompt_exp.c)
SRCRULES=$(addprefix src/parser/rules/, check_and_or.c check_rule_case.c \
          check_case_clause.c check_case_item.c check_command.c \
          check_compound_list.c check_do_group.c check_element.c \
          check_else_clause.c check_rule_for.c check_funcdec.c check_input.c \
          check_list.c check_pipeline.c check_prefix.c check_redirection.c \
          check_rule_if.c check_shell_command.c check_simple_command.c \
          check_rule_until.c check_rule_while.c raise_error.c)
SRCENV=$(addprefix src/environ/, environ.c environ_vars.c environ_alias.c\
environ_env.c)
SRCSTR=$(addprefix src/str/, str_tools.c fnmatch.c)
SRCOPT=$(addprefix src/getopt/, my_getopt.c shopt.c)
SRCRDL=$(addprefix src/readline/, readline.c keypress.c ctrl_mod.c signal.c\
del.c complete.c)
SRCHIS=$(addprefix src/history/, history.c)
SRCEXEC=$(addprefix src/exec/, exec.c exec_and_or.c exec_pipeline.c\
exec_simple_commands.c exec_command.c exec_rule_if.c exec_rule_while.c\
exec_rule_until.c exec_shell_command.c exec_rule_for.c exec_else_clause.c\
exec_compound_list.c exec_do_group.c exec_rule_case.c exec_case_clause.c\
exec_case_item.c exec_funcdec.c redirections.c)
SRCBUILT=$(addprefix src/builtins/, builtin.c cd.c break.c echo.c exit.c\
alias.c history.c export.c jobs.c source.c continue.c shopt.c)
SRCJOBS=$(addprefix src/jobs/, utility.c jobs.c grounds.c status.c cont.c)
SRCEVALEXPR=$(addprefix src/evalexpr/, conversion.c eval.c evalexpr_lexer.c\
eval2.c fifo.c filo.c)
SRCEXP=$(addprefix src/expand/, expand.c expand_arith.c expand_tildes.c\
expand_backslash.c expand_path.c)
TARGET=42sh

ASTDOT=ast.dot
ASTPNG=ast.png

DOXYFILE=Doxyfile

INCLUDE=src/include/
LIBS=-lncurses -lm

all: $(TARGET)

$(TARGET):
	$(MAKE) -C build 42sh

mano: SRC+=src/main.c
mano: CFLAGS+=-O0
mano:
	$(CC) $(CFLAGS) $(SRC) -o $(TARGET) $(LIBS)

check: $(TARGET)
	cd check && python test.py

$(ASTPNG): $(ASTDOT)
	dot -Tpng -Gcharset=latin1 $(ASTDOT) > $(ASTPNG)
	feh $(ASTPNG)

doc: $(INCLUDE)
	doxygen $(DOXYFILE)

clean: clean_ast
	$(MAKE) -C build clean
	$(RM) -r doc/html doc/latex
	$(RM) digius_p-42sh.tar.bz2
distclean:
	$(RM) -r build

clean_ast:
	$(RM) $(ASTPNG) $(ASTDOT)

export:
	git archive HEAD --prefix=digius_p-42sh/ | bzip2 > digius_p-42sh.tar.bz2

.PHONY: clean clean_ast doc check $(TARGET)

#	rm -f $(TARGET) $(TARGET).core $(SRC:.c=.o) digius_p-42sh.tar.bz2
