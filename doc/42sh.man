.TH 42sh 1  "November 06, 2013" "version 0.1" "42sh MANUAL PAGE"
.SH NAME
.B 42sh
- command interpreter (shell) 
.SH SYNOPSIS
.B 42sh
[options] [file]
.SH DESCRIPTION
.B 42sh
is a command interpreter for the system. It follows the directives 
given in the SCL document, but the main reference is Bash with its
posix option, along with the 42sh subject.
.PP
.B 42sh
is a shell that reads line from the terminal, interprets them, and
generally executes other commands.
.PP
If no arguments are present and if the standard input of the shell
is connected to a terminal, the shell is considered an interactive
shell. An interactive shell generally prompts before each command
and handles errors.
.PP
The options are as follow :
.HP
.B -c 
.I string
.RS
.RS
If the -c option is present, then commands are read from string. If
there are arguments after the string, they are assigned to the
positional parameters, starting with $0.
.RE
.RE
.HP
.B [-+]O [
.I shopt option
.B ]
.HP
.RS
.RS
.I shopt option
is one of the shell options accepted by the
.B shopt
builtin. If
.I shopt option
is present,
.B -O
sets the value of that option;
.B +O
unsets it. If
.I shopt option
is not supplied, the name and values of the shell options accepted
by
.B shopt
are printed on the standard output. If the invocation option is
.B +O
, the ouput is displayed in a format that may be reused as input.
.RE
.RE
.B --norc
.RS
.RS
Do not read and execute the personal initialization file if the
shell is interactive. This option is on by default if the shell is
invoked as
.B 42sh
.RE
.RE
.B --ast-print
.RS
.RS
Activate the AST printer.
.RE
.RE
.B --version
.RS
.RS
Prints the current
.B 42sh
version on the standard output and exits.
.SH EXIT STATUS
The exit status of an executed command is the value returned by the
.I waitpid
system call or equivalent function. Exit statuses fall between 0 and
255, though, as explained below, the shell may use values above 125
specially. Exit statuses from shell builtins and compound commands
are also limited to this range. Under certain circumstances the shell
will use special values to indicate specific failure modes.
.PP
For the shell's purpose, a command which exits with a zero exit status
has succeeded. An exit status of zero indicates success. A non-zero
exit status indicates failure. When a command terminates on a fatal signal
.I N
,
.B 42sh
uses the value of 128 +
.I N
as the exit status.
.PP
If a command is not found, the child process created to execute it returns
a status of 127. If a command is found but it is not executable, the return
status is 126.
.PP
If a command fails because of an error during expansion or redirection, the
exit status is greater than zero.
.PP
Shell builtin commands return a status of 0 (
.I true
) if successful, and non-zero (
.I false
) if an error occurs while they execute. All builtins return an exit status
of 2 to indicate incorrect usage.
.PP
.B 42sh
itself returns the exit status of the last command executed, unless a syntax
error occurs, in which case it exits with a non-zero value.
.SH AUTHORS
Pierre-Olivier Di Giussepe (digius_p)
.br
Thomas Tsakiris (tsakir_t)
.br
Kevin Waser (waser_k)
.br
Hatim-Pierre Fazileabasse (fazile_h)
.br
Tanguy Dupont de Dinechin (dupont_t)
