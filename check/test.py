#import modules
import imp
import os
import string
import subprocess
from subprocess import Popen, PIPE
import argparse
import signal

class Alarm(Exception):
    pass

def alarm_handler(signum, frame):
    raise Alarm

parser = argparse.ArgumentParser(description='Do some test. Remember, your memory is short and test are important.')
parser.add_argument('-c', '--category', action='store_true', help='Display the categories and the percentage of successful tests for each of these categories without displaying the related tests.')
parser.add_argument('-f', '--final', action='store_true' , help='Display the percentage of successful tests for all the tests only.')
parser.add_argument('-n', '--number', action='store_true' , help='Display the number of successful tests on the total number of tests (instead of a percentage).')
prange = parser.add_mutually_exclusive_group()
prange.add_argument('-a', '--all', action='store_true', help='Execute the test suite on all categories (default option if none is chosen).')
prange.add_argument('-e', '--select', nargs='+' , metavar='<Categories>', help='Execute the test suite on the categories passed in argument only.', choices=os.listdir("."))
args = parser.parse_args()
#print args.accumulate(args.integers)

_CURRENT_DIR = '.'


class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


def check_integrity(test, context):
    passed = 1
    try:
        context.desc
    except :
        print "|- " + bcolors.FAIL + "/!\\ missing variable `desc` in " + test + bcolors.ENDC
        passed = 0

    try:
        context.test
    except :
        print "|- " + bcolors.FAIL + "/!\\ missing variable `test` in " + test + bcolors.ENDC
        passed = 0

    return passed;

#end of check_integrity()

def run_test(curr_dir, file_or_dir, test):
    res = 0;
    f = open(os.path.join(curr_dir, file_or_dir, test))
    current_test = imp.load_source('current_test', file_or_dir + "_" + test + ".py", f)
    if check_integrity(test, current_test):
        signal.signal(signal.SIGALRM, alarm_handler)
        signal.alarm(3)  # 4 seconde
        pipe = Popen(["../42sh", "-c", current_test.test], stdout=PIPE, stderr=PIPE)
        try:
            text, err = pipe.communicate()
            exp_return = pipe.returncode
            signal.alarm(0)  # reset the alarm
        except Alarm:
            print bcolors.FAIL + "|- " + test, current_test.desc + " : Timeout" + bcolors.ENDC
            pipe.terminate()
            return 0
        err2 = ""
        try:
            result = current_test.expected == text or current_test.expected == text[:-1];
        except :
            pipe2 = Popen(["bash", "--posix", "-c",  current_test.test], stdout=PIPE, stderr=PIPE)
            text2, err2 = pipe2.communicate()
            current_test.expected = text2
            result = text == text2 or text2 == text[:-1]
            exp_return = pipe2.returncode
        returnc = pipe.returncode == exp_return
        if result & returnc:
            print bcolors.GREEN + "|- " + test + " " + current_test.desc + bcolors.ENDC
            res = 1
        else:
          print bcolors.FAIL + "|- " + test + " " + current_test.desc
          if not result:
            print "|-> Standard Outputs not equals"
            print "|--> expected : '" + current_test.expected.replace('\n', '\\n') + "'" + "\n|--> received : '" + text.replace('\n', '\\n') + "'" + bcolors.ENDC
          if not returnc:
            print bcolors.FAIL + "|-> Exit status not equals"
            print "|--> expected : '" + str(exp_return) + "'\n|--> received : '" + str(pipe.returncode) + "'" + bcolors.ENDC
          if err:
            print bcolors.WARNING + "|--> 42sh error : " + err.rstrip(err[-1:]) + bcolors.ENDC
          if err2:
            print bcolors.WARNING + "|--> bash error : " + err2.rstrip(err2[-1:]) + bcolors.ENDC
    del current_test.desc
    del current_test.test
    del current_test.expected
    del current_test
    f.close()
    return res

#end of run_test()


def run_tests(curr_dir):
    test_num = 0
    test_succ = 0
    if args.select:
        dirs = args.select
    else:
        dirs = os.listdir(curr_dir)
    for file_or_dir in dirs:

        cat_num = 0
        cat_succ = 0
        if os.path.isdir(file_or_dir):
            print ".Category : [" + bcolors.BLUE, file_or_dir, bcolors.ENDC + "]"
            for test in os.listdir(os.path.join(curr_dir, file_or_dir)):
                if test.startswith("test"):
                    cat_num += 1
                    cat_succ += run_test(curr_dir, file_or_dir, test)
            if not args.final and cat_num > 0:
                if args.number:
                    print "`---------- [" + bcolors.BLUE, file_or_dir, bcolors.ENDC + "] Score :", cat_succ, "/", cat_num
                else:
                    print "`---------- [" + bcolors.BLUE, file_or_dir, bcolors.ENDC + "] Score :", (cat_succ / float(cat_num)) * 100, "%" 
            else:
                print "`---------- [" + bcolors.BLUE, file_or_dir, bcolors.ENDC + "]"
            print ""

        test_num += cat_num
        test_succ += cat_succ
    if test_num > 0:
        if args.number:
            print "Total Score :", test_succ, "/", test_num
        else:
            print "Total Score :", (test_succ / float(test_num)) * 100, "%"
        print ""

#end of run_tests()

def main():

    if args.final:
        print "final option"
    if args.number:
        print "number option"
    if args.all:
        print "all option"
    base_dir = _CURRENT_DIR

    run_tests(base_dir)

#end of main()


if __name__ == '__main__':
    main()
