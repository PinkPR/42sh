#include "fifo.h"

struct fifo *g_input = NULL;

struct fifo *fifo_add(struct fifo *fifo, enum token token, int value)
{
  struct fifo *new = malloc(sizeof (struct fifo));
  new->token = token;
  new->value = value;
  new->next = fifo;

  return new;
}

static void *copy(void *ptr)
{
  return ptr;
}

struct fifo *fifo_pop(struct fifo **fifo)
{
  struct fifo *tmp = NULL;
  struct fifo *tmp2 = NULL;
  tmp = *fifo;

  if (tmp && tmp->next)
  {
    while (tmp->next->next)
      tmp = copy(tmp->next);

    tmp2 = copy(tmp->next);
    tmp->next = 0;

    return tmp2;
  }
  else
  {
    tmp = copy(*fifo);
    *fifo = 0;
    return tmp;
  }
}
