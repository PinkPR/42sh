#include "filo.h"

struct filo *filo_add(struct filo *filo, enum token token, int value)
{
  struct filo *new = malloc(sizeof (struct filo));
  new->token = token;
  new->value = value;

  new->next = filo;

  return new;

}

struct filo *filo_pop(struct filo **filo)
{
  struct filo *new = *filo;

  if (*filo == NULL)
    return NULL;

  *filo = (*filo)->next;
  return new;
}
