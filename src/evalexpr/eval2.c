#include <stdio.h>
#include <string.h>
#include <math.h>

#include "conversion.h"
#include "tokens.h"
#include "fifo.h"
#include "filo.h"

struct int_couple *g_couple = NULL;

static void else_body(struct filo **tmp2, struct filo **tmp3,
                      struct filo **stack2)
{
  *tmp2 = filo_pop(stack2);
  g_couple->nb2 = (*tmp2)->value;
  if (*stack2 && (*stack2)->token != NOT && (*stack2)->token != BNOT)
  {
    *tmp3 = filo_pop(stack2);
    g_couple->nb1 = ((*tmp3)->value);
    free(*tmp3);
  }
  else
    g_couple->nb1 = 0;
}

void output_loop(struct fifo **tmp1, struct filo **tmp2,
                 struct filo **tmp3, struct filo **stack2)
{
  g_couple = calloc(sizeof (struct int_couple), 1);
  while (g_output)
  {
    *tmp1 = fifo_pop(&g_output);
    if ((*tmp1)->token == NUMBER)
    {
      *stack2 = filo_add(*stack2, (*tmp1)->token, (*tmp1)->value);
      free(*tmp1);
    }
    else if (*stack2)
    {
      else_body(tmp2, tmp3, stack2);

      if (launch_eval(tmp1, tmp2, tmp3, stack2))
        break;
    }
    else
    {
      fprintf(stderr, "Evalexpr : unexpected token.\n");
      break;
    }
  }

  free(g_couple);
}

static void eval_next(int nb1, int nb2, struct fifo *tmp1,
                      struct filo **stack2)
{
  if (tmp1->token == EXP)
    *stack2 = filo_add(*stack2, NUMBER, pow(g_couple->nb1, g_couple->nb2));
  else if (tmp1->token == OR)
    *stack2 = filo_add(*stack2, NUMBER, nb1 || nb2);
  else if (tmp1->token == AND)
    *stack2 = filo_add(*stack2, NUMBER, nb1 && nb2);
  else if (tmp1->token == BXOR)
    *stack2 = filo_add(*stack2, NUMBER, nb1 ^ nb2);
  else if (tmp1->token == BOR)
    *stack2 = filo_add(*stack2, NUMBER, nb1 | nb2);
  else if (tmp1->token == BAND)
    *stack2 = filo_add(*stack2, NUMBER, nb1 & nb2);
  else if (tmp1->token == NOT)
    *stack2 = filo_add(*stack2, NUMBER, !nb2);
  else if (tmp1->token == BNOT)
    *stack2 = filo_add(*stack2, NUMBER, ~nb2);
}

int launch_eval(struct fifo **tmp1, struct filo **tmp2,
                struct filo **tmp3, struct filo **stack2)
{
  tmp3 = tmp3;

  if ((*tmp1)->token == PLUS)
    *stack2 = filo_add(*stack2, NUMBER, g_couple->nb1 + g_couple->nb2);
  else if ((*tmp1)->token == MINUS)
    *stack2 = filo_add(*stack2, NUMBER, g_couple->nb1 - g_couple->nb2);
  else if ((*tmp1)->token == MUL)
    *stack2 = filo_add(*stack2, NUMBER, g_couple->nb1 * g_couple->nb2);
  else if ((*tmp1)->token == DIV)
  {
    if (!g_couple->nb2)
    {
      fprintf(stderr, "Evalexpr : division by 0.\n");
      free(*tmp1);
      return 1;
    }
    *stack2 = filo_add(*stack2, NUMBER, g_couple->nb1 / g_couple->nb2);
  }
  else if ((*tmp1)->token == MOD)
    *stack2 = filo_add(*stack2, NUMBER, g_couple->nb1 % g_couple->nb2);
  else
    eval_next(g_couple->nb1, g_couple->nb2, *tmp1, stack2);
  free(*tmp1);
  free(*tmp2);
  return 0;
}
