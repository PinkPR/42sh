#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "conversion.h"
#include "tokens.h"
#include "fifo.h"
#include "filo.h"
#include "str_tools.h"

struct filo *g_stack = NULL;

long long int evalexpr(char *str);

static void remove_spaces(char *str)
{
  int l = strlen(str);
  for (int i = 0; i < l; ++i)
  {
    while (str[i] == ' ')
    {
      for (int j = i; j < l; ++j)
        str[j] = str[j + 1];
    }
  }
}

static char *shift_str(char *str, int start, int shift_val)
{
  if (shift_val > 0)
    str = realloc(str, sizeof (char) * (strlen(str) + shift_val + 1));
  if (shift_val > 0) // right_shift
  {
    for (int i = strlen(str) + 1; i >= start; --i)
      str[i + shift_val] = str[i];
  }
  else
  {
    for (unsigned int i = start - shift_val; i <= strlen(str); ++i)
      str[i + shift_val] = str[i];
  }
  return str;
}

static char *failure_par(char *s)
{
  write(STDERR_FILENO, "42sh: invalid parenthesis\n", 26);
  if (s)
    free(s);
  return NULL;
}

static char *inner_loop(char *str, int i, int len)
{
  char *s = malloc(sizeof (char) * (len + 1));
  int num = 1;
  int pos = i;
  for (i = pos + 1; i < len; ++i)
  {
    num = num - (str[i] == ')') + (str[i] == '(');
    if (num == 0)
    {
      s[i - pos - 1] = '\0';
      int a = evalexpr(s);
      char *s2 = itoa(a);
      str = shift_str(str, pos, strlen(s2) - i + pos - 1);
      for (unsigned int k = 0; k < strlen(s2); ++k)
        str[pos + k] = s2[k];
      free(s2);
      break;
    }
    else
      s[i - pos - 1] = str[i];
    if (i == len - 1)
      failure_par(s);
  }
  return str;
}

static char *evaluate_inner(char *str)
{
  int len = strlen(str);
  for (int i = 0; i < len; ++i)
    if (str[i] == '(')
    {
      str = inner_loop(str, i, len);
      str = evaluate_inner(str);
      break;
    }
    else if (str[i] == ')')
      failure_par(NULL);
  return str;
}

long long int evalexpr(char *str)
{
  remove_spaces(str);
  str = evaluate_inner(str);
  make_stack(str);
  long long int ret = 0;
  struct fifo *tmp1 = NULL;
  struct filo *tmp2 = NULL;
  struct filo *tmp3 = NULL;
  struct filo *stack2 = NULL;

  output_loop(&tmp1, &tmp2, &tmp3, &stack2);

  if (stack2)
  {
    tmp2 = filo_pop(&stack2);
    ret = tmp2->value;
  }

  free(tmp2);
  free(g_output);
  free(stack2);
  free(g_input);
  return (ret);
}
