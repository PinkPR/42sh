#include <stdio.h>
#include "tokens.h"
#include "fifo.h"
#include "evalexpr_lexer.h"
#include "filo.h"
#include "conversion.h"

struct fifo *g_output = NULL;

static int is_o0(enum token token)
{
  return token == AND || token == OR;
}

static int is_o1(enum token token)
{
  return !is_o0(token) && (token == BAND || token == BOR || token == BXOR);
}

static int is_o2(enum token token)
{
  return token == PLUS || token == MINUS;
}

static int is_o25(enum token token)
{
  return token == MUL || token == DIV || token == MOD;
}

static int is_o3(enum token token)
{
  return !is_o1(token) && !is_o2(token) && !is_o0(token) && !is_o25(token);
}

static void eval_number(int value)
{
  g_output = fifo_add(g_output, NUMBER, value);
}

static void eval_op(enum token token)
{
  struct filo *tmp = NULL;

  while (g_stack &&
         ((is_o2(g_stack->token) && is_o2(token))
         || (is_o2(g_stack->token) && is_o1(token))
         || (is_o25(g_stack->token) && is_o25(token))
         || (is_o25(g_stack->token) && is_o2(token))
         || (is_o25(g_stack->token) && is_o1(token))
         || (is_o25(g_stack->token) && is_o0(token))
         || (is_o2(g_stack->token) && is_o0(token))
         || (is_o1(g_stack->token) && is_o1(token))
         || (is_o1(g_stack->token) && is_o0(token))
         || (is_o0(g_stack->token) && is_o0(token))
         || (is_o3(g_stack->token) && is_o0(token))
         || (is_o3(g_stack->token) && is_o1(token))
         || (is_o3(g_stack->token) && is_o2(token))
         || (is_o3(g_stack->token) && is_o25(token))
         || (is_o3(g_stack->token) && is_o3(token))))
  {
    tmp = filo_pop(&g_stack);
    g_output = fifo_add(g_output, tmp->token, 0);
    free(tmp);
  }
  g_stack = filo_add(g_stack, token, 0);
}

void make_stack(char *str)
{
  g_input = make_fifo(str);
  struct fifo *tmp = NULL;
  struct filo *tmp2 = NULL;
  tmp = fifo_pop(&g_input);
  enum token tok = 0;

  while (tmp)
  {
    if (tmp->token == NUMBER)
      eval_number(tmp->value);
    else
      eval_op(tmp->token);
    free(tmp);
    tmp = fifo_pop(&g_input);
  }

  while (g_stack)
  {
    tmp2 = filo_pop(&g_stack);
    tok = tmp2->token;
    g_output = fifo_add(g_output, tok, 0);
    free(tmp2);
  }
}
