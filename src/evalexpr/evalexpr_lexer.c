#include <stdio.h>
#include <string.h>
#include "evalexpr_lexer.h"
#include "tokens.h"
#include "conversion.h"
#include "fifo.h"

enum token g_last_tok = 0;

static int is_number(char c)
{
  return c <= '9' && c >= '0';
}

static int my_atoi(char *str)
{
  if (is_number(str[0]))
  {
    if (g_last_tok == MINUSUN)
    {
      g_last_tok = NUMBER;
      return -atoi(str);
    }
    else
      return atoi(str);
  }
  else
    return 0;
}

static void clean_spaces(char **str)
{
  while ((*str)[0] == ' ')
    (*str)++;
}

static char *get_word(char **str)
{
  int is_nb = is_number((*str)[0]);
  char *new = calloc(1, 1);
  int i = 0;

  if (is_nb)
  {
    for (; (*str)[0] && is_number((*str)[0]); i++)
    {
      new = realloc(new, i + 2);
      new[i] = (*str)[0];
      (*str)++;
    }
  }
  else
  {
    for (; (*str)[0] && !is_number((*str)[0]) && (*str)[0] != ' '; i++)
    {
      new = realloc(new, i + 2);
      new[i] = (*str)[0];
      (*str)++;
    }
  }

  new[i] = 0;
  return new;
}

static enum token get_token2(char *str)
{
  if (str[0] == '|' && str[1] == '|')
    return OR;
  else if (str[0] == '^')
    return BXOR;
  else if (str[0] == '|')
    return BOR;
  else if (str[0] == '&')
    return BAND;
  else if (str[0] == '!')
    return NOT;
  else if (str[0] == '~')
    return BNOT;
  else
    return NUMBER;
}

static enum token get_token(char *str)
{
  if (str[0] == '+')
    return PLUS;
  else if (str[0] == '-')
  {
    if (g_last_tok == 0 || g_last_tok != NUMBER)
      return MINUSUN;
    else
      return MINUS;
  }
  else if (str[0] == '*' && str[1] == '*')
    return EXP;
  else if (str[0] == '/')
    return DIV;
  else if (str[0] == '%')
    return MOD;
  else if (str[0] == '*')
    return MUL;
  else if (str[0] == '&' && str[1] == '&')
    return AND;
  else
    return get_token2(str);
}

struct fifo *make_fifo(char *str)
{
  int value = 0;
  enum token tok = 0;
  char *word = NULL;

  while (str[0])
  {
    clean_spaces(&str);
    tok = get_token(str);
    if (tok == MINUSUN)
      g_last_tok = tok;
    else if (g_last_tok != MINUSUN)
      g_last_tok = tok;
    if (tok != MINUSUN)
    {
      word = get_word(&str);
      value = my_atoi(word);
      free(word);
      g_input = fifo_add(g_input, tok, value);
    }
    else
    {
      str++;
    }
  }

  return g_input;
}
