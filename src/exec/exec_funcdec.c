#include "exec.h"
#include "environ.h"

int execute_func_dec(struct ast *ast)
{
  char *name = NULL;
  int i = 0;
  if (ast->sons[0]->node->token == FUNC_DEC)
    i++;
  name = ast->sons[i]->node->token_value;
  while (ast->sons[3 + i]->node->token == EOL)
    i++;
  add_func(name, ast->sons[3 + i]);
  ast->sons_nb--;
  return 0;
}
