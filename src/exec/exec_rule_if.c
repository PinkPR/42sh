#include <stdio.h>
#include "exec.h"

int execute_rule_if(struct ast *ast)
{
  if (!execute_compound_list(ast->sons[1]))
    return execute_compound_list(ast->sons[3]);
  else
  {
    if (ast->sons_nb > 5)
      return execute_else_clause(ast->sons[4]);
  }

  return 0;
}
