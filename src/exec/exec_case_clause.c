#include "exec.h"

int execute_case_clause(struct ast *ast, char *ref)
{
  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->node->rule == CASE_ITEM)
      execute_case_item(ast->sons[i], ref);
  }

  return 0;
}
