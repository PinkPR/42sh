#include "exec.h"

static int check_sons(struct ast *ast)
{
  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (!(ast->sons[i]))
      return 0;
  }

  return 1;
}

int execute_command(struct ast *ast)
{
  if (ast && check_sons(ast) && ast->sons_nb > 0)
  {
    if (ast->sons[0]->node->rule == SIMPLE_COMMAND)
      return execute_simple_commands(ast->sons[0]);
    else if (ast->sons[0]->node->rule == SHELL_COMMAND)
      return execute_shell_command(ast->sons[0]);
    else if (ast->sons[0]->node->rule == FUNCDEC)
      return execute_func_dec(ast->sons[0]);
  }
  return 0;
}

int execute_piped_commands(struct ast *ast)
{
  if (ast && check_sons(ast) && ast->sons_nb > 0)
  {
    if (ast->sons[0]->node->rule == SIMPLE_COMMAND)
      return execute_piped_command(ast->sons[0]);
    else if (ast->sons[0]->node->rule == SHELL_COMMAND)
      return execute_shell_command(ast->sons[0]);
    else if (ast->sons[0]->node->rule == FUNCDEC)
      return execute_func_dec(ast->sons[0]);
  }
  return 0;
}
