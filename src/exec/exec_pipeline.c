#include <unistd.h>
#include <sys/wait.h>
#include "exec.h"
#include <stdio.h>
int *g_pids = NULL;

static void close_pipe(int *pfd)
{
  close(pfd[0]);
  close(pfd[1]);
}

static void push_pipe(int *pfd1, int *pfd2)
{
  pfd1[0] = pfd2[0];
  pfd1[1] = pfd2[1];
}

static void pipe_run_first(struct ast *ast, int *pfd1, int *pfd2, int *pid)
{
  int ret = 0;
  if (pipe(pfd2) == -1)
    exit(0);
  if ((*pid = fork()) == 0)
  {
    close(pfd2[1]);
    dup2(pfd2[0], 0);
    free(g_pids);
    ret = execute_piped_commands(ast);
    close(0);
    exit(ret);
  }
  push_pipe(pfd1, pfd2);
}

static void pipe_run(struct ast *ast, int *pfd1, int *pfd2, int *pid)
{
  int ret = 0;
  if (pipe(pfd2) == -1)
    exit(0);
  if ((*pid = fork()) == 0)
  {
    close(pfd2[1]);
    close(pfd1[0]);
    dup2(pfd2[0], 0);
    dup2(pfd1[1], 1);
    free(g_pids);
    ret = execute_piped_commands(ast);
    close(pfd2[0]);
    close(pfd1[1]);
    close(0);
    close(1);
    exit(ret);
  }
  close_pipe(pfd1);
  push_pipe(pfd1, pfd2);
}

static void pipe_run_last(struct ast *ast, int *pfd1, int *pid)
{
  int ret = 0;
  if ((*pid = fork()) == 0)
  {
    close(pfd1[0]);
    dup2(pfd1[1], 1);
    free(g_pids);
    ret = execute_piped_commands(ast);
    close(pfd1[1]);
    close(1);
    exit(ret);
 }
  close_pipe(pfd1);
}

int execute_pipeline(struct ast *ast)
{
  int ret = 127;
  int pfd1[2];
  int pfd2[2];
  if (ast->sons_nb == 1)
    return execute_command(ast->sons[0]);
  else
  {
    g_pids = calloc(1, ast->sons_nb * sizeof (int));
    pipe_run_first(ast->sons[ast->sons_nb - 1], pfd1,
        pfd2, g_pids + ast->sons_nb - 1);

    for (int i = ast->sons_nb - 2; i > 0; --i)
    {
      enum rule rule = ast->sons[i]->node->rule;
      if (rule == COMMAND)
        pipe_run(ast->sons[i], pfd1, pfd2, g_pids + i);
    }

    pipe_run_last(ast->sons[0], pfd1, g_pids);
    close_pipe(pfd2);
    for (int i = 0; i < ast->sons_nb; ++i)
      if (ast->sons[i]->node->rule == COMMAND)
        waitpid(g_pids[i], &ret, 0);
    free(g_pids);
  }
  return WEXITSTATUS(ret);
}
