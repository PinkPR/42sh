#include <stdio.h>
#include "builtin.h"
#include "exec.h"

int execute_compound_list(struct ast *ast)
{
  int ret = 0;
  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->node->rule == AND_OR)
      ret = execute_and_or(ast->sons[i]);
    if (ret == MNB_CONTINUE || ret == MNB_BREAK)
      return ret;
  }

  return ret;
}
