#include "exec.h"
#include "expand.h"
#include "str_tools.h"
#include "environ.h"
#include "builtin.h"
#include "readline.h"
#include "main.h"
#include "redirections.h"
#include "builtin.h"
#include "jobs.h"

static int args_create(struct ast *ast, char **args)
{
  struct ast *son;
  struct ast *sson;
  int argc = 0;
  for (int i = 0; i < ast->sons_nb; i++)
  {
    son = ast->sons[i];
    sson = son->sons[0];
    if (sson->node->rule != REDIRECTION)
    {
      if (son->node->rule == ELEMENT)
        args[argc++] = expand(sson->node->token_value);
      else if (son->node->rule == PREFIX)
      {
        char *tmp = expand(sson->node->token_value);
        add_var(tmp);
        free(tmp);
      }
    }
  }
  args[argc] = NULL;
  return argc;
}

static void free_args(char **args)
{
  for (int i = 0; args[i]; i++)
    free(args[i]);
  free(args);
}

static int *dup_redirections(s_outputs *op)
{
  int *backup = malloc(sizeof (int) * op->nb_outputs);
  for (int i = 0; i < op->nb_outputs; i += 2)
  {
    backup[i] = dup(op->outputs[i]);
    dup2(op->outputs[i + 1], op->outputs[i]);
  }
  return backup;
}

static int restore_redirections(s_outputs *op, int *backup, int status)
{
  for (int i = 0; i < op->nb_outputs; i += 2)
  {
    dup2(backup[i], op->outputs[i]);
    close(backup[i]);
    dup2(op->outputs[i], op->outputs[i + 1]);
  }
  free(backup);
  return status;
}

static void case_fork(char **argv, s_outputs *op, int *status, int *backup)
{
  pid_t pid = fork();
  if (pid == 0)
  {
    if (execvp(argv[0], argv) == -1)
    {
      free_args(argv);
      free_outputs(op);
      free_g_main();
      free(backup);
      exit(127);
    }
  }
  else
  {
    if (g_control && g_control->shell_pgid == getpgrp())
      g_main.cur_job_pid = pid;
    waitpid(pid, status, WUNTRACED | WSTOPPED | WCONTINUED);
  }
}

static int exec_cmd(char **argv, s_outputs *op)
{
  int *backup = dup_redirections(op);
  int status = exec_builtin(argv[0], &(argv[1]), g_bltn_list);
  if (!status || status != 42)
    return restore_redirections(op, backup, status);
  status = 0;
  if (g_control == NULL || g_control->can_fork)
    case_fork(argv, op, &status, backup);
  else if (execvp(argv[0], argv) == -1)
  {
    free_args(argv);
    free_outputs(op);
    free_g_main();
    exit(127);
  }
  return restore_redirections(op, backup, WEXITSTATUS(status));
}

int execute_piped_command(struct ast *ast)
{
  int status = 0;
  int pos = 0;
  char **args = malloc(sizeof (char *) * (ast->sons_nb + 1));
  pos = args_create(ast, args);
  status = exec_builtin(args[0], &(args[1]), g_bltn_list);
  if (!status || status != 42)
  {
    free_args(args);
    exit(status);
  }
  status = 0;
  if (!args[0])
    status = 0;
  if (is_func(args[0]))
    status = exec_func(args[0], pos, args);
  else if (execvp(args[0], args) == -1)
  {
    free_args(args);
    fprintf(stderr, "%s: command not found.\n", args[0]);
    return 127;
  }
  free_args(args);
  return status;
}

int execute_simple_commands(struct ast *ast)
{
  int status = 0;
  int argc = 0;
  char **args = malloc(sizeof (char *) * (ast->sons_nb + 1));
  s_outputs *outputs = get_redirection(ast);
  argc = args_create(ast, args);
  if (!args[0])
  {
    free(args);
    free_outputs(outputs);
    return 0;
  }
  if (is_func(args[0]))
    status = exec_func(args[0], argc, args);
  else
    status = exec_cmd(args, outputs);
  if (status == 127)
    fprintf(stderr, "%s: command not found.\n", args[0]);
  free_args(args);
  free_outputs(outputs);
  return status;
}
