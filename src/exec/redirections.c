#include <fcntl.h>
#include <unistd.h>
#include "redirections.h"
#include "ast.h"
#include "main.h"
#include "exec.h"
#include "expand.h"
#include "readline.h"

static enum redir get_redir2(char *redir)
{
  char c1 = redir[0];
  char c2 = redir[1];
  char c3 = 0;
  if (c2)
    c3 = redir[2];
  if (c1 == '<')
  {
    if (c2 == '&')
      return DUPOUT_REDIR;
    else if (c2 == '<' && c3 == '-')
      return HDNO_REDIR;
    else if (c2 == '<')
      return HD_REDIR;
    else if (c2 == '>')
      return DIAMOND_REDIR;
    else
      return IN_REDIR;
  }
  return NOREDIR;
}

static enum redir get_redir(char *redir)
{
  char c1 = redir[0];
  char c2 = redir[1];
  if (c1 == '>')
  {
    if (c2 == '>')
      return APPEND_REDIR;
    else if (c2 == '&')
      return DUPOUT_REDIR;
    else if (c2 == '|')
      return EXTEND_REDIR;
    return OUT_REDIR;
  }
  return get_redir2(redir);
}

static int get_ionumber(int ion, enum redir redir)
{
  if (ion >= 0)
    return ion;
  else
  {
    if (redir == OUT_REDIR || redir == APPEND_REDIR || redir == EXTEND_REDIR
        || redir == DUPOUT_REDIR)
      return 1;
    else
      return 0;
  }
}

static void add_output(s_outputs *outs, int in, int out)
{
  outs->outputs = realloc(outs->outputs,
      sizeof (int) * (outs->nb_outputs + 2));
  outs->outputs[outs->nb_outputs] = in;
  outs->outputs[outs->nb_outputs + 1] = out;
  outs->nb_outputs += 2;
}

static int get_no_tab(char *str, int no)
{
  int i = 0;
  while (no && str[i] == ' ')
    i++;
  return i;
}

static void heredoc(char *word, s_outputs *outputs, int no)
{
  int tube[2];
  pipe(tube);
  s_readl *rl = NULL;
  while (1)
  {
    rl = readline(1);
    char *str = strdup(rl->command);
    free(rl->command);
    free(rl);
    int count = strlen(str);
    int i = get_no_tab(str, no);
    str[count - 1] = 0;
    if (!strcmp(str + i, word))
    {
      free(str);
      break;
    }
    str[count - 1] = '\n';
    write(tube[1], str + i, count - i);
    free(str);
  }
  close(tube[1]);
  add_output(outputs, STDIN_FILENO, tube[0]);
}

static int get_fd(enum redir red, char *word, s_outputs *outputs)
{
  int flags = 0;
  if (red == OUT_REDIR || red == EXTEND_REDIR || red == DIAMOND_REDIR)
    flags = O_RDWR | O_CREAT | O_TRUNC;
  else if (red == APPEND_REDIR)
    flags = O_WRONLY | O_CREAT | O_APPEND;
  else if (red == HD_REDIR)
    heredoc(word, outputs, 0);
  else if (red == HDNO_REDIR)
    heredoc(word, outputs, 1);
  int fd = 0;
  if (red == DUPOUT_REDIR || red == DUPIN_REDIR)
    fd = atoi(word);
  else
    fd = open(word, flags, 0644);
  return fd;
}

static void set_outputs(struct ast *sson, s_outputs *outputs)
{
  int ion = -1;
  if (sson->sons[0]->node->token == IONUMBER)
    ion = atoi(sson->sons[0]->node->token_value);
  int j = (ion == -1) ? 0 : 1;
  char *redir = sson->sons[j]->node->token_value;
  char *word = expand(sson->sons[j + 1]->node->token_value);
  enum redir red = get_redir(redir);
  ion = get_ionumber(ion, red);
  int fd = get_fd(red, word, outputs);
  if (fd > 0)
    add_output(outputs, ion, fd);
  free(word);
}

s_outputs *get_redirection(struct ast *ast)
{
  s_outputs *outputs = malloc(sizeof (s_outputs));
  outputs->outputs = NULL;
  outputs->nb_outputs = 0;
  struct ast *son;
  struct ast *sson;
  for (int i = 0; i < ast->sons_nb; i++)
  {
    son = ast->sons[i];
    sson = son->sons[0];
    if (sson->node->rule == REDIRECTION)
      set_outputs(sson, outputs);
  }
  return outputs;
}

void free_outputs(s_outputs *op)
{
  for (int i = 0; i < op->nb_outputs; i++)
    if (op->outputs[i] > 2)
      close(op->outputs[i]);
  free(op->outputs);
  free(op);
}

