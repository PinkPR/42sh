#include <string.h>
#include "exec.h"
#include "fnmatch.h"

static char **get_words(struct ast *ast)
{
  char **words = malloc(sizeof (char *));
  int offset = 0;

  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->node->token == WORD)
    {
      words[offset] = ast->sons[i]->node->token_value;
      offset++;
      words = realloc(words, sizeof (char *) * (offset + 2));
      words[offset + 1] = NULL;
    }
  }

  return words;
}

static struct ast *get_complist(struct ast *ast)
{
  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->node->rule == COMPOUND_LIST)
      return ast->sons[i];
  }

  return NULL;
}

int execute_case_item(struct ast *ast, char *ref)
{
  int pos = 0;

  if (ast->sons[0]->node->token == LEFT_PAR)
    pos++;

  char **words = get_words(ast);
  struct ast *complist = get_complist(ast);

  for (int i = 0; words[i] != 0; i++)
  {
    if (!my_fnmatch(words[i], ref))
    {
      execute_compound_list(complist);
      break;
    }
  }

  free(words);
  return 0;
}
