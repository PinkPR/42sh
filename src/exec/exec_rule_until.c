#include <stdio.h>
#include "exec.h"
#include "readline.h"
#include "builtin.h"

int execute_rule_until(struct ast *ast)
{
  int ret = execute_compound_list(ast->sons[1]);
  int ret2 = 0;

  signal(SIGINT, sigint_cancel);
  signal(SIGTSTP, SIG_IGN);
  while (ret && g_continue)
  {
    ret2 = execute_do_group(ast->sons[2]);
    ret = execute_compound_list(ast->sons[1]);
    if (ret2 == MNB_BREAK)
      return 0;
    if (ret2 == MNB_CONTINUE)
      continue;
  }
  signal(SIGINT, empty_signal);
  signal(SIGTSTP, sigtstp_handler);

  return 0;
}
