#include "exec.h"

int execute_ast(struct ast *ast)
{
  int ret = 0;
  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->sons[0]->node->token != EOL
        && ast->sons[i]->sons[0]->node->token != ENDOF)
      ret = execute_input(ast->sons[i]);
  }
  if (ast->sons_nb == 0)
    return -1;
  return ret;
}

int execute_input(struct ast *ast)
{
  enum rule rule = ast->sons[0]->node->rule;
  if (rule == LIST)
    return execute_list(ast->sons[0]);
  return 0;
}

int execute_list(struct ast *ast)
{
  int ret = 0;
  for (int i = 0; i < ast->sons_nb; i++)
  {
    enum rule rule = ast->sons[i]->node->rule;
    if (rule == AND_OR)
     ret = execute_and_or(ast->sons[i]);
  }
  return ret;
}
