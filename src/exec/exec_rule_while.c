#include <stdio.h>
#include "exec.h"
#include "readline.h"
#include "builtin.h"

int g_continue;

void sigint_cancel(int num)
{
  num++;
  num--;
  g_continue = 0;
  write(STDOUT_FILENO, "\n", 1);
}

int execute_rule_while(struct ast *ast)
{
  int ret = execute_compound_list(ast->sons[1]);
  int ret2 = 0;

  signal(SIGTSTP, SIG_IGN);
  signal(SIGINT, sigint_cancel);
  while (!ret && g_continue)
  {
    ret2 = execute_do_group(ast->sons[2]);
    ret = execute_compound_list(ast->sons[1]);
    if (ret2 == MNB_BREAK)
      return 0;
    if (ret2 == MNB_CONTINUE)
      continue;
  }
  signal(SIGINT, empty_signal);
  signal(SIGTSTP, sigtstp_handler);

  return 0;
}
