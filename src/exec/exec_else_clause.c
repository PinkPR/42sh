#include "exec.h"

int execute_else_clause(struct ast *ast)
{
  if (ast->sons[0]->node->token == ELSE)
    return execute_compound_list(ast->sons[1]);
  else if (ast->sons[0]->node->token == ELIF)
  {
    if (!execute_compound_list(ast->sons[1]))
      return execute_compound_list(ast->sons[3]);
    else
      return execute_compound_list(ast->sons[5]);
  }
  if (ast->sons_nb > 4)
    return execute_else_clause(ast->sons[4]);
  return 0;
}
