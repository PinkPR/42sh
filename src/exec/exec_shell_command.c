#include "exec.h"

int execute_shell_command(struct ast *ast)
{
  struct ast *son = ast->sons[0];
  enum rule rule = son->node->rule;
  if (rule == RULE_IF)
    return execute_rule_if(son);
  else if (rule == RULE_FOR)
    return execute_rule_for(son);
  else if (rule == RULE_WHILE)
    return execute_rule_while(son);
  else if (rule == RULE_UNTIL)
    return execute_rule_until(son);
  else if (rule == RULE_CASE)
    return execute_rule_case(son);
  else
    return execute_compound_list(ast->sons[1]);
  return 0;
}
