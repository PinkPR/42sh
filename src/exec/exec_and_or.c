#include "exec.h"

int execute_and_or(struct ast *ast)
{
  int ret = 0;
  for (int i = 0; i < ast->sons_nb; i++)
  {
    enum rule rule = ast->sons[i]->node->rule;
    enum token token = ast->sons[i]->node->token;
    if (rule == PIPELINE)
      ret = execute_pipeline(ast->sons[i]);
    else if (rule == NONE)
    {
      if (token == DOUBLE_AND && ret == 1)
          return ret;
      else if (token == DOUBLE_OR && ret == 0)
          return ret;
    }

  }
  return ret;
}
