#include <stdio.h>
#include <string.h>
#include "exec.h"
#include "environ.h"
#include "readline.h"
#include "builtin.h"

static int get_nb(struct ast *ast)
{
  int i = 2;
  int nb = 0;

  for (; ast->sons[i]->node->token != WORD
       && ast->sons[i]->node->token != SEMI_COLON
       && i < ast->sons_nb; i++)
    continue;

  if (ast->sons[i]->node->token != SEMI_COLON)
  {
    for (; ast->sons[i]->node->token == WORD; i++)
      nb++;
    return nb;
  }
  return 0;
}

static int is_in(struct ast *ast)
{
  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->node->token == IN)
      return i;
  }

  return 0;

}

static char **get_words(struct ast *ast)
{
  char **words = NULL;
  int count = 0;

  for (int i = 2; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->node->token == WORD)
    {
      count++;
      words = realloc(words, sizeof (char *) * count);
      words[count - 1] = ast->sons[i]->node->token_value;
    }
  }

  return words;
}

int execute_rule_for(struct ast *ast)
{
  int i = 0;
  int nb = 0;
  int ret = 0;
  struct ast *do_group = ast->sons[ast->sons_nb - 1];
  char **words = NULL;
  char *var = NULL;
  i = is_in(ast);
  if (i)
  {
    nb = get_nb(ast);
    words = get_words(ast);
    for (int j = 0; j < nb && g_continue; j++)
    {
      var = strdup(ast->sons[1]->node->token_value);
      st_add_var(strdup(var), strdup(words[j]));
      ret = execute_do_group(do_group);
      free(var);
      if (ret == MNB_BREAK)
        return 0;
      if (ret == MNB_CONTINUE)
        continue;
    }
  }
  free(words);
  return 0;
}
