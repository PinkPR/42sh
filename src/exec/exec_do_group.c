#include <stdio.h>
#include "exec.h"

int execute_do_group(struct ast *ast)
{
  int ret = execute_compound_list(ast->sons[1]);

  return ret;
}
