#include "exec.h"
#include <stdio.h>

int execute_rule_case(struct ast *ast)
{
  int pos = 0;
  char *word = ast->sons[1]->node->token_value;

  for (; ast->sons[pos]->node->rule != CASE_CLAUSE; pos++)
  {
    if (ast->sons[pos + 1] == NULL)
      break;
    else
      continue;
  }
  execute_case_clause(ast->sons[pos], word);

  return 0;
}
