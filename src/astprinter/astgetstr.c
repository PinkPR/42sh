#include "astprinter.h"

static void insert_bs(char *str)
{
  for (int i = strlen(str); i > 0; i--)
    str[i] = str[i - 1];

  str[0] = '\\';
}

static char *normalize_str(char *str)
{
  for (int i = 0; str[i] != 0; i++)
  {
    if (str[i] == '"')
    {
      str = realloc(str, strlen(str) + 2);
      insert_bs(&(str[i]));
      i++;
    }
  }

  return str;
}

static char *get_str_rule2(enum rule rule)
{
  if (rule == 11)
    return strdup("Compound List");
  else if (rule == 12)
    return strdup("Rule For");
  else if (rule == 13)
    return strdup("Rule While");
  else if (rule == 14)
    return strdup("Rule Until");
  else if (rule == 15)
    return strdup("Rule Case");
  else if (rule == 16)
    return strdup("Rule If");
  else if (rule == 17)
    return strdup("Else Clause");
  else if (rule == 18)
    return strdup("Do Group");
  else if (rule == 19)
    return strdup("Case Clause");
  else if (rule == 20)
    return strdup("Case Item");
  else if (rule == 22)
    return strdup("Origin");
  else
    return strdup("Redir Heredoc");
}

static char *get_str_rule(enum rule rule)
{
  if (rule == -1)
    return strdup("Input");
  else if (rule == 1)
    return strdup("List");
  else if (rule == 2)
    return strdup("And Or");
  else if (rule == 3)
    return strdup("Pipeline");
  else if (rule == 4)
    return strdup("Command");
  else if (rule == 5)
    return strdup("Simple Command");
  else if (rule == 6)
    return strdup("Shell Command");
  else if (rule == 7)
    return strdup("Func Declaration");
  else if (rule == 8)
    return strdup("Redirection");
  else if (rule == 9)
    return strdup("Prefix");
  else if (rule == 10)
    return strdup("Element");
  else
    return get_str_rule2(rule);
}

static char *get_str_token4(enum token token, char *token_value)
{
  if (token == DO)
    return strdup("Do");
  else if (token == UNTIL)
    return strdup("Until");
  else if (token == DONE)
    return strdup("Done");
  else if (token == FI)
    return strdup("Fi");
  else if (token == IN)
    return strdup("In");
  else if (token == IONUMBER)
    return normalize_str(strdup(token_value));
  else if (token == ENDOF)
    return strdup("EOF");
  else
    return strdup("404");
}

static char *get_str_token3(enum token token, char *token_value)
{
  if (token == HEREDOC)
    return normalize_str(strdup(token_value));
  else if (token == ASSIGNMENT_WORD)
    return normalize_str(strdup(token_value));
  else if (token == DOUBLE_SEMI_COLON)
    return strdup(";;");
  else if (token == IF)
    return strdup("If");
  else if (token == THEN)
    return strdup("Then");
  else if (token == ELSE)
    return strdup("Else");
  else if (token == ELIF)
    return strdup("Elif");
  else if (token == FOR)
    return strdup("For");
  else if (token == WHILE)
    return strdup("While Tok");
  else if (token == CASE)
    return strdup("Case");
  else if (token == ESAC)
    return strdup("Esac");
  return get_str_token4(token, token_value);
}

static char *get_str_token2(enum token token, char *token_value)
{
  if (token == FUNC_DEC)
    return strdup("Function");
  else if (token == SIMPLE_R_CHEV)
    return strdup(">");
  else if (token == DOUBLE_R_CHEV)
    return strdup(">>");
  else if (token == SIMPLE_L_CHEV)
    return strdup("<");
  else if (token == DOUBLE_L_CHEV)
    return strdup("<<");
  else if (token == HEREDOC_CHE)
    return strdup("<<-");
  else if (token == R_CHEV_AND)
    return strdup(">&");
  else if (token == L_CHEV_AND)
    return strdup("<&");
  else if (token == R_CHEV_PIPE)
    return strdup(">|");
  else if (token == INVERT_CHEV)
    return strdup("<>");
  else if (token == SEMI_COLON)
    return strdup(";");
  else
    return get_str_token3(token, token_value);
}

char *get_str_token(enum token token, char *token_value)
{
  if (token == EOL)
    return strdup("EOL");
  else if (token == SIMPLE_AND)
    return strdup("&");
  else if (token == DOUBLE_AND)
    return strdup("&&");
  else if (token == DOUBLE_OR)
    return strdup("||");
  else if (token == PIPE)
    return strdup("|");
  else if (token == NOT)
    return strdup("!");
  else if (token == WORD)
    return normalize_str(strdup(token_value));
  else if (token == LEFT_BRACE)
    return strdup("{");
  else if (token == RIGHT_BRACE)
    return strdup("}");
  else if (token == LEFT_PAR)
    return strdup("(");
  else if (token == RIGHT_PAR)
    return strdup(")");
  else
    return get_str_token2(token, token_value);
}

char *get_str(struct node *node)
{
  if (node->rule != 0)
    return get_str_rule(node->rule);
  else
    return get_str_token(node->token, node->token_value);
}
