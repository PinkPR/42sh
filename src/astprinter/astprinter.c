#include <stdio.h>
#include "astprinter.h"
#include "rules.h"

static void *magic(void *ptr)
{
  return ptr;
}

static void init_print(FILE *f)
{
  fprintf(f, "digraph ast {\n");
}

static void end_print(FILE *f)
{
  fprintf(f, "}\n");
}

static void print_link(FILE *f, struct ast *ast, struct ast *ast2)
{
  char *str = 0;

  str = get_str(ast->node);
  fprintf(f, "a%p", magic(ast));
  fprintf(f, " -> ");

  free(str);

  fprintf(f, "a%p", magic(ast2));

  fprintf(f, ";\n");
}

static void print_body(FILE *f, struct ast *ast)
{
  char *str;
  str = get_str(ast->node);
  char *color = "palegreen";
  enum rule num = ast->node->rule;
  if (num >= COMMAND && num <= SHELL_COMMAND)
    color = "cyan1";
  else if (num >= RULE_FOR && num <= RULE_IF)
    color = "indianred1";
  else if (num == ELEMENT)
    color = "lightgoldenrod1";
  else if (num == NONE)
    color = "steelblue1";
  fprintf(f,
      "a%p [label=\"%s\", shape=box style=\"filled,rounded\" , color=%s];\n",
      magic(ast), str, color);
  free(str);

  for (int i = 0; i < ast->sons_nb; i++)
  {
    print_body(f, ast->sons[i]);
    print_link(f, ast, ast->sons[i]);
  }
}

void ast_print(struct ast *ast)
{
  FILE *f = fopen("ast.dot", "w");

  init_print(f);
  print_body(f, ast);
  end_print(f);
  fclose(f);
}
