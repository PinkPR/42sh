#include <stdlib.h>
#include "main.h"
#include "builtin.h"

static void free_args(char **args)
{
  for (int i = 0; args[i]; i++)
    free(args[i]);
}

static int is_digit(char c)
{
  return c >= '0' && c <= '9';
}

int builtin_exit(char **args)
{
  int ret = 0;

  if (args[0])
  {
    if (is_digit(args[0][0]))
      ret = atoi(args[0]);
    else
    {
      fprintf(stderr, "exit: %s: numeric argument required\n", args[0]);
      ret = 255;
    }
  }
  free_g_main();
  free_args(args);
  exit(ret);
  return ret;
}
