#include <string.h>
#include "builtin.h"

struct bltn_list *g_bltn_list = NULL;

struct bltn_list *bltn_add(struct bltn_list *list, char *name,
                           int (*callback)(char **args))
{
  struct bltn_list *new = malloc(sizeof (struct bltn_list));
  new->bltn_name = name;
  new->callback = callback;

  new->next = list;

  return new;
}

void bltn_list_free(struct bltn_list *list)
{
  if (list)
  {
    bltn_list_free(list->next);
    free(list->bltn_name);
    free(list);
  }
}

static int is_builtin(char *name, struct bltn_list *list)
{
  if (list == NULL)
    return 0;
  else if (!strcmp(list->bltn_name, name))
    return 1;
  else
    return is_builtin(name, list->next);
}

int exec_builtin(char *name, char **args, struct bltn_list *list)
{
  if (list == NULL)
    return 42;
  else if (is_builtin(name, list))
  {
    while (strcmp(list->bltn_name, name))
      list = list->next;
    return list->callback(args);
  }
  else
    return exec_builtin(name, args, list->next);
}

void init_g_bltn_list(void)
{
  g_bltn_list = bltn_add(g_bltn_list, strdup("cd"), builtin_cd);
  g_bltn_list = bltn_add(g_bltn_list, strdup("break"), builtin_break);
  g_bltn_list = bltn_add(g_bltn_list, strdup("echo"), builtin_echo);
  g_bltn_list = bltn_add(g_bltn_list, strdup("exit"), builtin_exit);
  g_bltn_list = bltn_add(g_bltn_list, strdup("alias"), builtin_alias);
  g_bltn_list = bltn_add(g_bltn_list, strdup("unalias"), builtin_unalias);
  g_bltn_list = bltn_add(g_bltn_list, strdup("export"), builtin_export);
  g_bltn_list = bltn_add(g_bltn_list, strdup("history"), builtin_history);
  g_bltn_list = bltn_add(g_bltn_list, strdup("jobs"), builtin_jobs);
  g_bltn_list = bltn_add(g_bltn_list, strdup("fg"), builtin_fg);
  g_bltn_list = bltn_add(g_bltn_list, strdup("bg"), builtin_bg);
  g_bltn_list = bltn_add(g_bltn_list, strdup("wait"), builtin_wait);
  g_bltn_list = bltn_add(g_bltn_list, strdup("source"), builtin_source);
  g_bltn_list = bltn_add(g_bltn_list, strdup("."), builtin_source);
  g_bltn_list = bltn_add(g_bltn_list, strdup("continue"), builtin_continue);
  g_bltn_list = bltn_add(g_bltn_list, strdup("shopt"), builtin_shopt);
}
