#include <unistd.h>
#include <string.h>
#include "builtin.h"
#include "environ.h"

static char *custom_dup(char *str)
{
  char *new = malloc(strlen(str) + 1);
  int i = 0;

  for (; str[i]; i++)
    new[i] = str[i];

  new[i] = 0;

  return new;
}

static char *custom_cat(char *s1, char *s2)
{
  int i = 0;
  int j = 0;
  char *new = malloc(strlen(s1) + strlen(s2) + 1);

  for (; s1[i]; i++)
    new[i] = s1[i];

  for (; s2[j]; j++)
    new[i + j] = s2[j];

  new[i + j] = 0;

  return new;
}

static void add_to_env(char *key, char *value)
{
  char *new = NULL;
  char *new2 = NULL;
  new = custom_cat(key, "=");
  new2 = custom_cat(new, value);

  add_env(custom_dup(new2));
  free(new);
  free(key);
  free(value);
  free(new2);
}

int builtin_cd(char **args)
{
  char *pwd = custom_dup(get_var("PWD"));
  char *oldpwd = custom_dup(get_var("OLDPWD"));
  char *path = args[0];

  if (!args[0])
    path = get_var("HOME");

  if (args[0] && !strcmp(args[0], "-"))
    path = oldpwd;

  int ret = chdir(path);

  if (ret)
  {
    free(pwd);
    free(oldpwd);
    fprintf(stderr, "cd: %s: No such file or directory\n", args[0]);
    return 1;
  }

  path = calloc(1024, 1);
  path = getcwd(path, 1024);

  add_to_env(custom_dup("PWD"), custom_dup(path));
  add_to_env(custom_dup("OLDPWD"), custom_dup(pwd));

  free(pwd);
  free(path);
  free(oldpwd);

  return 0;
}
