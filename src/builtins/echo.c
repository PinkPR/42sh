#include <unistd.h>
#include <string.h>
#include "builtin.h"
#include "my_getopt.h"
#include "main.h"

static void shift_left(char *str)
{
  for (unsigned int i = 1; i <= strlen(str); i++)
    str[i - 1] = str[i];
}

static char get_num(char *str)
{
  char *new = malloc(4);

  for (int i = 0; i < 3; i++)
    new[i] = str[i + 1];

  new[3] = 0;

  char c = strtol(new, NULL, 8);
  free(new);

  return c;
}

static int is_number_b8(char c)
{
  return c <= '8' && c >= '0';
}

static void apply_change(char *str)
{
  shift_left(str);

  if (str[0] == 'a')
    str[0] = '\a';
  else if (str[0] == 'b')
    str[0] = '\b';
  else if (str[0] == 'f')
    str[0] = '\f';
  else if (str[0] == 'n')
    str[0] = '\n';
  else if (str[0] == 'r')
    str[0] = '\r';
  else if (str[0] == 't')
    str[0] = '\t';
  else if (str[0] == 'v')
    str[0] = '\v';
  else if (str[0] == '\\')
    str[0] = '\\';
  else if (str[0] == '0' && is_number_b8(str[1]) && is_number_b8(str[2])
           && is_number_b8(str[3]))
  {
    char c = get_num(str);

    for (int i = 0; i < 3; i++)
      shift_left(str);

    str[0] = c;
  }
}

static char *apply_e_option(char *str)
{
  char *new = str;

  for (int i = 0; new[i]; i++)
  {
    if (new[i] == '\\')
    {
      apply_change(&(new[i]));
      i--;
    }
  }

  return new;
}

static int check_e(char **args)
{
  if (args[0] && args[0][0] == '-' && strlen(args[0]) > 1 &&
      (args[0][1] == 'e' || args[0][2] == 'e'))
    return 1;
  else if (args[0] && args[1] && args[1][0] == '-' && args[1][1] == 'e')
    return 1;

  return 0;
}

static int check_E(char **args)
{
  if (args[0] && args[0][0] == '-' && strlen(args[0]) > 1 &&
      (args[0][1] == 'E' || args[0][2] == 'E'))
    return 1;
  else if (args[0] && args[1] && args[1][0] == '-' && args[1][1] == 'E')
    return 1;

  return 0;
}

static int check_n(char **args)
{
  if (args[0] && args[0][0] == '-' && strlen(args[0]) > 1 &&
      (args[0][1] == 'n' || args[0][2] == 'n'))
    return 1;
  else if (args[0] && args[1] && args[1][0] == '-' && args[1][1] == 'n')
    return 1;

  return 0;
}

int builtin_echo(char **args)
{
  int n = 0;

  if (args[0])
  {
    n = check_e(args) + check_n(args) + check_E(args);
    for (int i = n; args[i] != NULL; i++)
    {
      if (check_e(args) || arg_is_set(g_main.shopts, "xpg_echo"))
        printf("%s", apply_e_option(args[i]));
      else
        printf("%s", args[i]);
      if (args[i + 1] != NULL)
        printf(" ");
    }
  }
  if (!check_n(args))
    printf("\n");

  fflush(stdout);
  return 0;
}
