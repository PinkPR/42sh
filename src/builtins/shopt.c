#include <stdio.h>
#include <string.h>
#include "builtin.h"
#include "my_getopt.h"
#include "main.h"
#include "str_tools.h"
#include <stdio.h>
#include "main.h"

static int set_unset_shopt(s_arg_l *l, int i, char *opt)
{
  if (!l)
  {
    fprintf(stderr, "42sh: %s: invalid shell option name\n", opt);
    return 1;
  }
  if (str_cmp(l->arg->name, opt, ' '))
  {
    l->arg->set = i;
    return 0;
  }
  set_unset_shopt(l->next, i, opt);
  return 0;
}

int builtin_shopt(char **args)
{
  s_arg_l *l = g_main.shopts;
  if (!args[0])
  {
    shopt_print();
    return 0;
  }
  if (args[0][0] != '-')
  {
    int ret = 0;
    for (int i = 0; args[i]; ++i)
      ret = ret || shopt_print_shopt(args[i]);
    return ret;
  }
  if (args[0][0] == '-' && args[0][1] == 's')
    for (int i = 1; args[i]; ++i)
      return set_unset_shopt(l, 1, args[i]);
  if (args[0][0] == '-' && args[0][1] == 'u')
    for (int i = 1; args[i]; ++i)
      return set_unset_shopt(l, 0, args[i]);
  return 0;
}
