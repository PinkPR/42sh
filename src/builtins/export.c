#include "builtin.h"
#include "environ.h"
#include "main.h"

static void check_exp_args(int *ret, int *tab, char **argv)
{
  int i = -1;
  while (argv[++i] != NULL)
  {
    if (argv[i][0] == '-' && argv[i][1] == 'n' && argv[i][2] == '\0')
      tab[0] = 1;
    if (argv[i][0] == '-' && argv[i][1] == 'p' && argv[i][2] == '\0')
      tab[1] = 1;
    if (argv[i][0] == '-' && (argv[i][1] != 'p' || argv[i][1] != 'n')
        && (argv[i][2] == '\n' || argv[i][2] == ' '))
      *ret = -1;
  }
}

static int print_exp_err(int ret, char *name)
{
  if (ret == 0)
    return ret;
  if (ret == -1)
  {
    fprintf(stderr, "export: Invalid option: Usage `export [-n] [-p]");
    fprintf(stderr, " [name[=value]]'\n");
    return ret;
  }
  if (ret == -2)
    fprintf(stderr, "export: Invalid shell variable name %s\n", name);
  return ret;
}

static int is_valid_name(char *name)
{
  int i = -1;
  while (name[++i] != '\0')
  {
    if ((name[i] < '0' && name[i] > '9') || (name[i] < 'A' && name[i] > 'Z')
        || (name[i] < 'a' && name[i] > 'z'))
      return 0;
    if (name[i] == '=')
      return 1;
  }
  return 1;
}

static void str_sort(char ***name, int max_size, char *temp, int n)
{
  for (int i = 0; i < n; i++)
  {
    name[0][i] = strdup(environ[i]);
    name[0][i] = realloc(name[0][i], sizeof (char) * (max_size + 1));
  }
  for (int i = 0; i < n - 1; i++)
  {
    for (int j = i + 1; j < n; j++)
    {
      if (strcmp(name[0][i], name[0][j]) > 0)
      {
        strcpy(temp, name[0][i]);
        strcpy(name[0][i], name[0][j]);
        strcpy(name[0][j], temp);
        int len = sizeof (temp);
        memset(temp, '\0', len);
      }
    }
  }
}

static int sort_env(char ***name)
{
  int n = -1;
  int max_size = 1;
  int size = -1;
  while (environ[++n] != NULL)
  {
    while (environ[n][++size] != '\0')
      continue;
    if (size > max_size)
      max_size = size;
    size = -1;
  }
  char temp[max_size];
  name[0] = malloc(sizeof (char *) * n);
  str_sort(name, max_size, temp, n);
  return n;
}

static int init_env_str(char ***env_names, char ***env_values,
                        int len_name, int len_value)
{
  char **s_environ = NULL;
  int count = sort_env(&s_environ);
  env_names[0] = malloc(sizeof (char *) * count);
  env_values[0] = malloc(sizeof (char *) * count);
  for (int i = 0; i < count; i++)
  {
    while (s_environ[i][++len_name] != '=' && s_environ[i][len_name] != '\0')
      continue;
    env_names[0][i] = malloc(sizeof (char) * (len_name + 2));
    strncpy(env_names[0][i], s_environ[i], len_name + 1);
    env_names[0][i][len_name + 1] = '\0';
    while (s_environ[i][++len_value + len_name + 1] != '\0')
      continue;
    env_values[0][i] = malloc(sizeof (char) * (len_value + 4));
    strncpy(env_values[0][i] + 1, s_environ[i] + len_name + 1, len_value);
    env_values[0][i][0] = '"';
    env_values[0][i][len_value + 1] = '"';
    env_values[0][i][len_value + 2] = '\0';
    len_value = -1;
    len_name = -1;
    free(s_environ[i]);
  }
  free(s_environ);
  return count;
}

static int print_env(void)
{
  //uses the global variable environ to get env and display it
  char **env_names = NULL;
  char **env_values = NULL;
  int len_name = -1;
  int len_value = -1;
  int count = init_env_str(&env_names, &env_values, len_name, len_value);
  for (int i = 0; i < count; i++)
  {
    printf("export %s%s\n", env_names[i], env_values[i]);
    free(env_names[i]);
    free(env_values[i]);
  }
  free(env_names);
  free(env_values);
  return 0;
}

int rm_from_env(char **argv, int pos)
{
  int i = 0;
  while (argv[++pos] != NULL)
  {
    for (i = 0; argv[pos][i] != '=' && argv[pos][i] != '\0'; i++)
      continue;
    char *to_unset = strdup(argv[pos]);
    to_unset[i] = '\0';
    if (rm_env(to_unset) == -1)
    {
      free(to_unset);
      return -3;
    }
    free(to_unset);
  }
  return 0;
}

int builtin_export(char **argv)
{
  int ret = 0;
  int pos = 0;
  //First value is for -n and 2nd is for -p
  int opt[2] = {0, 0};
  check_exp_args(&ret, opt, argv);
  if (ret < 0)
    return print_exp_err(ret, NULL);
  pos = opt[0] + opt[1] - 1;
  if (opt[0] == 0)
  {
    while (argv[++pos] != NULL)
    {
      //Adding found names and values to environ after integrity checking
      if (is_valid_name(argv[pos]) == 1)
      {
        char *valid_str = strdup(argv[pos]);
        ret = add_env(valid_str);
      }
      else
        return print_exp_err(-2, argv[pos]);
    }
  }
  //if -n then remove name frome env
  if (opt[0] != 0)
    ret = rm_from_env(argv, pos);
  //if -p or no args then print env
  if ((argv[0] == NULL || opt[1] != 0) && ret >= 0)
    ret = print_env();
  return print_exp_err(ret, NULL);
}
