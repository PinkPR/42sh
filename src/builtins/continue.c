#include "builtin.h"

int builtin_continue(char **args)
{
  args++;
  args--;

  return MNB_CONTINUE;
}
