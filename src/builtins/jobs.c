#include "builtin.h"
#include "jobs.h"
#include "main.h"

static int check_arguments(char **args)
{
  int l = 0;
  if (args[0])
  {
    l = !strcmp("-l", args[0]);
    if (l == 0)
    {
      write(STDOUT_FILENO, "42sh: jobs: ", 12);
      write(STDOUT_FILENO, args[0], strlen(args[0]));
      write(STDOUT_FILENO, ": invalid parameter\n", 20);
      return -1;
    }
    if (args[1])
    {
      write(STDOUT_FILENO, "42sh: jobs: too many arguments\n", 31);
      return -1;
    }
  }
  return l;
}

static int has_final_newline(char *str)
{
  int pos = 0;
  int nl = 0;
  while (str[pos])
  {
    if (str[pos] == '\n')
      nl = 1;
    else if (str[pos] >= 32 && str[pos] <= 126)
      nl = 0;
    pos++;
  }
  return nl;
}

static void print_extended(void)
{
  s_job *job = g_control->jobs;
  int n = 0;
  printf("\n");
  while (job)
  {
    n++;
    long int ld = job->pgid;
    if (job->completed || job->stopped)
      printf("[%d] %6ld suspended  %s", n, ld, job->command);
    else
      printf("[%d] %6ld running    %s", n, ld, job->command);
    if (!has_final_newline(job->command))
      printf("\n");
    job = job->next;
  }
}

static void print_standard(void)
{
  s_job *job = g_control->jobs;
  int n = 0;
  printf("\n");
  while (job)
  {
    n++;
    if (job->completed || job->stopped)
      printf("[%d]\t suspended  %s", n, job->command);
    else
      printf("[%d]\t running    %s", n, job->command);
    if (!has_final_newline(job->command))
      printf("\n");
    job = job->next;
  }
}

int builtin_jobs(char **args)
{
  int l = check_arguments(args);
  if (l == -1)
    return 1;
  if (g_control->jobs == NULL)
  {
    write(STDOUT_FILENO, "No current jobs.\n", 17);
    return 1;
  }
  if (l)
    print_extended();
  else
    print_standard();
  return 0;
}

static int get_int(char **args)
{
  int res = 0;
  if (args[0])
  {
    int i = 0;
    while (args[0][i])
    {
      if (args[0][i] < '0' || args[0][i] > '9')
      {
        write(STDOUT_FILENO, "42sh: numeric argument expected.\n", 33);
        return -2;
      }
      res = res * 10 + args[0][i] - '0';
      ++i;
    }
    if (args[1])
    {
      write(STDOUT_FILENO, "42sh: too many arguments.\n", 25);
      return -2;
    }
    return res;
  }
  return -1;
}

static int invalid_jobs(int n)
{
  if (n == -2)
    return 1;
  if (n == 0)
  {
    write(STDOUT_FILENO, "42sh: no job numbered 0.\n", 25);
    return 1;
  }
  s_job *job = g_control->jobs;
  if (job == NULL)
  {
    write(STDOUT_FILENO, "42sh: no background jobs.\n", 26);
    return 1;
  }
  return 0;
}

int builtin_wait(char **args)
{
  int n = get_int(args);
  if (invalid_jobs(n))
    return 1;
  s_job *job = g_control->jobs;
  if (n == -1)
  {
    while (job->next)
      job = job->next;
    wait_job(job);
    return 0;
  }
  for (int i = 1; i < n; ++i)
  {
    job = job->next;
    if (job == NULL)
    {
      write(STDOUT_FILENO, "42sh: wait: job does not exist.\n", 32);
      return 1;
    }
  }
  wait_job(job);
  return 0;
}

int builtin_bg(char **args)
{
  int n = get_int(args);
  if (invalid_jobs(n))
    return 1;
  s_job *job = g_control->jobs;
  if (n == -1)
  {
    while (job->next)
      job = job->next;
    job_to_background(job, 1);
    return 0;
  }
  for (int i = 1; i < n; ++i)
  {
    job = job->next;
    if (job == NULL)
    {
      write(STDOUT_FILENO, "42sh: bg: job does not exist.\n", 30);
      return 1;
    }
  }
  job_to_background(job, 1);
  return 0;
}

int builtin_fg(char **args)
{
  int n = get_int(args);
  if (invalid_jobs(n))
    return 1;
  s_job *job = g_control->jobs;
  if (n == -1)
  {
    while (job->next)
      job = job->next;
    format_job_info(job, "running");
    g_main.cur_job_pid = job->pid;
    job_to_foreground(job, 1);
    return 0;
  }
  for (int i = 1; i < n && job; ++i)
    job = job->next;
  if (job == NULL)
  {
    write(STDOUT_FILENO, "42sh: bg: job does not exist.\n", 30);
    return 1;
  }
  format_job_info(job, "running");
  g_main.cur_job_pid = job->pid;
  job_to_foreground(job, 1);
  return 0;
}
