#include "environ.h"
#include "builtin.h"
#include "history.h"

static int invalid_option(char *str)
{
  write(STDERR_FILENO, "42sh: history: ", 15);
  write(STDERR_FILENO, str, strlen(str));
  write(STDERR_FILENO, ": invalid option\n", 17);
  write(STDERR_FILENO, "history: usage: history [-c]", 28);
  write(STDERR_FILENO, " or history -r [filename]", 25);
  write(STDERR_FILENO, " or history [n]\n", 16);
  return 2;
}

static void numeric_required(char *str)
{
  write(STDERR_FILENO, "42sh: history: ", 15);
  write(STDERR_FILENO, str, strlen(str));
  write(STDERR_FILENO, ": numeric argument required\n", 28);
}

static int my_atoi(char **args, int i)
{
  char *str = args[i];
  int val = 0;
  for (int i = 0; str[i]; ++i)
  {
    if (str[i] < '0' || str[i] > '9')
    {
      numeric_required(str);
      return -1;
    }
    val *= 10;
    val += str[i] - '0';
  }
  if (args[i + 1])
  {
    write(STDOUT_FILENO, "42sh: history: too many arguments\n", 34);
    return 1;
  }
  return val;
}

static void display_history_list(int n, s_chain *pos)
{
  while (pos)
  {
    printf("%5d  %s\n", n, pos->str);
    n++;
    pos = pos->prev;
  }
}

static int display_history(int num)
{
  s_chain *pos = g_history->h_base;
  int n = 1;
  while (pos && pos->next)
  {
    pos = pos->next;
    n++;
  }
  if (num == 0)
    display_history_list(1, pos);
  else
  {
    for (int i = 0; i < n - num && pos->prev; ++i)
      pos = pos->prev;
    display_history_list(n - num + 1, pos);
  }
  return 0;
}

static int clear_history(void)
{
  s_chain *pos = g_history->h_base;
  while (pos)
  {
    s_chain *next = pos->next;
    free(pos->str);
    free(pos);
    pos = next;
  }
  g_history->h_base = NULL;
  return 0;
}

static char *get_line(int fd, int *pos, int max)
{
  lseek(fd, *pos, SEEK_SET);
  int size = 64;
  int old = 0;
  int r = (*pos == max);
  char *buf = malloc(sizeof (char) * size);
  while (r == 0)
  {
    read(fd, buf + old, size - old);
    for (int i = old; i < size; ++i)
      if (buf[i] == '\n' || buf[i] == '\0')
      {
        *pos += i + 1;
        r = 1;
        buf[i] = '\0';
        break;
      }
    if (r == 0)
    {
      old = size;
      size *= 2;
      buf = realloc(buf, sizeof (char) * size);
    }
  }
  return buf;
}

static int hist_size(void)
{
  if (get_var("HISTSIZE"))
  {
    int pos = 0;
    int tot = 0;
    char *num = get_var("HISTSIZE");
    while (num[pos])
    {
      tot *= 10;
      tot += num[pos] - '0';
      pos++;
    }
    return tot - 1;
  }
  return 499;
}

static int load_history(char *file)
{
  clear_history();
  if (file == NULL)
  {
    free(g_history);
    init_history();
    return 0;
  }
  g_history->h_base = NULL;
  g_history->h_cur = NULL;
  int fd = open(file, O_RDWR, 0777);
  if (fd <= 0)
    return 0;
  int pos = 0;
  int end = lseek(fd, 0, SEEK_END);
  int num = 0;
  int max = hist_size();
  while (pos < end && num < max)
  {
    char *str = get_line(fd, &pos, end);
    add_to_history(str);
    free(str);
    num++;
  }
  close(fd);
  return 0;
}

int builtin_history(char **args)
{
  int r = 0;
  char *file = NULL;
  int c = 0;
  int num = 0;
  for (int i = 0; args[i]; ++i)
  {
    if (!strcmp("-r", args[i]))
    {
      r = 1;
      file = args[i + 1];
    }
    else if (!strcmp("-c", args[i]))
      c = 1;
    else if (args[i][0] == '-')
      return invalid_option(args[i]);
    else
      num = my_atoi(args, i);
  }
  if (num == -1)
    return 1;
  if (c)
    return clear_history();
  if (r)
    return load_history(file);
  return display_history(num);
}
