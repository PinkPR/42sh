#include <string.h>
#include "builtin.h"
#include "environ.h"

static int is_assign(char *str)
{
  int i = 0;
  while (str[i] && str[i] != ' ')
  {
    if (str[i] == '=')
      return 1;
    i++;
  }
  return 0;
}

int builtin_alias(char **args)
{
  int ret = 0;
  if (!args[0])
    print_alias(NULL);
  else
  {
    for (int i = 0; args[i]; i++)
    {
      if (is_assign(args[i]))
        add_alias(args[i]);
      else
        ret = print_alias(args[i]);
    }
  }
  return ret;
}

int builtin_unalias(char **args)
{
  if (!args[0])
  {
    printf("unalias: usage: unalias [-a] name [name ...]\n");
    return 2;
  }
  else
  {
    int a = !strcmp("-a", args[0]);
    if (a)
      remove_alias(NULL);
    else
    {
      for (int i = 0; args[i]; i++)
      {
        if (!remove_alias(args[i]))
          printf("unalias: %s: not found\n", args[i]);
      }
    }
  }
  return 0;
}
