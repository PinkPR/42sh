#include "builtin.h"

int builtin_break(char **args)
{
  args++;
  args--;

  return MNB_BREAK;
}
