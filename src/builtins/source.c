#include <fcntl.h>
#include <unistd.h>
#include "main.h"
#include "builtin.h"

int read_and_execute_from_fd(int fd)
{
  int res = 0;
  int len = 4096;
  char *str = NULL;
  int prevlen = 0;
  int rd = 4096;
  while (rd == 4096)
  {
    str = realloc(str, len);
    for (int i = prevlen; i < len; ++i)
      str[i] = '\0';
    rd = read(fd, str + prevlen, len - prevlen);
    prevlen = len;
    len *= 2;
  }
  char *tmp = str;
  res = build_ast_and_run(str, 2);
  free(tmp);
  return res;
}

int builtin_source(char **args)
{
  for (int i = 0; args[i]; ++i)
  {
    int fd = open(args[i], O_RDONLY);
    if (fd > 0)
    {
      read_and_execute_from_fd(fd);
      close(fd);
    }
    else
    {
      write(STDOUT_FILENO, "42sh: source: ", 14);
      write(STDOUT_FILENO, args[i], strlen(args[i]));
      perror(" ");
      return 1;
    }
  }
  if (args[0] == NULL)
    return 2;
  return 0;
}
