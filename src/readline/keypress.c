#include "readline.h"

int my_putchar(int ch)
{
  char c = ch;
  return write(STDOUT_FILENO, &c, 1);
}

void curs_right(void)
{
  if (g_term->pos < g_term->len)
  {
    g_term->pos++;
    if ((g_term->pos + g_term->prompt_len) % g_term->col == 0)
    {
      g_term->cur_line++;
      tputs(tgetstr("do", NULL), 1, my_putchar);
    }
    else
      tputs(tgetstr("nd", NULL), 1, my_putchar);
  }
}

void curs_left(void)
{
  if (g_term->pos > 0)
  {
    g_term->pos--;
    if ((g_term->pos + g_term->prompt_len + 1) % g_term->col == 0)
    {
      g_term->cur_line--;
      tputs(tgetstr("up", NULL), 1, my_putchar);
      for (int i = 0; i < g_term->col - 1; ++i)
        tputs(tgetstr("nd", NULL), 1, my_putchar);
    }
    else
      tputs(tgetstr("le", NULL), 1, my_putchar);
  }
}

static void check_arrow_keys(void)
{
  char c;
  tputs(tgetstr("ve", NULL), 1, my_putchar);
  read(1, &c, 1);
  if (c != '[')
    return;
  read(1, &c, 1);
  if (c == 'A')       // UP (for history)
    move_history_and_update(1);
  else if (c == 'B')  // DOWN (for history)
    move_history_and_update(-1);
  else if (c == 'C')  // RIGHT
    curs_right();
  else if (c == 'D')  // LEFT
    curs_left();
  else if (c == '3')
  {
    read(1, &c, 1);
    if (c == '~')     // XTERM HARDCODE FOR DEL
      del_char();
  }
}

static void check_overflow(void)
{
  if (g_term->len >= g_term->max_len - 1)
  {
    g_term->max_len *= 2;
    g_term->command = realloc(g_term->command, g_term->max_len);
  }
  if (g_term->len == 0)
  {
    for (int i = 0; i < g_term->max_line - g_term->cur_line; ++i)
      tputs(tgetstr("up", NULL), 1, my_putchar);
    int tostart = (g_term->prompt_len) % g_term->col + 1;
    for (int i = 0; i < tostart; ++i)
      tputs(tgetstr("le", NULL), 1, my_putchar);
    for (int i = 0; i < (g_term->prompt_len) % g_term->col; ++i)
      tputs(tgetstr("nd", NULL), 1, my_putchar);
  }
}

void insert_char(char c)
{
  if (c < 32 || c > 126)        // NON-PRINTABLE
    return;
  check_overflow();
  for (int i = g_term->len - 1; i >= g_term->pos; --i)
    g_term->command[i + 1] = g_term->command[i];
  g_term->command[g_term->pos] = c;
  g_term->pos++;
  if ((g_term->pos + g_term->prompt_len) % g_term->col == 0)
    g_term->cur_line++;
  g_term->len++;
  if ((g_term->len + g_term->prompt_len) % g_term->col == 0)
    g_term->max_line++;
  int pos = g_term->pos;
  write(STDOUT_FILENO, g_term->command + pos - 1, g_term->len - pos + 1);
  write(STDOUT_FILENO, " ", 1);
  tputs(tgetstr("cd", NULL), 1, my_putchar);
  for (int i = 0; i < g_term->max_line - g_term->cur_line; ++i)
    tputs(tgetstr("up", NULL), 1, my_putchar);
  int tostart = (g_term->len + g_term->prompt_len) % g_term->col + 1;
  for (int i = 0; i < tostart; ++i)
    tputs(tgetstr("le", NULL), 1, my_putchar);
  for (int i = 0; i < (pos + g_term->prompt_len) % g_term->col; ++i)
    tputs(tgetstr("nd", NULL), 1, my_putchar);
}

static void execute_command(void)
{
  g_term->command[g_term->len] = '\n';
  g_term->command[g_term->len + 1] = '\0';
  write(STDOUT_FILENO, g_term->command, g_term->len + 1);
  tcsetattr(STDIN_FILENO, TCSANOW, &(g_term->base_attr));
}

int check_key(char c, int col_change)
{
  if (c == 9)           // TAB
    autocomplete_start(g_term->command);
  else
  {
    g_term->current_tab = NULL;
    g_term->displayed = 0;
    if (c == '\n')
    {
      cursor_to_prompt();
      execute_command();
      return 1;
    }
    else if (c == 27)     // ESC
      check_arrow_keys();
    else if (c == 8 || c == 127)  // BACKSPACE OR DEL
      delete_char();
    else if (!ctrl_modifier(c))
      insert_char(c);
    if (col_change)
    {
      cursor_to_start();
      display_and_position_cursor();
    }
  }
  return 0;
}
