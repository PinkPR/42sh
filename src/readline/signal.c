#include "readline.h"
#include "main.h"
#include "prompt.h"
#include "jobs.h"

int get_last_line_len(char *str)
{
  int len = 1;
  int i = 0;
  while (str[i])
  {
    if (str[i] == 27) // ESC
    {
      while (str[i] && str[i] != 'm')
        ++i;
      if (str[i] == 0)
        return len;
    }
    ++i;
    if (str[i] >= 32 && str[i] <= 126)
      ++len;
    if (str[i] == 10)   // Newline
      len = 0;
  }
  return len;
}

void sigtstp_handler(int num)
{
  if (getpid() != g_control->shell_pgid)
    g_main.cur_job_pid = getpid();
  num++;
  if (!isatty(STDIN_FILENO) || g_main.cur_job_pid == 0)
    return;
  long int ld = g_main.cur_job_pid;
  s_job *job = g_control->jobs;
  while (job && job->pid != g_main.cur_job_pid)
    job = job->next;
  if (job)
  {
    job->stopped = 1;
    fprintf(stderr, "\n%ld (%s): %s", ld, "suspended", job->command);
  }
  else
  {
    fprintf(stderr, "\n%ld (%s): %s", ld, "suspended", g_main.command);
    add_new_job(g_main.cur_job_pid, g_main.command);
  }
  if (g_main.cur_job_pid != getpid())
    kill(g_main.cur_job_pid, SIGTSTP);
}

void sigint_handler(int num)
{
  num++;
  num--;
  cursor_to_last();
  g_term->pos = 0;
  g_term->len = 0;
  g_main.PS2 = 0;
  free(g_term->prompt);
  g_term->prompt = expanded_PS1_prompt();
  g_term->prompt_len = get_last_line_len(g_term->prompt);
  g_term->prompt_height = 0;
  for (int i = 0; i < g_term->prompt_len; ++i)
    if (g_term->prompt[i] == 10)              // Newline
      g_term->prompt_height++;
  g_term->cur_line = g_term->prompt_height;
  g_term->max_line = g_term->prompt_height;
  write(STDOUT_FILENO, "\n", 1);
  write(STDERR_FILENO, g_term->prompt, strlen(g_term->prompt));
}

void empty_signal(int num)
{
  num++;
  num--;
  write(STDOUT_FILENO, "\n", 1);
}
