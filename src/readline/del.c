#include "readline.h"

void del_char(void)
{
  if (g_term->pos < g_term->len)
  {
    int pos = g_term->pos;
    for (int i = pos; i < g_term->len; ++i)
      g_term->command[i] = g_term->command[i + 1];
    g_term->len--;
    if ((g_term->len + g_term->prompt_len) % g_term->col == (g_term->col - 1))
      g_term->max_line--;
    write(STDOUT_FILENO, g_term->command + pos, g_term->len - pos);
    write(STDOUT_FILENO, " ", 1);
    for (int i = 0; i < g_term->max_line - g_term->cur_line; ++i)
      tputs(tgetstr("up", NULL), 1, my_putchar);
    int tostart = (g_term->len + g_term->prompt_len) % g_term->col + 1;
    for (int i = 0; i < tostart; ++i)
      tputs(tgetstr("le", NULL), 1, my_putchar);
    for (int i = 0; i < (g_term->pos + g_term->prompt_len) % g_term->col; ++i)
      tputs(tgetstr("nd", NULL), 1, my_putchar);
  }
}

void delete_char(void)
{
  if (g_term->pos > 0)
  {
    for (int i = g_term->pos - 1; i < g_term->len; ++i)
      g_term->command[i] = g_term->command[i + 1];
    g_term->len--;
    curs_left();
    if ((g_term->len + g_term->prompt_len) % g_term->col == (g_term->col - 1))
      g_term->max_line--;
    int pos = g_term->pos;
    write(STDOUT_FILENO, g_term->command + pos, g_term->len - pos);
    write(STDOUT_FILENO, " ", 1);
    for (int i = 0; i < g_term->max_line - g_term->cur_line; ++i)
      tputs(tgetstr("up", NULL), 1, my_putchar);
    int tostart = (g_term->len + g_term->prompt_len) % g_term->col + 1;
    for (int i = 0; i < tostart; ++i)
      tputs(tgetstr("le", NULL), 1, my_putchar);
    for (int i = 0; i < (pos + g_term->prompt_len) % g_term->col; ++i)
      tputs(tgetstr("nd", NULL), 1, my_putchar);
  }
}
