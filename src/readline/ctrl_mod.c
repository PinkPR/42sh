#include "readline.h"
#include "history.h"

void opti_display_and_position_cursor(void)
{
  write(STDOUT_FILENO, g_term->command, g_term->len);
  write(STDOUT_FILENO, " ", 1);
  tputs(tgetstr("cd", NULL), 1, my_putchar);
  for (int i = 0; i < g_term->max_line - g_term->cur_line; ++i)
    tputs(tgetstr("up", NULL), 1, my_putchar);
  int tostart = (g_term->len + g_term->prompt_len) % g_term->col + 1;
  for (int i = 0; i < tostart; ++i)
    tputs(tgetstr("le", NULL), 1, my_putchar);
  for (int i = 0; i < (g_term->pos + g_term->prompt_len) % g_term->col; ++i)
    tputs(tgetstr("nd", NULL), 1, my_putchar);
}

void cursor_to_prompt(void)
{
  int curx = (g_term->pos + g_term->prompt_len) % g_term->col;
  int tarx = g_term->prompt_len;
  for (int i = 0; i < curx - tarx; ++i)
    tputs(tgetstr("le", NULL), 1, my_putchar);
  for (int i = 0; i < tarx - curx; ++i)
    tputs(tgetstr("ri", NULL), 1, my_putchar);
  for (int i = 0; i < g_term->cur_line - g_term->prompt_height; ++i)
    tputs(tgetstr("up", NULL), 1, my_putchar);
}

static void delete_to_last(void)
{
  cursor_to_prompt();
  g_term->len = g_term->pos;
  g_term->max_line = g_term->cur_line;
  opti_display_and_position_cursor();
}

static void cursor_to_first(void)
{
  cursor_to_prompt();
  g_term->pos = 0;
  g_term->cur_line = g_term->prompt_height;
  opti_display_and_position_cursor();
}

void cursor_to_last(void)
{
  cursor_to_prompt();
  g_term->pos = g_term->len;
  g_term->cur_line = g_term->max_line;
  opti_display_and_position_cursor();
}

static void reset_text_metadata(void)
{
  g_term->pos = 0;
  g_term->len = 0;
  g_term->max_line = g_term->prompt_height;
  g_term->cur_line = g_term->prompt_height;
}

void move_history_and_update(int dir)
{
  cursor_to_prompt();
  if (g_history->h_cur == NULL)
  {
    for (int i = 0; i < g_term->len; ++i)
      g_history->saved_command[i] = g_term->command[i];
    g_history->saved_command[g_term->len] = '\0';
  }
  move_history(dir);
  reset_text_metadata();
  if (g_history->h_cur)
  {
    char *str = g_history->h_cur->str;
    int len = strlen(str);
    if (str[len - 1] == '\n' || str[len - 1] == ' ')
      len--;
    for (int i = 0; i < len; ++i)
      insert_char(str[i]);
    return;
  }
  if (g_history->saved_command[0] == '\0')
    tputs(tgetstr("cd", NULL), 1, my_putchar);
  for (int i = 0; g_history->saved_command[i]; ++i)
    insert_char(g_history->saved_command[i]);
}

static void clear_the_screen(void)
{
  tputs(tgetstr("cl", NULL), 1, my_putchar);
  display_and_position_cursor();
}

int ctrl_modifier(char c)
{
  if (c == 1)  // CTRL-A
    cursor_to_first();
  else if (c == 6)  // CTRL-F
    curs_right();
  else if (c == 2)  // CTRL-B
    curs_left();
  else if (c == 16)  // CTRL-P
    move_history_and_update(1);
  else if (c == 14)  // CTRL-N
    move_history_and_update(-1);
  else if (c == 5)  // CTRL-E
    cursor_to_last();
  else if (c == 11)  // CTRL-K
    delete_to_last();
  else if (c == 12)  // CTRL-L
    clear_the_screen();
  else if (c == 21) // CTRL-U
    sigint_handler(0);
  else
    return 0;
  return 1;
}
