#include "readline.h"
#include "history.h"
#include "environ.h"
#include "main.h"
#include "prompt.h"
#include "builtin.h"
#include "jobs.h"

s_readl *g_term;

static void kill_background_process(void)
{
  signal(SIGCHLD, SIG_DFL);
  s_job *job = g_control->jobs;
  while (job)
  {
    kill(job->pid, SIGKILL);
    kill(job->pid + 1, SIGKILL);
    job = job->next;
  }
}

void reset_term(void)
{
  tputs(tgetstr("ve", NULL), 1, my_putchar);
  if (g_term)
  {
    if (g_control->shell_pgid == getpid())
      tcsetattr(STDIN_FILENO, TCSANOW, &(g_term->base_attr));
    free(g_term->command);
    if (g_term->prompt)
      free(g_term->prompt);
    free(g_term);
  }
  if (!g_control)
    return;
  if (g_control->shell_pgid == getpid())
    kill_background_process();
  s_job *nextjob;
  s_job *job = g_control->jobs;
  while (job)
  {
    nextjob = job->next;
    free_job(job);
    job = nextjob;
  }
  free_cmds();
  free(g_control);
  free_g_main();
}

static void init_readline(void)
{
  g_term = malloc(sizeof (s_readl));
  g_term->current_tab = NULL;
  struct termios attr;
  tcgetattr(STDIN_FILENO, &attr);
  g_term->base_attr = attr;
  g_term->notified = 0;
  attr.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &attr);
  char *termtype = getenv("TERM");
  if (termtype == 0)
    write(2, "Specify a terminal type with `setenv TERM <yourtype>'.\n", 55);
  int success = tgetent(NULL, termtype);
  if (success < 0)
    write(2, "Could not access the termcap data base.\n", 40);
  if (success == 0)
  {
    write(2, "Terminal type `", 15);
    write(2, termtype, strlen(termtype));
    write(2, "is not defined.\n", 16);
  }
}

static int update_column(void)
{
  int new_col = tgetnum("co");
  if (g_term->col != new_col)
  {
    g_term->cur_line = g_term->prompt_height;
    g_term->max_line = g_term->prompt_height;
    g_term->max_line += (g_term->len + g_term->prompt_len) / new_col;
    g_term->cur_line += (g_term->pos + g_term->prompt_len) / new_col;
    g_term->col = new_col;
    return 1;
  }
  g_term->col = new_col;
  return 0;
}

static void set_prompt_info(int incomplete)
{
  if (incomplete)
    g_term->prompt = expanded_PS2_prompt();
  else
    g_term->prompt = expanded_PS1_prompt();
  g_term->prompt_len = get_last_line_len(g_term->prompt);
  g_term->prompt_height = 0;
  for (unsigned int i = 0; i < strlen(g_term->prompt); ++i)
    if (g_term->prompt[i] == 10)              // Newline
      g_term->prompt_height++;
  g_term->pos = 0;
  g_term->len = 0;
  g_term->cur_line = g_term->prompt_height;
  g_term->max_line = g_term->prompt_height;
  g_term->command = malloc(sizeof (char) * 1024);
  g_term->max_len = 1024;
  g_term->col = tgetnum("co");
}

void cursor_to_start(void)
{
  for (int i = 0; i < (g_term->pos + g_term->prompt_len) % g_term->col; ++i)
    tputs(tgetstr("le", NULL), 1, my_putchar);
  for (int i = 0; i < g_term->cur_line; ++i)
    tputs(tgetstr("up", NULL), 1, my_putchar);
}

void display_and_position_cursor(void)
{
  write(STDERR_FILENO, g_term->prompt, strlen(g_term->prompt));
  write(STDOUT_FILENO, g_term->command, g_term->len);
  write(STDOUT_FILENO, " ", 1);
  tputs(tgetstr("cd", NULL), 1, my_putchar);
  for (int i = 0; i < g_term->max_line - g_term->cur_line; ++i)
    tputs(tgetstr("up", NULL), 1, my_putchar);
  int tostart = (g_term->len + g_term->prompt_len) % g_term->col + 1;
  for (int i = 0; i < tostart; ++i)
    tputs(tgetstr("le", NULL), 1, my_putchar);
  for (int i = 0; i < (g_term->pos + g_term->prompt_len) % g_term->col; ++i)
    tputs(tgetstr("nd", NULL), 1, my_putchar);
}

static void kill_shell(void)
{
  tputs(tgetstr("ve", NULL), 1, my_putchar);
  close_history();
  free_envir();
  tcsetattr(STDIN_FILENO, TCSANOW, &(g_term->base_attr));
  arg_l_del(g_main.opts);
  arg_l_del(g_main.shopts);
  free(g_term->prompt);
  free(g_term->command);
  free(g_term);
  free(g_main.command);
  bltn_list_free(g_bltn_list);
  g_term = NULL;
  write(STDIN_FILENO, "exit\n", 5);
  exit(0);
}

static void handle_ctrld()
{
  if (g_term->len == 0)
  {
    if (g_main.PS2 == 0)
    {
      if (g_term->notified == 0)
      {
        g_term->notified = 1;
        if (check_jobs() == 0)
          kill_shell();
      }
      else
        kill_shell();
    }
    else
      sigint_handler(0);
  }
  else
    del_char();
}

s_readl *readline(int incomplete)
{
  init_readline();
  set_prompt_info(incomplete);
  write(STDERR_FILENO, g_term->prompt, strlen(g_term->prompt));
  while (1)
  {
    char c;
    g_term->command[g_term->len] = '\0';
    tputs(tgetstr("ve", NULL), 1, my_putchar);
    read(1, &c, 1);
    tputs(tgetstr("vi", NULL), 1, my_putchar);
    if (c == 4)     // CTRL-D
      handle_ctrld();
    else
      g_term->notified = 0;
    if (check_key(c, update_column()))
    {
      free(g_term->prompt);
      g_term->prompt = NULL;
      tcsetattr(STDIN_FILENO, TCSANOW, &(g_term->base_attr));
      tputs(tgetstr("ve", NULL), 1, my_putchar);
      return g_term;
    }
  }
}
