#include <sys/types.h>
#include <sys/stat.h>
#include "path.h"
#include "readline.h"

static char *word_to_complete(char *str)
{
  int i = g_term->pos;
  if ((i == g_term->len || str[i] == ' ') && i > 0)
    --i;
  if (str[i] == ' ' || g_term->len == 0)
    return NULL;
  char c = str[i];
  while (i > 0 && c != ' ' && c != ';' && c != '\n')
  {
    i--;
    c = str[i];
  }
  char *tocomp = malloc(sizeof (char) * (g_term->len - i + 1));
  int j = 0;
  if (str[i] == ' ')
    i++;
  c = str[i];
  while (i + j < g_term->len && c != ' ' && c != ';' && c != '\n')
  {
    tocomp[j] = str[i + j];
    ++j;
    c = str[i + j];
  }
  tocomp[j] = '\0';
  return tocomp;
}

static int is_directory(char *str)
{
  struct stat statbuf;
  FILE *fb = fopen(str, "r");
  if (fb != NULL)
  {
    stat(str, &statbuf);
    fclose(fb);
    if (S_ISDIR(statbuf.st_mode))
      return 1;
    else
      return 0;
  }
  return 0;
}

static void display(int num, int max_len)
{
  int k = 0;
  s_path_l *pos;
  for (pos = g_patharray; pos; pos = pos->next)
  {
    if (k % num == 0)
      write(STDOUT_FILENO, "\n", 1);
    char *s = pos->path;
    if (s[0] == '.')
      s = s + 1;
    if (s[0] == '/')
      s = s + 1;
    ++k;
    write(STDOUT_FILENO, s, strlen(s));
    int d = is_directory(s);
    if (d)
      write(STDOUT_FILENO, "/", 1);
    for (unsigned int i = 0; i < max_len - strlen(s) - d; ++i)
      write(STDOUT_FILENO, " ", 1);
  }
  write(STDOUT_FILENO, "\n", 1);
}

static void display_choices(void)
{
  g_term->displayed = 1;
  s_path_l *pos = g_patharray;
  unsigned int max_len = 0;
  while (pos)
  {
    if (strlen(pos->path) - 1 > max_len)
      max_len = strlen(pos->path) - 1;
    pos = pos->next;
  }
  max_len += 4;
  display(g_term->col / max_len, max_len);
  display_and_position_cursor();
}

static void replace_by_first(char *str, char *tocomp)
{
  if (str[g_term->pos] == ' ')
    curs_left();
  int i;
  if (g_term->pos < 2)
    i = 1;
  else
    i = (str[g_term->pos - 1] != '/' || str[g_term->pos - 2] != '.');

  while (g_term->pos > 0 && str[g_term->pos - 1] != ' ' && i)
  {
    curs_left();
    if (g_term->pos < 2)
      i = 1;
    else
      i = (str[g_term->pos - 1] != '/' || str[g_term->pos - 2] != '.');
  }
  for (unsigned int i = 0; i < strlen(tocomp); ++i)
    del_char();
  char *s = g_patharray->path;
  if (s[0] == '.')
    s = s + 1;
  for (unsigned int i = 0; i < strlen(s); ++i)
    insert_char(s[i]);
  if (is_directory(s))
    insert_char('/');
}

static int get_max_len(int max_len)
{
  int j = 0;
  while (j < max_len)
  {
    char c = g_patharray->path[j];
    s_path_l *pos = g_patharray->next;
    while (pos)
    {
      if (pos->path[j] != c)
        return j;
      pos = pos->next;
    }
    j++;
  }
  return -1;
}

static void choose_next(char *str, char *tocomp)
{
  if (g_patharray == NULL)
    return;
  if (g_term->current_tab == NULL)
    g_term->current_tab = g_patharray;
  else
    g_term->current_tab = g_term->current_tab->next;
  if (g_term->current_tab == NULL)
    g_term->current_tab = g_patharray;
  if (str[g_term->pos] == ' ')
    curs_left();
  int i = (g_term->pos < 2 || str[g_term->pos - 1] != '/'
      || str[g_term->pos - 2] != '.');
  while (g_term->pos > 0 && str[g_term->pos - 1] != ' ' && i)
  {
    curs_left();
    i = (g_term-> pos < 2 || str[g_term->pos - 1] != '/'
        || str[g_term->pos - 2] != '.');
  }
  for (unsigned int i = 0; i < strlen(tocomp); ++i)
    del_char();
  char *s = g_term->current_tab->path;
  if (s[0] == '.')
    s = s + 1;
  for (unsigned int i = 0; i < strlen(s); ++i)
    insert_char(s[i]);
}

static void trick_autocomplete(char *s, char *tocomp, unsigned int final)
{
  for (unsigned int i = 0; i < strlen(tocomp); ++i)
    del_char();
  int l = (s[0] == '.');
  if (s[0] == '.')
    s = s + 1;
  for (unsigned int i = (s[1] == '.'); i < final - l; ++i)
    insert_char(s[i]);
  g_term->current_tab = g_patharray;
  while (g_term->current_tab->next)
    g_term->current_tab = g_term->current_tab->next;
}

static void choose_choice(char *str, char *tocomp)
{
  g_term->displayed = 0;
  char *s = g_patharray->path;
  unsigned int final = get_max_len(strlen(s));
  if (final - 1 != strlen(tocomp))
  {
    g_term->current_tab = NULL;
    if (str[g_term->pos] == ' ')
      curs_left();
    int p = g_term->pos;
    int i = (p < 2 || str[p - 1] != '/' || str[p - 2] != '.');
    while (g_term->pos > 0 && str[g_term->pos - 1] != ' ' && i)
    {
      curs_left();
      p = g_term->pos;
      i = (p < 2 || str[p - 1] != '/' || str[p - 2] != '.');
    }
    trick_autocomplete(s, tocomp, final);
  }
  else
    choose_next(str, tocomp);
}

void autocomplete_start(char *str)
{
  if (g_term->current_tab == NULL)
  {
    if (g_term->pos == 0)
      return;
    char *tocomp = word_to_complete(str);
    if (tocomp == NULL)
      return;
    if (strlen(tocomp) > 1 && tocomp[0] == '.' && tocomp[1] == '/')
      autocomplete(tocomp + 2);
    else
      autocomplete(tocomp);
    if (g_patharray)
    {
      if (g_patharray->next == NULL)
        replace_by_first(str, tocomp);
      else if (g_term->displayed == 0)
        display_choices();
      else
        choose_choice(str, tocomp);
    }
    free(tocomp);
  }
  else
    choose_next(str, g_term->current_tab->path);
}
