#include "expand.h"
#include "history.h"
#include "environ.h"

s_history *g_history;

void move_history(int up)
{
  if (up == 1)        // UP
  {
    if (g_history->h_cur == NULL)
      g_history->h_cur = g_history->h_base;
    else if (g_history->h_cur->next)
      g_history->h_cur = g_history->h_cur->next;
  }
  else if (up == -1)  // DOWN
  {
    if (g_history->h_cur != NULL)
      g_history->h_cur = g_history->h_cur->prev;
  }
}

void add_to_history(char *str)
{
  int len = strlen(str);
  if (len == 0 || str[0] == '\n')
    return;
  char *copy = malloc(sizeof (char) * (len + 1));
  for (int i = 0; i <= len; ++i)
  {
    if (str[i] != '\n')
      copy[i] = str[i];
    else
      copy[i] = ' ';
  }
  s_chain *new = malloc(sizeof (s_chain));
  new->str = copy;
  new->prev = NULL;
  new->next = g_history->h_base;
  if (g_history->h_base)
    g_history->h_base->prev = new;
  g_history->h_base = new;
}

static char *get_line(int fd, int *pos, int max)
{
  lseek(fd, *pos, SEEK_SET);
  int size = 64;
  int old = 0;
  int r = (*pos == max);
  char *buf = malloc(sizeof (char) * size);
  while (r == 0)
  {
    read(fd, buf + old, size - old);
    for (int i = old; i < size; ++i)
      if (buf[i] == '\n' || buf[i] == '\0')
      {
        *pos += i + 1;
        r = 1;
        buf[i] = '\0';
        break;
      }
    if (r == 0)
    {
      old = size;
      size *= 2;
      buf = realloc(buf, sizeof (char) * size);
    }
  }
  return buf;
}

static char *history_file(void)
{
  char *tofree = NULL;
  if (get_var("HISTFILE"))
    tofree = strdup(get_var("HISTFILE"));
  else
    tofree = strdup("~/.42sh_history");
  char *toreturn = expand(tofree);
  free(tofree);
  return toreturn;
}

static int hist_size(void)
{
  if (get_var("HISTSIZE"))
  {
    int pos = 0;
    int tot = 0;
    char *num = get_var("HISTSIZE");
    while (num[pos])
    {
      tot *= 10;
      tot += num[pos] - '0';
      pos++;
    }
    return tot - 1;
  }
  return 499;
}

void init_history(void)
{
  g_history = malloc(sizeof (s_history));
  g_history->h_base = NULL;
  g_history->h_cur = NULL;
  char *file = history_file();
  int fd = open(file, O_RDWR | O_CREAT, 0777);
  free(file);
  int pos = 0;
  int end = lseek(fd, 0, SEEK_END);
  int num = 0;
  int max = hist_size();
  while (pos < end && num < max)
  {
    char *str = get_line(fd, &pos, end);
    add_to_history(str);
    free(str);
    num++;
  }
  close(fd);
}

static void purge_memory(void)
{
  s_chain *pos = g_history->h_base;
  while (pos)
  {
    s_chain *next = pos->next;
    free(pos->str);
    free(pos);
    pos = next;
  }
  free(g_history);
}

static int hist_filesize(void)
{
  if (get_var("HISTFILESIZE"))
  {
    int pos = 0;
    int tot = 0;
    char *num = get_var("HISTFILESIZE");
    while (num[pos])
    {
      tot *= 10;
      tot += num[pos] - '0';
      pos++;
    }
    return tot - 1;
  }
  return 499;
}

void close_history(void)
{
  char *file = history_file();
  int fd = open(file, O_RDWR | O_CREAT | O_TRUNC, 0777);
  free(file);
  s_chain *pos = g_history->h_base;
  if (pos == NULL)
  {
    close(fd);
    return;
  }
  int i = 0;
  int histfilesize = hist_filesize();
  while (i < histfilesize && pos->next != NULL)
  {
    pos = pos->next;
    i++;
  }
  while (pos)
  {
    write(fd, pos->str, strlen(pos->str));
    write(fd, "\n", 1);
    pos = pos->prev;
  }
  close(fd);
  purge_memory();
}
