#include "str_tools.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

char *str_cpy(char *src, char *dst, char sep)
{
  char *tmp = NULL;
  if (!src)
    return NULL;
  if (!dst)
    dst = malloc(str_len(src, sep) + 1);
  else
  {
    if (str_len(src, sep) > str_len(dst, sep))
      dst = realloc(dst, str_len(src, sep) + 1);
  }
  tmp = dst;
  while (*src != '\0' && *src != sep)
    *(tmp++) = *(src++);
  *tmp = '\0';
  return dst;
}

int str_len(char *str, char sep)
{
  if (str)
    return (*str != '\0' && *str != sep) ? str_len(++str, sep) + 1 : 0;
  return -1;
}

int str_cmp(char *str1, char *str2, char sep)
{
  if (str1 && str2)
  {
    while (*str1 != sep && *str2 != sep
           && *str1 != '\0' && *str2 != '\0' && *str1 == *str2)
    {
      ++str1;
      ++str2;
    }
    if (*str1 == sep || *str2 == sep)
      return *(str1 - 1) == *(str2 - 1);
    return *str1 == *str2;
  }
  return 0;
}

char *itoa(long long int i)
{
  int w = 1;
  long long int b = i;
  if (b <= 0)
  {
    w++;
    b *= -1;
  }
  for (long long int a = b; a > 0; a /= 10)
    w++;
  char *str = malloc(sizeof (char) * (w + 1));
  snprintf(str, w, "%lli", i);
  return str;
}
