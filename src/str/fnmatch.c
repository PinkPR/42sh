#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "fnmatch.h"

struct string_l *g_cat_list = NULL;

void clear_cat(void)
{
  struct string_l *pos = g_cat_list;
  while (pos)
  {
    struct string_l *next = pos->next;
    free(pos->str);
    free(pos);
    pos = next;
  }
  g_cat_list = NULL;
}

char *custom_cat(char *s1, char *s2)
{
  int i = 0;
  int j = 0;
  char *new = malloc(strlen(s1) + strlen(s2) + 1);

  for (; s1[i]; i++)
    new[i] = s1[i];

  for (; s2[j]; j++)
    new[i + j] = s2[j];

  new[i + j] = 0;
  struct string_l *add = malloc(sizeof (struct string_l));
  add->str = new;
  add->next = g_cat_list;
  g_cat_list = add;
  return new;
}

static int brace(const char **pattern, const char *string)
{
  int i = 0;
  int r = 0;
  (*pattern)++;
  const char *s = 0;
  if (**pattern == '!' && (*pattern)++)
    i++;
  s = *pattern;
  do {
    if ((**pattern == '-') && (*pattern - s > 0) && (*(*pattern + 1) != ']'))
    {
      if ((*string > *(*pattern - 1)) && (*string < *(*pattern + 1)))
        r = 1;
      (*pattern)++;
    }
    if (**pattern == *string)
      r = 1;
    (*pattern)++;
  } while (**pattern != ']');
  return (i) ? !r : r;
}

int my_fnmatch(const char *pattern, const char *string)
{
  if (!pattern || !string || ((*string != '\0') && (*pattern == '\0')))
    return 1;
  if ((*pattern == '\0' && *string == '\0')
      || (*pattern == '*' && (!my_fnmatch(pattern + 1, string)))
      || (*pattern == '*' && *string != '\0'
          && (!my_fnmatch(pattern, string + 1))))
    return 0;
  if (*pattern == '\\' && *string != '\0')
    return (*(pattern + 1) == *string) ? my_fnmatch(pattern + 2, ++string) : 1;
  if (*pattern == '[')
    return (brace(&pattern, string)) ? my_fnmatch(pattern + 1, string + 1) : 1;
  if (*pattern == *string || (*string != '\0' && *pattern == '?'))
    return my_fnmatch(pattern + 1, string + 1);
  return 1;
}
