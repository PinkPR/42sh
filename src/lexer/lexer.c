#include "lexer.h"
#include "environ.h"
#include "exec.h"
#include "expand.h"

static int is_delim(char *str, int i)
{
  char c = str[i];
  return (c == ' ' || c == '\0' || c == '\n' || c == ';' || c == '/');
}

static int strncmp2(char *str, char *cmp, size_t n, int can_be_stick)
{
  if (strlen(str) < n)
    return 1;
  for (size_t i = 0; i < n; i++)
  {
    if (str[i] != cmp[i])
      return 1;
  }
  if (can_be_stick)
    return 0;
  if (is_delim(str, n))
    return 0;
  else
    return 1;
}

static struct node *node_str(enum rule rule, enum token token,
                             char **str, int count)
{
  struct node *node = new_node(rule, token, strndup(*str, count));
  if (**str != 0)
    *str += count;
  return node;
}

static struct node *tokenize_word(char **str, enum rule rule)
{
  int nb = 0;
  struct node *node = new_node(0, WORD, get_word(*str, &nb));
  char *alias = get_alias(node->token_value);
  if (alias && (rule == 11 || rule == -1))
  {
    *str += nb - 1;
    *str[0] = '#';
    *str = replace(*str, "#", expand_variables(alias));
    return tokenize(str, rule);
  }
  *str += nb;
  return node;
}


static struct node *tokenize_key2(char **str, enum rule rule)
{
  if (!strncmp2(*str, "fi", 2, 0) && rule && rule != 10)
    return node_str(rule, FI, str, 2);
  else if (!strncmp2(*str, "esac", 4, 0) && rule == 19)
    return node_str(rule, ESAC, str, 4);
  else if (!strncmp2(*str, "while", 5, 0) && rule && rule != 10)
    return node_str(RULE_WHILE, WHILE, str, 5);
  else if (!strncmp2(*str, "until", 5, 0) && rule && rule != 10)
    return node_str(RULE_UNTIL, UNTIL, str, 5);
  else if (!strncmp2(*str, "case", 4, 0) && rule && rule != 10)
    return node_str(CASE_CLAUSE, CASE, str, 4);
  else
    return tokenize_word(str, rule);
}

static struct node *tokenize_key(char **str, enum rule rule)
{
  int nb = 0;
  if (is_ionumber(*str, &nb))
    return node_str(rule, IONUMBER, str, nb);
  else if (!strncmp2(*str, "if", 2, 0) && rule && rule != 10)
    return node_str(RULE_IF, IF, str, 2);
  else if (!strncmp2(*str, "else", 4, 0) && rule && rule != 10)
    return node_str(ELSE_CLAUSE, ELSE, str, 4);
  else if (!strncmp2(*str, "elif", 4, 0) && rule && rule != 10)
    return node_str(ELSE_CLAUSE, ELIF, str, 4);
  else if (!strncmp2(*str, "then", 4, 0) && rule && rule != 10)
    return node_str(rule, THEN, str, 4);
  else if (!strncmp2(*str, "for", 3, 0) && rule && rule != 10)
    return node_str(RULE_FOR, FOR, str, 3);
  else if (!strncmp2(*str, "in", 2, 0) && rule && rule != 10)
    return node_str(rule, IN, str, 2);
  else if (!strncmp2(*str, "done", 4, 0) && rule && rule != 10)
    return node_str(rule, DONE, str, 4);
  else if (!strncmp2(*str, "do", 2, 0) && rule && rule != 10)
    return node_str(rule, DO, str, 2);
  return tokenize_key2(str, rule);
}

static struct node *tokenize_other(char **str, enum rule rule)
{
  struct node *node = NULL;
  int nb = 0;
  if (*str[0] == ')')
    return node_str(rule, RIGHT_PAR, str, 1);
  else if (*str[0] == '>')
    return node_str(rule, SIMPLE_R_CHEV, str, 1);
  else if (*str[0] == '<')
    return node_str(rule, SIMPLE_L_CHEV, str, 1);
  else if (*str[0] == ';')
    return node_str(rule, SEMI_COLON, str, 1);
  else if (rule == REDIR_HEREDOC)
    node = new_node(rule, HEREDOC, get_word(*str, &nb));
  else if (is_assign(*str))
  {
    if (rule && rule != 10)
      node = new_node(rule, ASSIGNMENT_WORD, get_assignement(*str, &nb));
    else
      node = new_node(rule, WORD, get_assignement(*str, &nb));
  }
  else
    return tokenize_key(str, rule);
  if (nb > 0)
    *str += nb;
  return node;
}

static struct node *tokenize_single(char **str, enum rule rule)
{
  if (!strncmp2(*str, "||", 2, 1))
    return node_str(AND_OR, DOUBLE_OR, str, 2);
  else if (!strncmp2(*str, "function", 8, 0) && rule && rule != 10)
    return node_str(FUNCDEC, FUNC_DEC, str, 8);
  else if (*str[0] == '\n')
    return node_str(rule, EOL, str, 1);
  else if (*str[0] == '\0')
    return node_str(rule, ENDOF, str, 1);
  else if (*str[0] == '&')
    return node_str(rule, SIMPLE_AND, str, 1);
  else if (*str[0] == '|')
    return node_str(rule, PIPE, str, 1);
  else if (*str[0] == '!')
    return node_str(rule, NOT, str, 2);
  else if (*str[0] == '{')
    return node_str(rule, LEFT_BRACE, str, 1);
  else if (*str[0] == '}')
    return node_str(rule, RIGHT_BRACE, str, 1);
  else if (*str[0] == '(')
    return node_str(rule, LEFT_PAR, str, 1);
  return tokenize_other(str, rule);
}

static struct node *tokenize_str(char **str, enum rule rule)
{
  if (!strncmp2(*str, "<<-", 3, 1))
    return node_str(REDIR_HEREDOC, HEREDOC_CHE, str, 3);
  else if (!strncmp2(*str, "<<", 2, 1))
    return node_str(REDIR_HEREDOC, DOUBLE_L_CHEV, str, 2);
  else if (!strncmp2(*str, ">>", 2, 1))
    return node_str(REDIRECTION, DOUBLE_R_CHEV, str, 2);
  else if (!strncmp2(*str, "<&", 2, 1))
    return node_str(REDIRECTION, L_CHEV_AND, str, 2);
  else if (!strncmp2(*str, ">&", 2, 1))
    return node_str(REDIRECTION, R_CHEV_AND, str, 2);
  else if (!strncmp2(*str, ">|", 2, 1))
    return node_str(REDIRECTION, R_CHEV_PIPE, str, 2);
  else if (!strncmp2(*str, "<>", 2, 1))
    return node_str(REDIRECTION, INVERT_CHEV, str, 2);
  else if (!strncmp2(*str, ";;", 2, 1))
    return node_str(CASE_CLAUSE, DOUBLE_SEMI_COLON, str, 2);
  else if (!strncmp2(*str, "&&", 2, 1))
    return node_str(AND_OR, DOUBLE_AND, str, 2);
  return tokenize_single(str, rule);
}

struct node *tokenize(char **str, enum rule rule)
{
  while (*str[0] == ' ')
    (*str)++;
  if (*str[0] == '#')
  {
    while (*str[0] != '\n')
      (*str)++;
  }
  return tokenize_str(str, rule);
}
