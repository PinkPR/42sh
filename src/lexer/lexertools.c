#include "lexer.h"
#include <stdio.h>

static int is_delim(char *str, int i, char d)
{
  char c = str[i];
  int back = i > 0 && str[i - 1] == '\\';
  if (d == ' ')
  {
    return (back || (c != ' ' && c != '\n' && c != 0 && c != ';' && c != ')'
            && c != '(' && c != '<' && c != '>' && c != '&' && c != '|'
            && ((i > 0 && str[i - 1] == '$') || c != '!')));
  }
  if (c == 0)
    return -1;
  else if (d == 'A')
    return c != ')' && str[i + 1] != ')';
  return c != d;
}

static char get_end(char *str, int i, char end)
{
  if (str[i] == '$' && str[i + 1] == '(' && str[i + 2] == '(')
    end = 'A';
  else if (str[i] == ')' && str[i - 1] == ')')
    end = ' ';
  else if (str[i] == '$' && str[i + 1] == '(')
    end = ')';
  else if (end != 'A' && str[i] == ')')
    end = ' ';
  else if (str[i] == '"' || str[i] == '\'' || str[i] == '`')
  {
    if (end == ' ')
      end = str[i];
    else
      end = ' ';
  }
  return end;
}

char *get_word(char *str, int *nb)
{
  char *ret = malloc(sizeof (char) * 4);
  char end = ' ';
  int i = 0;

  while (is_delim(str, i, end) || end != ' ')
  {
    if (!str[i])
      break;
    end = get_end(str, i, end);
    ret[i] = str[i];
    i++;
    ret = realloc(ret, sizeof (char) * (i + 2));
  }
  ret[i] = 0;
  *nb += i;
  return ret;
}

int is_assign(char *str)
{
  int i = 0;
  if ((str[0] >= '0' && str[0] <= '9') || str[0] == '-' || str[0] == '?'
      || str[0] == '$' || str[0] == '"' || str[0] == '\'')
    return 0;
  while (is_delim(str, i, ' '))
  {
    if (str[i] == '=')
      return str[i + 1] != ' ';
    i++;
  }
  return 0;
}

char *get_assignement(char *str, int *nb)
{
  char *ret = malloc(sizeof (char) * 4);
  size_t i = 0;
  while (str[i] != '=')
    i++;
  str[i] = 0;
  i = 0;
  char *key = get_word(str, nb);
  ret = realloc(ret, sizeof (char) * (strlen(key) + 1));
  for (i = 0; i < strlen(key); i++)
    ret[i] = key[i];
  ret[i++] = '=';
  *nb += 1;
  free(key);
  key = get_word(str + i, nb);
  ret = realloc(ret, sizeof (char) * (i + strlen(key)));
  size_t j = 0;
  for (j = 0; j < strlen(key); j++)
    ret[i + j] = key[j];
  ret = realloc(ret, sizeof (char) * (i + j + 1));
  ret[i + j] = 0;
  free(key);
  return ret;
}

static int get_int(char *str, int *d)
{
  char *nb = malloc(sizeof (char) * strlen(str));
  int i = 0;
  while (str[i] >= '0' && str[i] <= '9')
  {
    nb[i] = str[i];
    *d += 1;
    i++;
  }
  nb[i] = 0;
  int ret = atoi(nb);
  free(nb);
  return ret;
}

int is_ionumber(char *str, int *d)
{
  *d = 0;
  int nb = get_int(str, d);
  if (nb >= 0 && *d > 0)
  {
    if (str[*d] == '<' || str[*d] == '>')
      return 1;
  }
  return 0;
}
