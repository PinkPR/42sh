#include "prompt.h"

static void get_date(char *ret, int *pos)
{
  time_t cur_time;
  time(&cur_time);
  char *date = ctime(&cur_time);
  int i = 0;
  int num = 0;
  while (num != 3)
  {
    i++;
    if (date[i] == ' ')
      num++;
  }
  date[i] = '\0';
  int len = strlen(date);
  memcpy(ret + *pos, date, len);
  *pos += len;
}

static void get_hostname(int full, char *ret, int *pos)
{
  char *buf = malloc(sizeof (char) * 64);
  int len = 64;
  while (gethostname(buf, len) != 0)
  {
    len *= 2;
    buf = realloc(buf, len);
  }
  if (full == 0)
  {
    int i = 0;
    while (buf[i] && buf[i] != '.')
      ++i;
    buf[i] = '\0';
  }
  len = strlen(buf);
  memcpy(ret + *pos, buf, len);
  *pos += len;
  free(buf);
}

static void extended_date(char *str, int *i, char *ret, int *pos)
{
  char *tmp = malloc(sizeof (char) * 64);
  int j = 0;
  while (str[*i + 1 + j])
  {
    tmp[j] = str[*i + 1 + j];
    ++j;
    if (tmp[j - 1] == '}')
    {
      tmp[j - 1] = '\0';
      break;
    }
  }
  *i += j;
  tmp[j] = '\0';
  char *buf = malloc(sizeof (char) * 1024);
  time_t cur_time;
  time(&cur_time);
  struct tm *tm = localtime(&cur_time);
  int len = strftime(buf, 1024, tmp, tm);
  memcpy(ret + *pos, buf + 1, len - 1);
  *pos += len - 1;
  free(tmp);
  free(buf);
}

static void get_login(char *ret, int *pos)
{
  char *buf = getlogin();
  int len = strlen(buf);
  memcpy(ret + *pos, buf, len);
  *pos += len;
}

static void get_cwd(char *ret, int *pos)
{
  char *buf = getcwd(NULL, 0);
  int len = strlen(buf);
  memcpy(ret + *pos, buf, len);
  *pos += len;
  free(buf);
}

static void get_home(char *ret, int *pos)
{
  char *buf = getenv("HOME");
  int len = strlen(buf);
  memcpy(ret + *pos, buf, len);
  *pos += len;
}

static void get_uid(char *ret, int *pos)
{
  uid_t uid = getuid();
  if (uid == 0)
    ret[*pos] = '#';
  else
    ret[*pos] = '$';
  (*pos)++;
}

static void calculate_octal(char *s, int *i, char *ret, int *pos)
{
  if ((s[*i + 1] >= '0' && s[*i + 1] <= '7')
      && (s[*i + 2] >= '0' && s[*i + 2] <= '7'))
  {
    ret[*pos] = 64 * (s[*i] - '0') + 8 * (s[*i + 1] - '0') + s[*i + 2] - '0';
    (*pos)++;
    *i += 2;
  }
  else
    add_two_characters(ret, '\\', s[*i], pos);
}

static void second_check(char *str, int *i, char *ret, int *pos)
{
  char next = str[*i];
  if (next == 'W')
    get_home(ret, pos);
  else if (next == '$')
    get_uid(ret, pos);
  else if (next == 's')
  {
    add_two_characters(ret, '4', '2', pos);
    add_two_characters(ret, 's', 'h', pos);
  }
  else if (next >= '0' && next <= '7')
    calculate_octal(str, i, ret, pos);
  else if (next == '\\')
    add_character(ret, '\\', pos);
  else if (next != '[' && next != ']')
    add_two_characters(ret, '\\', next, pos);
}

void first_check(char *str, int *i, char *ret, int *pos)
{
  char next = str[*i];
  if (next == 'a')
    add_character(ret, 7, pos);  // BEL
  else if (next == 'd')
    get_date(ret, pos);
  else if (next == 'D' && str[*i + 1] == '{')
    extended_date(str, i, ret, pos);
  else if (next == 'e')
    add_character(ret, 27, pos);  // ESCAPE CHARACTER
  else if (next == 'h')
    get_hostname(0, ret, pos);
  else if (next == 'H')
    get_hostname(1, ret, pos);
  else if (next == 'n')
    add_character(ret, 10, pos);  // NEWLINE
  else if (next == 'r')
    add_character(ret, 13, pos);  // CARRIAGE RETURN
  else if (next == 'u')
    get_login(ret, pos);
  else if (next == 'w')
    get_cwd(ret, pos);
  else
    second_check(str, i, ret, pos);
}
