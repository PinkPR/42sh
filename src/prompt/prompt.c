#include "prompt.h"

void add_character(char *ret, char c, int *pos)
{
  ret[*pos] = c;
  (*pos)++;
}

void add_two_characters(char *ret, char c, char c2, int *pos)
{
  ret[*pos] = c;
  ret[*pos + 1] = c2;
  (*pos) += 2;
}

static char *finish_expansion(char *str, int pos)
{
  if (str[0] == '"' && str[pos - 1] == '"')
  {
    for (int i = 0; i < pos; ++i)
      str[i] = str[i + 1];
    pos -= 2;
  }
  else if (str[0] == '\'' && str[pos - 1] == '\'')
  {
    for (int i = 0; i < pos; ++i)
      str[i] = str[i + 1];
    pos -= 2;
  }
  str[pos] = '\0';
  str = realloc(str, sizeof (char) * (pos + 1));
  return str;
}

static char *expand_prompt(char *str)
{
  char *ret = malloc(sizeof (char) * 4096);
  int pos = 0;
  for (int i = 0; str[i]; ++i)
  {
    if (str[i] == '\\')
    {
      if (str[i + 1])
      {
        ++i;
        first_check(str, &i, ret, &pos);
      }
      else
        add_character(ret, str[i], &pos);
    }
    else
      add_character(ret, str[i], &pos);
  }
  return finish_expansion(ret, pos);
}

char *expanded_PS1_prompt(void)
{
  char *p = get_var("PS1");
  if (!p)
  {
    st_add_var(strdup("PS1"), strdup("42sh$ "));
    p = get_var("PS1");
  }
  p = expand_prompt(p);
  return p;
}

char *expanded_PS2_prompt(void)
{
  char *p = get_var("PS2");
  if (!p)
  {
    st_add_var(strdup("PS2"), strdup("> "));
    p = get_var("PS2");
  }
  p = expand_prompt(p);
  return p;
}
