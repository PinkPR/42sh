#include "my_getopt.h"
#include "str_tools.h"
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"

static void usage(void)
{
  fprintf(stderr, "usage: 42sh [options] [file]\n");
  fprintf(stderr, "  -c <command> :\t  Read command from <command>\n");
  fprintf(stderr, "  [+-]O :\t\t  Set/Unset shopt\n");
  fprintf(stderr, "  --norc :\t\t  Disable ressource reader\n");
  fprintf(stderr, "  --ast-printer :\t  Activate AST printer\n");
  fprintf(stderr, "  --version :\t\t  Display version number\n");
  exit(1);
}

void shopt_set(s_arg_l *l, int argc, char **argv, int *i)
{
  if (!l)
  {
    fprintf(stderr, "42sh: %s: invalid shell option name\n", argv[*i] + 3);
    usage();
  }
  if (str_cmp(l->arg->name, argv[*i] + 3, ' '))
  {
    l->arg->set = (argv[*i][0] == '-') ? 1 : 0;
    return;
  }
  shopt_set(l->next, argc, argv, i);
}

void shopt_init(void)
{
  g_main.shopts = init_opt("|ast_print |dotglob |expand_aliases |extglob "
                  "|nocaseglob |nullglob |sourcepath |xpg_echo");
}

void shopt_print(void)
{
  s_arg_l *tmp = g_main.shopts;
  while (tmp)
  {
    printf("%-15s %s\n", tmp->arg->name, tmp->arg->set ? "on" : "off");
    tmp = tmp->next;
  }
}

void shopt_shell_print(void)
{
  s_arg_l *tmp = g_main.shopts;
  while (tmp)
  {
    printf("shopt -%s %s\n", tmp->arg->set ? "s" : "u", tmp->arg->name);
    tmp = tmp->next;
  }
}

int shopt_print_shopt(char *shopt)
{
  s_arg_l *tmp = g_main.shopts;
  while (tmp)
  {
    if (strcmp(shopt, tmp->arg->name) == 0)
    {
      printf("%-15s %s\n", tmp->arg->name, tmp->arg->set ? "on" : "off");
      return !tmp->arg->set;
    }
    tmp = tmp->next;
  }
  return 1;
}
