#include "my_getopt.h"
#include "str_tools.h"
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"

static void usage(void)
{
  fprintf(stderr, "usage: 42sh [options] [file]\n");
  fprintf(stderr, "  -c <command> :\t  Read command from <command>\n");
  fprintf(stderr, "  [+-]O :\t\t  Set/Unset shopt\n");
  fprintf(stderr, "  --norc :\t\t  Disable ressource reader\n");
  fprintf(stderr, "  --ast-print :\t\t  Activate AST printer\n");
  fprintf(stderr, "  --version :\t\t  Display version number\n");
  exit(1);
}

static s_arg *arg_init(char *name, int has_val)
{
  s_arg *tmp = malloc(sizeof (s_arg));
  tmp->sht_name[0] = '\0';
  tmp->sht_name[1] = '\0';
  tmp->name = NULL;
  if (name[0] != '|')
    tmp->sht_name[0] = name++[0];
  if (name[0] == '|')
    tmp->name = str_cpy(++name, NULL, ' ');
  tmp->val = NULL;
  tmp->set = 0;
  tmp->has_val = has_val;
  return tmp;
}

static s_arg_l *arg_add(s_arg_l *l, char *name, int has_val)
{
  s_arg_l *tmp = malloc(sizeof (s_arg_l));
  tmp->arg = arg_init(name, has_val);
  tmp->next = l;
  return tmp;
}

static void arg_del(s_arg *arg)
{
  if (!arg)
    return;
  if (arg->name)
    free(arg->name);
  if (arg->val)
    free(arg->val);
  free(arg);
}

void arg_l_del(s_arg_l *arg_l)
{
  if (!arg_l)
    return;
  if (arg_l->next)
    arg_l_del(arg_l->next);
  if (arg_l->arg)
    arg_del(arg_l->arg);
  free(arg_l);
}

s_arg_l *init_opt(char *opt)
{
  s_arg_l *tmp = NULL;
  if (!opt)
    return NULL;
  while (*opt != '\0')
  {
    if (*opt != ' ')
    {
      if (*opt == '*')
        tmp = arg_add(tmp, ++opt, 1);
      else
        tmp = arg_add(tmp, opt, 0);
      while (*opt != ' ' && *opt != '\0')
        opt++;
    }
    if (*opt != '\0')
      opt++;
  }
  return tmp;
}

static void arg_set(s_arg_l *l, int argc, char **argv, int *i)
{
  int opt_size = (*(argv[*i] + 1) == '-') ? 2 : 1;
  if (!l)
  {
    fprintf(stderr, "42sh: %s: invalid option\n", argv[*i]);
    usage();
  }
  if (str_cmp(l->arg->name, argv[*i] + opt_size, ' ')
      || str_cmp(l->arg->sht_name, argv[*i] + opt_size, ' '))
  {
    l->arg->set = 1;
    if (l->arg->has_val)
    {
      if (*i < argc - 1)
        l->arg->val = str_cpy(argv[++(*i)], NULL, '\0');
      else
      {
        fprintf(stderr, "42sh: %s: option waits an argument\n", argv[*i]);
        usage();
      }
    }
    return;
  }
  arg_set(l->next, argc, argv, i);
}


s_arg_l *arg_parse(s_arg_l *l, int argc, char **argv)
{
  int i = 1;
  while (i < argc && (argv[i][0] == '-' || argv[i][0] == '+'))
  {
    if (argv[i][1] == 'O')
    {
      if (i + 1 == argc)
      {
        if (argv[i][0] == '-')
          shopt_print();
        else
          shopt_shell_print();
        exit(0);
      }
      else
      {
        shopt_set(g_main.shopts, argc, argv, &i);
        i++;
      }
    }
    else if (argv[i][0] == '-')
      arg_set(l, argc, argv, &i);
    i++;
  }
  return l;
}

int arg_is_set(s_arg_l *l, char *name)
{
  if (!l)
    return 0;
  if (l->arg)
    if ((l->arg->name && str_cmp(l->arg->name, name, ' '))
        || str_cmp(l->arg->sht_name, name, ' '))
      return l->arg->set;
  if (l->next)
    return arg_is_set(l->next, name);
  return 0;
}

char *arg_get(s_arg_l *l, char *name)
{
  if (!l)
    return 0;
  if (l->arg)
    if ((l->arg->name && str_cmp(l->arg->name, name, ' '))
        || str_cmp(l->arg->sht_name, name, ' '))
      return l->arg->val;
  if (l->next)
    return arg_get(l->next, name);
  return NULL;
}
