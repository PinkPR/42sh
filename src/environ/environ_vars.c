#include <string.h>
#include <stdio.h>
#include "environ.h"

extern s_envir *g_envir;

static char *get_key(char *str)
{
  char *ret = malloc(sizeof (char) * (strlen(str) + 1));
  int i = 0;
  while (str[i] != '=')
  {
    ret[i] = str[i];
    i++;
  }
  ret[i] = 0;
  return ret;
}

static char *get_value(char *str)
{
  char *ret = malloc(sizeof (char) * (strlen(str) + 1));
  int i = 0;
  while (str[i] != '=')
    i++;
  i++;
  int j = 0;
  while (str[i] && str[i + j])
  {
    ret[j] = str[i + j];
    j++;
  }
  ret[j] = 0;
  return ret;
}

char *get_var(char *key)
{
  s_list *tmp = g_envir->vars;
  while (tmp)
  {
    if (!strcmp(tmp->key, key))
      return tmp->value;
    tmp = tmp->next;
  }
  return NULL;
}

static int readonly(char *key)
{
  if (!strcmp(key, "UID"))
  {
    fprintf(stderr, "UID: readonly variable\n");
    return 1;
  }
  return 0;
}

static void update_var(s_list *var)
{
  var->next = g_envir->vars;
  g_envir->vars = var;
}

static s_list *new_var(char *key, char *value)
{
  s_list *var = malloc(sizeof (s_list));
  var->key = key;
  var->value = value;
  return var;
}

void st_add_var(char *key, char *value)
{
  if (value && value[0] == 0)
  {
    remove_var(key);
    free(key);
    return;
  }
  s_list *tmp = g_envir->vars;
  while (tmp)
  {
    if (!strcmp(tmp->key, key))
    {
      free(key);
      if (readonly(tmp->key))
        return;
      free(tmp->value);
      tmp->value = value;
      return;
    }
    tmp = tmp->next;
  }
  s_list *var = new_var(key, value);
  update_var(var);
  refresh_env(var->key);
}

void add_var(char *token_value)
{
  char *key = get_key(token_value);
  char *value = get_value(token_value);
  st_add_var(key, value);
}

void st_add_static_var(char *key, char *value)
{
  if (value && value[0] == 0)
  {
    remove_var(key);
    free(key);
    return;
  }
  s_list *tmp = g_envir->vars;
  while (tmp)
  {
    if (!strcmp(tmp->key, key))
    {
      free(tmp->value);
      tmp->value = value;
      free(key);
      return;
    }
    tmp = tmp->next;
  }
  s_list *var = malloc(sizeof (s_list));
  var->key = key;
  var->value = value;
  var->next = g_envir->vars;
  g_envir->vars = var;
  refresh_env(var->key);
}

void remove_var(char *key)
{
  s_list *tmp = g_envir->vars;
  s_list *parent = NULL;
  while (tmp)
  {
    if (!strcmp(tmp->key, key))
      break;
    parent = tmp;
    tmp = tmp->next;
  }
  if (tmp)
  {
    if (parent)
      parent->next = tmp->next;
    else
      g_envir->vars = tmp->next;
    free(tmp->key);
    free(tmp->value);
    free(tmp);
  }
}

