#include <string.h>
#include <stdio.h>
#include "environ.h"

extern s_envir *g_envir;

static int is_assign(char *str)
{
  int i = 0;
  while (str[i])
  {
    if (str[i] == '=')
      return 1;
    i++;
  }
  return 0;
}

static char *concat(char *str1, char *str2)
{
  str1 = realloc(str1, sizeof (char) * (strlen(str1) + strlen(str2) + 10));
  int i = strlen(str1);
  int j = 0;
  str1[i++] = '=';
  while (str2[j])
    str1[i++] = str2[j++];
  str1[i] = 0;
  return str1;
}

void add_envir_envs(char **environ)
{
  int i = 0;
  while (environ[i])
    add_env(strdup(environ[i++]));
}

int add_env(char *value)
{
  char *val = NULL;
  s_env *newenv = malloc(sizeof (s_env));
  if (!is_assign(value))
  {
    val = get_var(value);
    if (!val)
      val = strdup("");
    value = concat(value, val);
  }
  newenv->value = value;
  add_var(value);
  newenv->next = g_envir->envs;
  g_envir->envs = newenv;
  return putenv(value);
}

void refresh_env(char *key)
{
  char *value = NULL;
  s_env *tmp = g_envir->envs;
  while (tmp)
  {
    value = tmp->value;
    if (!strncmp(key, tmp->value, strlen(key)))
      break;
    tmp = tmp->next;
  }
  if (!tmp)
    return;
  char *val = NULL;
  val = get_var(key);
  if (!val)
    val = strdup("");
  value = concat(key, val);
  tmp->value = value;
  putenv(value);
}

void free_envs(void)
{
  s_env *tmp = g_envir->envs;
  while (tmp)
  {
    s_env *tmp2 = tmp->next;
    free(tmp->value);
    free(tmp);
    tmp = tmp2;
  }
  g_envir->envs = NULL;
}

int rm_env(char *value)
{
  s_env *tmp = g_envir->envs;
  s_env *parent = NULL;
  while (tmp)
  {
    if (!strncmp(tmp->value, value, strlen(value)))
      break;
    parent = tmp;
    tmp = tmp->next;
  }
  if (tmp)
  {
    if (parent)
      parent->next = tmp->next;
    else
      g_envir->envs = tmp->next;
    unsetenv(value);
    free(tmp->value);
    free(tmp);
    return 0;
  }
  return -1;
}
