#include <string.h>
#include <stdio.h>
#include "environ.h"
#include "exec.h"

extern s_envir *g_envir;

static char *get_key(char *str)
{
  char *ret = malloc(sizeof (char) * (strlen(str) + 1));
  int i = 0;
  while (str[i] != '=')
  {
    ret[i] = str[i];
    i++;
  }
  ret[i] = 0;
  return ret;
}

static char *get_value(char *str)
{
  char *ret = malloc(sizeof (char) * (strlen(str) + 1));
  int i = 0;
  while (str[i] != '=')
    i++;
  i++;
  int j = 0;
  while (str[i] && str[i + j])
  {
    ret[j] = str[i + j];
    j++;
  }
  ret[j] = 0;
  return ret;
}

char *get_alias(char *key)
{
  s_list *tmp = g_envir->aliases;
  while (tmp)
  {
    if (!strcmp(tmp->key, key))
      return tmp->value;
    tmp = tmp->next;
  }
  return NULL;
}

static s_list *new_alias(char *key, char *value, s_list *next)
{
  s_list *alias = malloc(sizeof (s_list));
  alias->key = key;
  alias->value = value;
  alias->next = next;
  return alias;
}

void st_add_alias(char *key, char *value)
{
  s_list *tmp = g_envir->aliases;
  s_list *pred = NULL;
  while (tmp)
  {
    if (strcmp(tmp->key, key) == 0)
    {
      free(tmp->value);
      tmp->value = value;
      free(key);
      return;
    }
    if (strcmp(tmp->key, key) > 0)
      break;
    pred = tmp;
    tmp = tmp->next;
  }
  if (pred)
    pred->next = new_alias(key, value, tmp);
  else
    g_envir->aliases = new_alias(key, value, tmp);
}

void add_alias(char *token_value)
{
  char *key = get_key(token_value);
  char *value = get_value(token_value);
  st_add_alias(key, value);
}

static int remove_all_aliases(void)
{
  s_list *tmp = g_envir->aliases;
  s_list *parent = NULL;
  while (tmp)
  {
    parent = tmp->next;
    free(tmp->key);
    free(tmp->value);
    free(tmp);
    tmp = parent;
  }
  g_envir->aliases = NULL;
  return 1;
}

int remove_alias(char *key)
{
  if (!key)
    return remove_all_aliases();
  s_list *tmp = g_envir->aliases;
  s_list *parent = NULL;
  while (tmp)
  {
    if (!strcmp(tmp->key, key))
      break;
    parent = tmp;
    tmp = tmp->next;
  }
  if (tmp)
  {
    if (parent)
      parent->next = tmp->next;
    else
      g_envir->aliases = tmp->next;
    free(tmp->key);
    free(tmp->value);
    free(tmp);
    return 1;
  }
  return 0;
}

int print_alias(char *key)
{
  if (key)
  {
    char *alias = get_alias(key);
    if (alias)
      fprintf(stdout, "%s='%s'\n", key, alias);
    else
    {
      fprintf(stderr, "alias: %s: not found\n", key);
      return 1;
    }
  }
  else
  {
    s_list *alias = g_envir->aliases;
    while (alias)
    {
      print_alias(alias->key);
      alias = alias->next;
    }
  }
  return 0;
}
