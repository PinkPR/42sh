#include <string.h>
#include <stdio.h>
#include "environ.h"
#include "exec.h"
#include "str_tools.h"
#include "unistd.h"

s_envir *g_envir;

static void add_envir_variables(char **argv)
{
  int i = 0;
  while (argv[i])
    add_var(argv[i++]);
}

void init_envir(char **argv)
{
  g_envir = malloc(sizeof (s_envir));
  g_envir->aliases = NULL;
  g_envir->funcs = NULL;
  g_envir->vars = NULL;
  g_envir->envs = NULL;
  add_envir_variables(argv);
  add_envir_envs(argv);
  st_add_var(strdup("$"), itoa(getpid()));
  st_add_static_var(strdup("UID"), itoa(getuid()));
  st_add_var(strdup("?"), itoa(0));
  st_add_var(strdup("#"), itoa(0));
  st_add_var(strdup("IFS"), strdup(" \t\n"));
}

static void free_vars(void)
{
  s_list *tmp = g_envir->vars;
  s_list *tmp2 = NULL;
  while (tmp)
  {
    free(tmp->key);
    free(tmp->value);
    tmp2 = tmp->next;
    free(tmp);
    tmp = tmp2;
  }
}

static void free_aliases(void)
{
  s_list *tmp = g_envir->aliases;
  s_list *tmp2 = NULL;
  while (tmp)
  {
    free(tmp->key);
    free(tmp->value);
    tmp2 = tmp->next;
    free(tmp);
    tmp = tmp2;
  }
}

static void free_funcs(void)
{
  s_func *tmp = g_envir->funcs;
  s_func *tmp2 = NULL;
  while (tmp)
  {
    free(tmp->name);
    free(tmp->ast);
    tmp2 = tmp->next;
    free(tmp);
    tmp = tmp2;
  }
}

void free_envir(void)
{
  free_aliases();
  free_vars();
  free_funcs();
  free_envs();
  free(g_envir);
  g_envir = NULL;
}

void add_func(char *name, struct ast *ast)
{
  s_func *func = malloc(sizeof (s_func));
  func->name = strdup(name);
  func->ast = ast;
  func->next = g_envir->funcs;
  g_envir->funcs = func;
}

int is_func(char *name)
{
  s_func *tmp = g_envir->funcs;
  while (tmp)
  {
    if (!strcmp(tmp->name, name))
      return 1;
    tmp = tmp->next;
  }
  return 0;
}

static char *get_all_args(int argc, char *argv[])
{
  int len = 1;
  for (int i = 1; i < argc; i++)
    len += strlen(argv[i]) + 1;
  char *ret = malloc(sizeof (char) * (len + 1));
  int j = 0;
  int k = 0;
  for (int i = 1; i < argc; i++)
  {
    j = 0;
    while (argv[i][j])
      ret[k++] = argv[i][j++];
    if (i < argc - 1)
      ret[k++] = ' ';
  }
  ret[k] = 0;
  return ret;
}

int exec_func(char *name, int argc, char *argv[])
{
  s_func *tmp = g_envir->funcs;
  while (tmp)
  {
    if (!strcmp(tmp->name, name))
      break;
    tmp = tmp->next;
  }
  if (tmp && tmp->ast)
  {
    st_add_var(strdup("@"), get_all_args(argc, argv));
    st_add_var(strdup("*"), get_all_args(argc, argv));
    st_add_var(strdup("#"), itoa(argc - 1));
    for (int i = 1; i < argc; i++)
      st_add_var(itoa(i), strdup(argv[i]));
    int ret = execute_shell_command(tmp->ast);
    for (int i = 1; i < argc; i++)
      remove_var(itoa(i));
    st_add_var(strdup("#"), itoa(0));
    return ret;
  }
  return -1;
}
