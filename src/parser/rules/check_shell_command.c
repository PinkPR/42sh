#include "check_rules.h"

static struct ast *rules_free(struct ast *ast)
{
  ast_free(ast);
  return NULL;
}

static struct ast *check_rules2(struct ast *ast)
{
  if (get_token() == CASE)
  {
    if (!add_check_ast(ast, check_rule_case()))
      return rules_free(ast);
    return ast;
  }
  else if (get_token() == IF)
  {
    if (!add_check_ast(ast, check_rule_if()))
      return rules_free(ast);
    return ast;
  }
  ast_free(ast);
  return NULL;
}

static struct ast *check_rules(struct ast *ast)
{
  if (get_token() == FOR)
  {
    if (!add_check_ast(ast, check_rule_for()))
      return rules_free(ast);
    return ast;
  }
  else if (get_token() == WHILE)
  {
    if (!add_check_ast(ast, check_rule_while()))
      return rules_free(ast);
    return ast;
  }
  else if (get_token() == UNTIL)
  {
    if (!add_check_ast(ast, check_rule_until()))
      return rules_free(ast);
    return ast;
  }
  return check_rules2(ast);
}

struct ast *check_shell_command(void)
{
  struct ast *ast = new_ast_rule(SHELL_COMMAND);
  if (eat_token(LEFT_BRACE, ast, SHELL_COMMAND))
  {
    if (add_check_ast(ast, check_compound_list()))
    {
      if (eat_token(RIGHT_BRACE, ast, SHELL_COMMAND))
        return ast;
      else
        return raise_error(RIGHT_BRACE, ast);
    }
  }
  else if (eat_token(LEFT_PAR, ast, SHELL_COMMAND))
  {
    if (add_check_ast(ast, check_compound_list()))
    {
      if (eat_token(RIGHT_PAR, ast, SHELL_COMMAND))
        return ast;
      else
        return raise_error(RIGHT_PAR, ast);
    }
  }
  return check_rules(ast);
}
