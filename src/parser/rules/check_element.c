#include "check_rules.h"

struct ast *check_element(void)
{
  struct ast *ast = new_ast_rule(ELEMENT);

  if (is_redirection())
  {
    add_check_ast(ast, check_redirection());
    return ast;
  }
  else if (eat_token(WORD, ast, ELEMENT))
    return ast;

  ast_free(ast);
  return NULL;
}
