#include "check_rules.h"

struct ast *check_case_item(void)
{
  struct ast *ast = new_ast_rule(CASE_ITEM);
  eat_token(LEFT_PAR, ast, CASE_ITEM);
  if (eat_token(WORD, ast, CASE_ITEM))
  {
    while (eat_token(PIPE, ast, CASE_ITEM))
    {
      if (!eat_token(WORD, ast, CASE_ITEM))
      {
        ast_free(ast);
        return NULL;
      }
    }
    if (eat_token(RIGHT_PAR, ast, CASE_ITEM))
    {
      while (eat_token(EOL, ast, CASE_ITEM))
        continue;

      add_check_ast(ast, check_compound_list());

      return ast;
    }
  }
  ast_free(ast);
  return NULL;
}
