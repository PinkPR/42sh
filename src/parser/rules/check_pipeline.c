#include "check_rules.h"

struct ast *check_pipeline(void)
{
  struct ast *ast = new_ast_rule(PIPELINE);
  eat_token(NOT, ast, PIPELINE);
  if (add_check_ast(ast, check_command()))
  {
    while (eat_token(PIPE, ast, PIPELINE))
    {
      while (eat_token(EOL, ast, PIPELINE))
        continue;
      if (!add_check_ast(ast, check_command()))
      {
        ast_free(ast);
        return NULL;
      }
    }
    return ast;
  }
  ast_free(ast);
  return NULL;
}
