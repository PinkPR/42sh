#include "check_rules.h"

struct ast *check_for2(struct ast *ast)
{
  if (eat_token(IN, ast, RULE_FOR))
  {
    while (eat_token(WORD, ast, RULE_FOR));

    if (!(eat_token(SEMI_COLON, ast, RULE_FOR) ||
          eat_token(EOL, ast, RULE_FOR)))
      return raise_error(SEMI_COLON, ast);

    while (eat_token(EOL, ast, RULE_FOR));

    return ast;
  }

  return raise_error(IN, ast);
}

struct ast *check_rule_for(void)
{
  struct ast *ast = new_ast_rule(RULE_FOR);

  if (!eat_token(FOR, ast, RULE_FOR))
    return raise_error(FOR, ast);

  if (!eat_token(WORD, ast, RULE_FOR))
    return raise_error(WORD, ast);

  while (eat_token(EOL, ast, RULE_FOR));

  if (get_token() == IN)
  {
    ast = check_for2(ast);
    if (!ast)
      return NULL;
  }


  if (add_check_ast(ast, check_do_group()))
    return ast;
  else
    return raise_error(DO, ast);
}
