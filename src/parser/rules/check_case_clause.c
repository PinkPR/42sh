#include "check_rules.h"

struct ast *check_case_clause(void)
{
  struct ast *ast = new_ast_rule(CASE_CLAUSE);
  if (add_check_ast(ast, check_case_item()))
  {
    while (eat_token(DOUBLE_SEMI_COLON, ast, CASE_CLAUSE))
    {
      while (eat_token(EOL, ast, CASE_CLAUSE))
        continue;

      if (!add_check_ast(ast, check_case_item()))
        break;
    }
    return ast;
  }
  ast_free(ast);
  return NULL;
}
