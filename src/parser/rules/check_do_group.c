#include "check_rules.h"

struct ast *check_do_group(void)
{
  struct ast *ast = new_ast_rule(DO_GROUP);

  if (!eat_token(DO, ast, DO_GROUP))
    return raise_error(DO, ast);

  if (!add_check_ast(ast, check_compound_list()))
    return raise_error(UNKNOWN, ast);

  if (!eat_token(DONE, ast, DO_GROUP))
    return raise_error(DONE, ast);

  return ast;
}
