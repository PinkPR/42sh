#include "check_rules.h"

struct ast *check_list(void)
{
  struct ast *ast = new_ast_rule(LIST);
  if (add_check_ast(ast, check_and_or()))
  {
    while (eat_token(SEMI_COLON, ast, LIST)
        || eat_token(SIMPLE_AND, ast, LIST))
    {
      if (!add_check_ast(ast, check_and_or()))
        break;
    }
    return ast;
  }
  ast_free(ast);
  return NULL;
}
