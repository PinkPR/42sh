#include "check_rules.h"

int is_funcdec(void)
{
  if (get_token() == FUNC_DEC)
    return 1;
  else if (get_token() == WORD && get_next_token() == LEFT_PAR)
    return 1;
  return 0;
}

struct ast *check_funcdec(void)
{
  struct ast *ast = new_ast_rule(FUNCDEC);
  eat_token(FUNC_DEC, ast, FUNCDEC);
  if (eat_token(WORD, ast, FUNCDEC))
  {
    if (eat_token(LEFT_PAR, ast, FUNCDEC)
        && eat_token(RIGHT_PAR, ast, FUNCDEC))
    {
      while (eat_token(EOL, ast, FUNCDEC))
        continue;
      if (add_check_ast(ast, check_shell_command()))
        return ast;
      else
        return raise_error(UNKNOWN, ast);
    }
  }
  ast_free(ast);
  return NULL;
}
