#include "check_rules.h"

struct ast *check_rule_if(void)
{
  struct ast *ast = new_ast_rule(RULE_IF);
  if (!eat_token(IF, ast, RULE_IF))
    return raise_error(IF, ast);

  if (!add_check_ast(ast, check_compound_list()))
    return raise_error(UNKNOWN, ast);

  if (!eat_token(THEN, ast, RULE_IF))
    return raise_error(THEN, ast);

  if (!add_check_ast(ast, check_compound_list()))
    return raise_error(UNKNOWN, ast);

  add_check_ast(ast, check_else_clause());

  if (!eat_token(FI, ast, RULE_IF))
    return raise_error(FI, ast);
  return ast;
}
