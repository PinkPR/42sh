#include <stdio.h>
#include "readline.h"
#include "check_rules.h"
#include "raise_error.h"
#include "tokenize.h"
#include "astprinter.h"
#include "main.h"

extern struct tokens g_toks;

static int my_strcmp(char *str1, char *str2)
{
  int i = 0;
  while (str1[i] == str2[i] && str1[i] && str2[i])
    ++i;
  if (str1[i] == str2[i])
    return 1;
  return 0;
}

void *raise_error(enum token expected, struct ast *ast)
{
  char *token2 = get_str_token(expected, "WORD");
  if (g_toks.token1->token_value[0] == '\0')
    g_main.PS2 = 1;
  else if (my_strcmp(g_toks.token1->token_value, "EOF"))
    g_main.PS2 = 1;
  else if (my_strcmp(g_toks.token1->token_value, "\n"))
     fprintf(stderr, "syntax error - unexpected token '%s', expected '%s'.\n",
            "newline", token2);
  else
  {
    if (expected != UNKNOWN)
      fprintf(stderr, "syntax error - unexpected token '%s', expected '%s'.\n",
            get_str_token(g_toks.token1->token, g_toks.token1->token_value),
            token2);
    else
      fprintf(stderr, "syntax error - unexpected token '%s'\n",
            get_str_token(g_toks.token1->token, g_toks.token1->token_value));
  }

  ast_free(ast);
  return NULL;
}
