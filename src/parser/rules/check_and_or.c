#include "check_rules.h"

struct ast *check_and_or(void)
{
  struct ast *ast = new_ast_rule(AND_OR);

  if (add_check_ast(ast, check_pipeline()))
  {
    while (eat_token(DOUBLE_AND, ast, AND_OR)
        || eat_token(DOUBLE_OR, ast, AND_OR))
    {
      while (eat_token(EOL, ast, AND_OR))
        continue;
      if (!add_check_ast(ast, check_pipeline()))
      {
        ast_free(ast);
        return NULL;
      }
    }
    return ast;
  }
  ast_free(ast);
  return NULL;
}
