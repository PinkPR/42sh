#include "check_rules.h"

struct ast *check_rule_until(void)
{
  struct ast *ast = new_ast_rule(RULE_UNTIL);

  if (!eat_token(UNTIL, ast, RULE_UNTIL))
    return raise_error(UNTIL, ast);

  if (add_check_ast(ast, check_compound_list()))
  {
    if (add_check_ast(ast, check_do_group()))
      return ast;
    else
      return raise_error(UNKNOWN, ast);
  }

  ast_free(ast);
  return NULL;
}
