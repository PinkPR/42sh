#include "check_rules.h"

struct ast *check_prefix(void)
{
  struct ast *ast = new_ast_rule(PREFIX);
  if (eat_token(ASSIGNMENT_WORD, ast, PREFIX))
    return ast;
  else if (is_redirection())
  {
    add_check_ast(ast, check_redirection());
    return ast;
  }
  ast_free(ast);
  return NULL;
}
