#include "check_rules.h"

struct ast *check_compound_list(void)
{
  struct ast *ast = new_ast_rule(COMPOUND_LIST);

  while (eat_token(EOL, ast, COMPOUND_LIST));

  if (!add_check_ast(ast, check_and_or()))
  {
    ast_free(ast);
    return NULL;
  }

  while (1)
  {
    if (!(eat_token(SEMI_COLON, ast, COMPOUND_LIST) ||
          eat_token(SIMPLE_AND, ast, COMPOUND_LIST) ||
          eat_token(EOL, ast, COMPOUND_LIST)))
      return raise_error(SEMI_COLON, ast);

    while (eat_token(EOL, ast, COMPOUND_LIST));

    if (!add_check_ast(ast, check_and_or()))
      break;
  }

  return ast;
}
