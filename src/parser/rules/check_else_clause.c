#include "check_rules.h"

static struct ast *check_else(struct ast *ast)
{
  if (!eat_token(ELSE, ast, ELSE_CLAUSE))
    return raise_error(ELSE, ast);

  if (add_check_ast(ast, check_compound_list()))
    return ast;
  else
  {
    ast_free(ast);
    return NULL;
  }
}

static struct ast *check_elif(struct ast *ast)
{
  if (!eat_token(ELIF, ast, ELSE_CLAUSE))
    return raise_error(ELIF, ast);

  if (!add_check_ast(ast, check_compound_list()))
  {
    ast_free(ast);
    return NULL;
  }

  if (!eat_token(THEN, ast, ELSE_CLAUSE))
    return raise_error(THEN, ast);

  if (!add_check_ast(ast, check_compound_list()))
  {
    ast_free(ast);
    return NULL;
  }

  add_check_ast(ast, check_else_clause());

  return ast;
}

struct ast *check_else_clause(void)
{
  struct ast *ast = new_ast_rule(ELSE_CLAUSE);

  if (get_token() == ELSE)
    return check_else(ast);
  else if (get_token() == ELIF)
    return check_elif(ast);
  else
  {
    ast_free(ast);
    return NULL;
  }
}
