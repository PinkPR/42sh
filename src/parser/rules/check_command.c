#include "check_rules.h"

struct ast *check_command(void)
{
  struct ast *ast = new_ast_rule(COMMAND);
  if (is_funcdec())
  {
    add_check_ast(ast, check_funcdec());
    while (add_check_ast(ast, check_redirection()))
      continue;
    return ast;
  }
  else if (add_check_ast(ast, check_shell_command()))
  {
    while (add_check_ast(ast, check_redirection()))
      continue;
    return ast;
  }
  else if (add_check_ast(ast, check_simple_command())
           && get_rule() != SHELL_COMMAND)
    return ast;
  ast_free(ast);
  return NULL;
}
