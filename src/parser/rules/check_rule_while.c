#include "check_rules.h"

struct ast *check_rule_while(void)
{
  struct ast *ast = new_ast_rule(RULE_WHILE);

  if (!eat_token(WHILE, ast, RULE_WHILE))
    return raise_error(WHILE, ast);

  if (add_check_ast(ast, check_compound_list()))
  {
    if (add_check_ast(ast, check_do_group()))
      return ast;
    else
      return raise_error(DO, ast);
  }
  else
    return raise_error(UNKNOWN, ast);

  ast_free(ast);
  return NULL;
}
