#include "check_rules.h"

struct ast *check_simple_command(void)
{
  int count = 0;
  struct ast *ast = new_ast_rule(SIMPLE_COMMAND);
  while (add_check_ast(ast, check_prefix()))
    count++;
  while (add_check_ast(ast, check_element()))
    count++;
  if (count > 0)
    return ast;
  ast_free(ast);
  return NULL;
}
