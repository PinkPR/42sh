#include "check_rules.h"

static int is_chev(enum token token)
{
  if (token == SIMPLE_R_CHEV || token == DOUBLE_R_CHEV
        || token == SIMPLE_L_CHEV || token == DOUBLE_L_CHEV
        || token == HEREDOC_CHE || token == R_CHEV_AND
        || token == L_CHEV_AND || token == R_CHEV_PIPE
        || token == INVERT_CHEV)
    return 1;
  return 0;
}

int is_redirection(void)
{
  if (get_token() == IONUMBER && is_chev(get_next_token()))
    return 1;
  else if (is_chev(get_token()))
    return 1;
  return 0;
}

struct ast *check_redirection(void)
{
  struct ast *ast = new_ast_rule(REDIRECTION);
  eat_token(IONUMBER, ast, REDIRECTION);
  if (eat_token(DOUBLE_L_CHEV, ast, REDIRECTION)
      || eat_token(HEREDOC_CHE, ast, REDIRECTION))
  {
    if (eat_token(get_token(), ast, REDIRECTION))
      return ast;
  }
  else if (is_chev(get_token()))
  {
    eat_token(get_token(), ast, REDIRECTION);
    if (eat_token(WORD, ast, REDIRECTION))
      return ast;
    else
      return raise_error(WORD, ast);
  }
  ast_free(ast);
  return NULL;
}
