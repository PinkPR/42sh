#include "check_rules.h"
#include "astprinter.h"
#include "main.h"

extern struct tokens g_toks;

struct ast *check_all(void)
{
  struct ast *ast = new_ast_rule(ORIGIN);
  struct ast *ast2 = ast;
  g_main.ast = ast;

  while (ast2)
  {
    ast2 = check_input();

    if (ast2)
      add_check_ast(ast, ast2);

    if (ast2 && ast2->sons[0]->node->token == ENDOF)
      break;
  }
  return ast;
}

struct ast *check_input(void)
{
  struct ast *ast = new_ast_rule(INPUT);
  if (add_check_ast(ast, check_list()))
  {
    if (eat_token(EOL, ast, INPUT))
      return ast;
    else if (eat_token(ENDOF, ast, INPUT))
      return ast;
  }
  else
  {
    if (eat_token(EOL, ast, INPUT))
      return ast;
    else if (eat_token(ENDOF, ast, INPUT))
      return ast;
  }
  ast_free(ast);
  return NULL;
}
