#include "check_rules.h"

struct ast *check_rule_case(void)
{
  struct ast *ast = new_ast_rule(RULE_CASE);

  if (!eat_token(CASE, ast, RULE_CASE))
    return raise_error(CASE, ast);

  if (!eat_token(WORD, ast, RULE_CASE))
    return raise_error(WORD, ast);

  while (eat_token(EOL, ast, RULE_CASE));

  if (!eat_token(IN, ast, RULE_CASE))
    return raise_error(IN, ast);

  while (eat_token(EOL, ast, RULE_CASE));

  add_check_ast(ast, check_case_clause());

  if (!eat_token(ESAC, ast, RULE_CASE))
    return raise_error(ESAC, ast);

  return ast;
}
