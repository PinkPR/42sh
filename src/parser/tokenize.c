#include "tokenize.h"

struct tokens g_toks;

static struct node *tokenize_copy(char **str, enum rule rule)
{
  char *tmp = strdup(*str);
  char *tmp2 = tmp;
  char **str2 = &tmp;
  struct node *token = tokenize(str2, rule);
  free(tmp2);
  return token;
}

void retokenize(enum rule rule)
{
  free(g_toks.token1);
  free_node(g_toks.token2);
  g_toks.token1 = tokenize(g_toks.str, rule);
  g_toks.token2 = tokenize_copy(g_toks.str, rule);
  if (g_toks.token1->rule > 0)
    g_toks.current_rule = g_toks.token1->rule;
}

void init_tokens(char **str, enum rule rule)
{
  g_toks.str = str;
  g_toks.expected_token = 0;
  g_toks.rule = 0;
  g_toks.current_rule = rule;
  g_toks.token1 = tokenize(str, rule);
  g_toks.token2 = tokenize_copy(g_toks.str, rule);
  if (g_toks.token1->rule > 0)
    g_toks.current_rule = g_toks.token1->rule;
}

void free_toks(void)
{
  free_node(g_toks.token1);
  free_node(g_toks.token2);
}

struct ast *add_token_ast(struct ast *ast, enum rule rule)
{
  ast_add_son(ast, rule, g_toks.token1->token, g_toks.token1->token_value);

  return ast->sons[ast->sons_nb - 1];
}
