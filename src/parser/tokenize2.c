#include "tokenize.h"

enum token get_token(void)
{
  if (g_toks.token1)
    return g_toks.token1->token;
  else
    return 0;
}

enum token get_next_token(void)
{
  if (g_toks.token2)
    return g_toks.token2->token;
  else
    return 0;
}

enum rule get_rule(void)
{
  return g_toks.rule;
}

enum rule get_current_rule(void)
{
  return g_toks.current_rule;
}
