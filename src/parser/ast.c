#include "ast.h"
#include "tokenize.h"

int eat_token(enum token waited, struct ast *ast,
              enum rule rule)
{
  if (get_token() == waited)
  {
    add_token_ast(ast, 0);
    g_toks.rule = rule;
    retokenize(rule);
    return 1;
  }
  if (rule == g_toks.rule)
    g_toks.expected_token = waited;
  return 0;
}

void free_node(struct node *node)
{
  free(node->token_value);
  free(node);
}

void ast_free(struct ast *ast)
{
  if (ast)
  {
    free_node(ast->node);

    for (int i = 0; i < ast->sons_nb; i++)
      ast_free(ast->sons[i]);
    free(ast->sons);
    free(ast);
  }
}

static void shift_array(struct ast ***ast, int *sons_nb, int n)
{
  for (int i = n; i < *sons_nb; i++)
    (*ast)[n] = (*ast)[n + 1];

  *sons_nb -= 1;
}

void clean_ast(struct ast *ast)
{
  for (int i = 0; i < ast->sons_nb; i++)
    clean_ast(ast->sons[i]);

  for (int i = 0; i < ast->sons_nb; i++)
  {
    if (ast->sons[i]->sons_nb == 0 && ast->sons[i]->node->rule)
    {
      shift_array(&(ast->sons), &(ast->sons_nb), i);
      i--;
    }
  }
}
