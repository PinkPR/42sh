#include "ast.h"
#include "tokenize.h"

#include <unistd.h>
struct node *new_node(enum rule rule, enum token token,
                      char *token_value)
{
  struct node *node = malloc(sizeof (struct node));

  node->rule = rule;
  node->token = token;
  node->token_value = token_value;

  return node;
}

struct ast *ast_new(void)
{
  struct ast *ast = malloc(sizeof (struct ast));

  ast->node = new_node(-1, 0, NULL);
  ast->sons = malloc(sizeof (struct ast *) * 256);
  ast->sons_nb = 0;

  return ast;
}

void ast_add_son(struct ast *ast, enum rule rule, enum token token,
                 char *token_value)
{
  ast->sons_nb++;
  ast->sons[ast->sons_nb - 1] = ast_new();

  free_node(ast->sons[ast->sons_nb - 1]->node);
  ast->sons[ast->sons_nb - 1]->node = new_node(rule, token, token_value);
}

int add_check_ast(struct ast *ast, struct ast *son)
{
  if (son)
  {
    ast->sons_nb++;
    ast->sons[ast->sons_nb - 1] = son;
    return 1;
  }

  return 0;
}

struct ast *new_ast_rule(enum rule rule)
{
  struct ast *ast = ast_new();
  ast->node->rule = rule;

  return ast;
}
