#ifndef ENVIRON_H
# define ENVIRON_H

# include <unistd.h>
# include "ast.h"

extern char **environ;

/**
** @struct list
** @author Kevin WASER
** @brief Basic chained element list structure
** @param key Key of the element
** @param value Value of the element
** @param next Pointer to the next element
*/
typedef struct list
{
  char *key;
  char *value;
  struct list *next;
} s_list;

/**
** @struct func
** @author Kevin WASER
** @brief Chained function list structure
** @param name Name of the function
** @param ast Ast of the function
** @param next Pointer to the next function
*/
typedef struct func
{
  char *name;
  struct ast *ast;
  struct func *next;
} s_func;

/**
** @struct env
** @author Kevin WASER
** @brief Chained string structure
** @param value Value of the string
** @param next Pointer to the next function
*/
typedef struct env
{
  char *value;
  struct env *next;
} s_env;

/**
** @struct envir
** @author Kevin WASER
** @brief Structure of the local environnement
** @param aliases Chained element list of the aliases
** @param funcs Chained function list of the functions
** @param vars Chained element list of the variables
** @param envs Chained string list of the environnement variables
*/
typedef struct envir
{
  s_list *aliases;
  s_func *funcs;
  s_list *vars;
  s_env *envs;

} s_envir;

/**
** @fn void init_envir(char **argv);
** @author Kevin WASER
** @brief Initialize local environnement variable and add all argv to it.
** @param argv List of assigment variables `WORD=WORD'
** @return Set the global variable and returns nothing
*/
void init_envir(char **argv);

/**
** @fn void add_var(char *token_value)
** @author Kevin WASER
** @brief Add assigment to local environnement
** @param token_value Assigment variable `WORD=WORD'
** @return Set the global variable and returns nothing
*/
void add_var(char *token_value);

/**
** @fn void st_add_var(char *key, char *value)
** @author Kevin WASER
** @brief Add variable `key' with value `value' to local environnement
** @param key Key of the variable
** @param value Value of the variable
** @return Set the global variable and returns nothing
*/
void st_add_var(char *key, char *value);


/**
** @fn void st_add_static_var(char *key, char *value)
** @author Kevin WASER
** @brief Add variable `key' with value `value' to local environnement
**
** Doesn't check if the variable is readonly.
**
** @param key Key of the variable
** @param value Value of the variable
** @return Set the global variable and returns nothing
*/
void st_add_static_var(char *key, char *value);

/**
** @fn void remove_var(char *key)
** @author Kevin WASER
** @brief Remove the variable `key' of the local environnement
** @param key Key of the variable
** @return Set the global variable and returns nothing
*/
void remove_var(char *key);

/**
** @fn int is_func(char *name)
** @author Kevin WASER
** @brief Returns 1 if `name' is a function in the local environnement
** @param name Name of the function
** @return Return 1 if `name' is in the function list, else returns 0
*/
int is_func(char *name);

/**
** @fn int exec_func(char *name, int argc, char *argv[])
** @brief Execute the ast of the function `name' with associated parameters
**
** Add `argv' temporary in the local environnement and execute the function
** with added parameters.
**
** @author Kevin WASER
** @param name Name of the function
** @param argc Number of parameters
** @param argv List of parameters
** @return Returns the exit status of the executed function or -1 if the
** function couldn't be executed
*/
int exec_func(char *name, int argc, char *argv[]);

/**
** @fn char *get_var(char *key)
** @author Kevin WASER
** @brief Return the value of the variable `key'
** @param key Key of the variable
** @return Returns the value of the variable `key', or NULL if it doesn't
** exists
**
*/
char *get_var(char *key);

/**
** @fn void add_func(char *name, struct ast *ast)
** @author Kevin WASER
** @brief Add a function `name' with the associated ast to the local environm.
** @param name Name of the function
** @param ast AST structure associated with the function
** @return Set the local environnement and returns nothing
*/
void add_func(char *name, struct ast *ast);

/**
** @fn void free_envir(void)
** @author Kevin WASER
** @brief Free the local environnement and set it to NULL
** @return Set the local environnement to NULL and returns nothing
*/
void free_envir(void);

/**
** @fn char *get_alias(char *key);
** @brief Returns the value that match the key
** @author Kevin WASER
** @param key Key of the alias
** @return Returns the value of the alias
*/
char *get_alias(char *key);

/**
** @fn void st_add_alias(char *key, char *value);
** @brief Add the alias 'key=value' to the local environnement
** @author Kevin WASER
** @param key Key of the alias
** @param value Value of the alias
** @return Returns nothing
*/
void st_add_alias(char *key, char *value);

/**
** @fn void add_alias(char *token_value);
** @brief Add the alias 'token_value' to the local environnement
** @author Kevin WASER
** @param token_value String that contains key=value of the alias
** @return Returns nothing
*/
void add_alias(char *token_value);

/**
** @fn int remove_alias(char *key);
** @brief Remove the alias 'key' from the local environnement
** @author Kevin WASER
** @param key Key of the alias
** @return Returns 1 if the alias has been removed else 0
*/
int remove_alias(char *key);

/**
** @fn int print_alias(char *key);
** @brief Print the alias 'key' or an error if not found.
** @author Kevin WASER
** @param key Key of the alias. Print all aliases if key = NULL
** @return Returns 1 if the alias is not found, 0 else
*/
int print_alias(char *key);

/**
** @fn int add_env(char *value);
** @brief Add the exported variable to the environnement and putenv it
** @author Kevin WASER
** @param value Value of the exported variable
** @return Returns the value of putenv(value)
*/
int add_env(char *value);

/**
** @fn int rm_env(char *value);
** @brief Remove the exported variable from the environnement and unsetenv it
** @author Kevin WASER
** @param value Name of the exportable variable to remove
** @return Returns 0 if the variable has been removed, -1 else
*/
int rm_env(char *value);

/**
** @fn void refresh_env(char *value);
** @brief Refresh the exported variable in the environnement
** @author Kevin WASER
** @param value Value of the exportable variable
** @return Returns nothing
*/
void refresh_env(char *value);

/**
** @fn void frees_envs(void);
** @brief Free all the exported variables
** @author Kevin WASER
** @param void Takes nothing
** @return Returns nothing
*/
void free_envs(void);


void add_envir_envs(char **environ);

#endif /* !ENVIRON_H */
