#ifndef PATH_H
# define PATH_H

typedef struct path_l s_path_l;

struct path_l
{
  char *path;
  struct path_l *next;
};

extern s_path_l *g_patharray;

void autocomplete(char *str);

#endif /* !PATH_H */
