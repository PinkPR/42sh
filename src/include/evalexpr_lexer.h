#ifndef LEXER_H
# define LEXER_H

# include <stdlib.h>
# include "tokens.h"

/**
** \fn struct fifo *make_fifo(char *str)
** \author Pierre-Olivier DI GIUSEPPE
** \brief builds the fifo to evaluate arithmetic expression
** \param str string to be evaluated
** \return returns a fifo containing the input
*/
struct fifo *make_fifo(char *str);

#endif /* !LEXER_H */
