#ifndef ASTPRINTER_H
# define ASTPRINTER_H

# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <string.h>

# include "rules.h"
# include "ast.h"

/**
** \fn void ast_print(struct ast *ast)
** \author Pierre-Olivier DI GIUSEPPE
** \brief makes a .dot file from the given ast
** \param ast pointer on the ast to be printed
*/
void ast_print(struct ast *ast);

/**
** \fn char *get_str_token(enum token token, char *token_value);
** \author Pierre-Olivier DI GIUSEPPE
** \brief returns a string according to a token and its token value
** \param token token to be used
** \param token_value string to be used as the token_value
** \return returns a string according to the token and its token_value
*/
char *get_str_token(enum token token, char *token_value);

/**
** \fn char *get_str(struct node *node)
** \author Pierre-Olivier DI GIUSEPPE
** \brief returns a string according to the token, the rule and the token val
**  of the node given
** \param node pointer on the node to be used to return the string
** \return returns the string described in the brief
*/
char *get_str(struct node *node);

#endif /* !ASTPRINTER_H */
