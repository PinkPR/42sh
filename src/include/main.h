#ifndef MAIN_H
# define MAIN_H

# include "ast.h"
# include "my_getopt.h"

struct main
{
  struct ast *ast;
  s_arg_l *opts;
  s_arg_l *shopts;
  int PS2;
  int pos;
  int max_len;
  char *command;
  int cur_job_pid;
};

extern struct main g_main;

/**
** \fn free_g_main(void)
** \author Pierre-Olivier DI GIUSEPPE
** \brief frees g_main global variable
*/

int build_ast_and_run(char *cmd, int is_c);
void free_g_main(void);
#endif /* !MAIN_H */
