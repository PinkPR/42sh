#ifndef JOBS_H
# define JOBS_H

# include "readline.h"

/**
** @struct job;
** @brief Structure used to store information concerning a job.
** @param next The next job in the job chain.
** @param command The command being executed by the job.
** @param pgid The process group id of the job.
** @param pid The process id of the job.
** @param status Used to save the exit status of the job.
** @param completed True if the job has completed.
** @param stopped True if the job is stopped.
** @param notified True if the job has notified the user of any change.
** @param tmodes Information concerning the terminal used to set attributes.
** @param in The file descriptor used for input.
** @param out The file descriptor used for standard output.
** @param err The file descriptor used for error output.
*/
typedef struct job
{
  struct job *next;
  char *command;
  pid_t pgid;
  pid_t pid;
  int status;
  int completed;
  int stopped;
  int notified;
  struct termios tmodes;
  int in;
  int out;
  int err;
} s_job;

/**
** @struct control;
** @brief Structure used to store information concerning jobs globally.
** @param shell_pgid The process group id of the shell.
** @param shell_tmodes The terminal attributes of the shell.
** @param is_tty True if the shell is interactive.
** @param jobs The first job in the chain of jobs.
** @param can_fork True if the shell can fork to execute other commands.
** @param cmds Commands to be executed as a list of strings.
*/
typedef struct control
{
  pid_t shell_pgid;
  struct termios shell_tmodes;
  int is_tty;
  s_job *jobs;
  int can_fork;
  char **cmds;
} s_control;

/**
** @struct fd;
** @brief Structure used to store basic file descriptors as a single unit.
** @param in The file descriptor used for input.
** @param out The file descriptor used for standard output.
** @param err The file descriptor used for error output.
*/
typedef struct fd
{
  int in;
  int out;
  int err;
} s_fd;

/**
** @var extern s_control *g_control;
** @brief A global variable of type s_control;
**
** Used to store job information globally;
*/
extern s_control *g_control;

/**
** \fn s_job *get_job(pid_t pgid)
** \author Tanguy DUPONT DE DINECHIN
** \brief Returns the job corresponding to the pgid;
** \param pgid The process group id corresponding to the job.
** \return Returns the job corresponding to the pgid.
*/
s_job *get_job(pid_t pgid);

/**
** \fn void init_jobs(void)
** \brief Sets up the global variable used to manage jobs.
*/
void init_jobs(void);

/**
** \fn void chop_and_launch(char *cmd)
** \brief Cuts the inputed string into seperate jobs and launches them.
** \param cmd The command to divide
*/
void chop_and_launch(char *cmd);

/**
** \fn void launch_job(s_job *job, int fg)
** \brief Launches the job in foreground or background.
** \param job The job to launch.
** \param fg True if the job is to be launched in foreground.
*/
void launch_job(s_job *job, int fg);

/**
** \fn void job_to_foreground(s_job *job, int cont)
** \brief Puts the job to the foreground.
** \param job The job to put to the foreground.
** \param cont True if the job should be continued.
*/
void job_to_foreground(s_job *job, int cont);

/**
** \fn void job_to_background(s_job *job, int cont)
** \brief Puts the job to the background.
** \param job The job to put to the background.
** \param cont True if the job should be continued.
*/
void job_to_background(s_job *job, int cont);

/**
** \fn void wait_job(s_job *job)
** \brief Waits until the job has stopped executing.
** \param job The job to wait.
*/
void wait_job(s_job *job);

/**
** \fn void format_job_info(s_job *job, const char *status)
** \brief Prints information based on job and status.
** \param job The job to print information on.
** \param status The status to print.
*/
void format_job_info(s_job *job, const char *status);

/**
** \fn void do_job_notification(void)
** \brief Updates the status of all jobs depending on recent events.
*/
void do_job_notification(void);

/**
** \fn void continue_job(s_job *job, int foreground)
** \brief Tells the job to continue execution.
** \param job The job to signal.
** \param foreground True if the job is to be put in foreground.
*/
void continue_job(s_job *job, int foreground);

/**
** \fn void free_job(s_job *job)
** \brief Frees all memory related to a job.
** \param job The job to free.
*/
void free_job(s_job *job);

/**
** \fn void add_new_job(int pid, char *cmd)
** \brief Used to add a new job when you press ctrl-z.
** \param pid The process id of the new job;
** \param cmd The name of the command being put in the background.
*/
void add_new_job(int pid, char *cmd);

/**
** \fn char **get_cmd(char *str)
** \brief Returns a list of strings corresponding to the list of jobs to do.
** \param str The initial string to divide into multiple jobs.
*/
char **get_cmd(char *str);

/**
** \fn int check_jobs(void)
** \brief Checks that there are no currently active or suspended jobs.
** \return 1 if there are still jobs, 0 if not.
*/
int check_jobs(void);

void free_cmds(void);

#endif /* !JOBS_H */
