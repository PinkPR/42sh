#ifndef HISTORY_H
# define HISTORY_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <fcntl.h>

/**
** @struct chain;
** @brief Structure used to store a double-linked list of strings.
** @param str The saved string.
** @param prev The previous element in the list.
** @param next The next element in the list.
*/
typedef struct chain s_chain;
struct chain
{
  char *str;
  s_chain *prev;
  s_chain *next;
};

/**
** @struct history;
** @brief Structure used to store information concerning the history.
** @param h_base The first element in the history list.
** @param h_cur The current element in the history list
** @param saved_command Saves the command currently being typed.
*/
typedef struct history
{
  s_chain *h_base;
  s_chain *h_cur;
  char saved_command[4096];
} s_history;

/**
** @var extern s_readl *g_history;
** @brief A global variable of type s_history*.
**
** Used to store metadata revolving around the history.
*/
extern s_history *g_history;

/**
** \fn void add_to_history(char *str)
** \author Tanguy DUPONT DE DINECHIN
** \brief Adds a string to the history list.
** \param str The string to add.
*/
void add_to_history(char *str);

/**
** \fn void init_history(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Fills the history list from ~/.42sh_history.
*/
void init_history(void);

/**
** \fn void close_history(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Writes the history to ~/.42sh_history and frees memory.
*/
void close_history(void);

/**
** \fn void move_history(int direction)
** \author Tanguy DUPONT DE DINECHIN
** \brief Updates history metadata.
** \param direction 1 to move up, -1 to move down.
*/
void move_history(int direction);

#endif /* !HISTORY_H */
