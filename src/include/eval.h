#ifndef EVAL_H
# define EVAL_H

/**
** \fn int evalexpr(char *str)
** \author Pierre-Olivier DI GIUSEPPE
** \brief evaluates an arithmetic expression
** \param str string to be evaluated
** \return returns the result of the calculation
*/
int evalexpr(char *str);

#endif /* !EVAL_H */
