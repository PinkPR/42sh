#ifndef FNMATCH_H
# define FNMATCH_H

int my_fnmatch(const char *pattern, const char *string);

struct string_l
{
  char *str;
  struct string_l *next;
};

extern struct string_l *g_cat_list;

void clear_cat(void);
char *custom_cat(char *s1, char *s2);

#endif /* !FNMATCH_H */
