#ifndef BUILTIN_H
# define BUILTIN_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define MNB_BREAK 12345678
# define MNB_CONTINUE 1234567

struct bltn_list
{
  char *bltn_name;
  int (*callback)(char **args);
  struct bltn_list *next;
};

struct bltn_list *g_bltn_list;

/**
** \fn struct bltn_list *bltn_add(struct bltn_list *list, char *name,
**                            int (*callback)(char **args))
** \author Pierre-Olivier DI GIUSEPPE
** \brief adds a builtin to a global list of builtins available
** \parram list builtin list to be updated
** \param name name of the builtin
** \param callback callback function pointing on the builtin function
** \return returns the list updated
*/
struct bltn_list *bltn_add(struct bltn_list *list, char *name,
                           int (*callback)(char **args));

/**
** \fn int exec_builtin(char *name, char **args, struct bltn_list *list)
** \author Pierre-Olivier DI GIUSEPPE
** \brief executes a builtin if available in the builtin list
** \param name name of the builtin
** \param args args to be used
** \param list list of availbale builtins
** \return returns the return of the builtin
*/
int exec_builtin(char *name, char **args, struct bltn_list *list);

/**
** \fn void bltn_list_free(struct bltn_list *list)
** \author Pierre-Olivier DI GIUSEPPE
** \brief frees the builtin list
** \param list builtin list to be free'd
*/
void bltn_list_free(struct bltn_list *list);

/**
** \fn void init_g_bltn_list(void)
** \author Pierre-Olivier DI GIUSEPPE
** \brief inits the builtin list
*/
void init_g_bltn_list(void);

/**
** \fn int rm_from_env(char **argv, int pos)
** \author Hatim-Pierre FAZILEABASSE
** \brief Removes every member of argv from the
** environement, starting at argv[pos]
** \param argv List of [name[=value]] to remove
** \param pos The starting point in the list argv
*/
int rm_from_env(char **argv, int pos);

/**
** \fn int builtin_cd(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's cd
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_cd(char **args);

/**
** \fn int builtin_break(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's break
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_break(char **args);

/**
** \fn int builtin_continue(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's continue
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_continue(char **args);

/**
** \fn int builtin_echo(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's echo
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_echo(char **args);

/**
** \fn int builtin_exit(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's exit
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_exit(char **args);

/**
** \fn int builtin_aslias(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's alias
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_alias(char **args);

/**
** \fn int builtin_unalias(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's unalias
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_unalias(char **args);

/**
** \fn int builtin_export(char **args)
** \author Hatim-Pierre FAZILEABASSE
** \brief Edits or prints the environement
** \param args Arguments given to the function
*/
int builtin_export(char **args);

/**
** \fn int builtin_history(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's history
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_history(char **args);

/**
** \fn int builtin_jobs(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's jobs
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_jobs(char **args);

/**
** \fn int builtin_wait(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's waita
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_wait(char **args);

/**
** \fn int builtin_fg(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's fg
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_fg(char **args);

/**
** \fn int builtin_bg(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's bg
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_bg(char **args);

int read_and_execute_from_fd(int fd);
int builtin_source(char **args);

/**
** \fn int builtin_shopt(char **args)
** \author everybody in the development team
** \brief follows the behaviour of Bash POSIX's shopt
** \param args args to be used
** \return follows the return values of Bash POSIX's
*/
int builtin_shopt(char **args);

#endif /* !BUILTIN_H */
