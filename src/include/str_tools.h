#ifndef STR_TOOLS_H
# define STR_TOOLS_H

/**
** @fn char *str_cpy(char *src, char *dst, char sep);
** @brief Copy str to dst up to the first sep char or '\0'
**
** @param *src Source string
** @param *dst Destination string. String is mallocated if NULL
** @param sep Separator character (in addition to '\0')
**
** @return Adress of the new string
*/
char *str_cpy(char *src, char *dst, char sep);

/**
** @fn int str_len(char *str, char sep);
** @brief Compute the length of str up to the fisrt sep char or '\0'
**
** @param *str Source string
** @param sep Separator character (in addition to '\0')
**
** @return Length of str up to sep or '\0'
*/
int str_len(char *str, char sep);

/**
** @fn int str_cmp(char *str1, char *str2, char sep)
** @brief Copy str to dst up to the first sep character of '\0'
**
** @param *str1 First string to match
** @param *str2 Second string to match
** @param sep Separator character (in addition to '\0')
**
** @return Return the adress of the new string
*/
int str_cmp(char *str1, char *str2, char sep);

/**
** @fn char *itoa(long long int i);
** @brief Write the integer into a string and returns it
** @author Kevin WASER
** @param i Integer to convert
** @return Returns the string with the number
*/
char *itoa(long long int i);

#endif /* !STR_TOOLS_H */
