#ifndef CHECK_RULES_H
# define CHECK_RULES_H

# include <stdio.h>

# include "ast.h"
# include "tokenize.h"
# include "raise_error.h"

/**
** \fn struct ast *check_all(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses all rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_all(void);

/**
** \fn struct ast *check_funcdec(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses function declaration rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_funcdec(void);

int is_funcdec(void);

/**
** \fn struct ast *check_shell_command(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses shell command rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_shell_command(void);

int is_shell_command(void);

/**
** \fn struct ast *check_command(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses command rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_command(void);

/**
** \fn struct ast *check_simple_command(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses simple command rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_simple_command(void);

/**
** \fn struct ast *check_rule_if(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses if rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_rule_if(void);

/**
** \fn struct ast *check_rule_while(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses while rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_rule_while(void);

/**
** \fn struct ast *check_rule_for(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses for rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_rule_for(void);

/**
** \fn struct ast *check_rule_until(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses until rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_rule_until(void);

/**
** \fn struct ast *check_rule_case(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses case rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_rule_case(void);

/**
** \fn struct ast *check_else_clause(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses else clause rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_else_clause(void);

/**
** \fn struct ast *check_do_group(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses do_group rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_do_group(void);

/**
** \fn struct ast *check_case_clause(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses case clause rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_case_clause(void);

/**
** \fn struct ast *check_input(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses input rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_input(void);

/**
** \fn struct ast *check_list(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses list rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_list(void);

/**
** \fn struct ast *check_and_or(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses and_or rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_and_or(void);

/**
** \fn struct ast *check_pipeline(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses pipeline rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_pipeline(void);

/**
** \fn struct ast *check_compound_list(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses compound_list rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_compound_list(void);

/**
** \fn struct ast *check_case_item(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses case item rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_case_item(void);

/**
** \fn struct ast *check_redirection(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses redirection rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_redirection(void);

int is_redirection(void);

/**
** \fn struct ast *check_prefix(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses prefix rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_prefix(void);

/**
** \fn struct ast *check_element(void)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief parses element rule
** \return returns the ast newly created if parsing was successful, NULL
**  otherwise.
*/
struct ast *check_element(void);

#endif /* !CHECK_RULES_H */
