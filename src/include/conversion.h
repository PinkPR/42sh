#ifndef CONVERSION_H
# define CONVERSION_H

# include "filo.h"

struct int_couple
{
  int nb1;
  int nb2;
};

extern struct fifo *g_output;
extern struct filo *g_stack;
extern struct fifo *g_input;

/**
** \fn void make_stack(char *str)
** \author Pierre-Olivier DI GIUSEPPE
** \brief make an output fifo from a string
** \param str string to be evaluated
*/
void make_stack(char *str);

/**
** \fn int launch_eval(struct fifo **tmp1, struct filo **tmp2,
**                     struct filo **tmp3, struct filo **stack2)
** \author Pierre-Olivier DI GIUSEPPE
** \brief launch the evaluation
** \return return 1 if there was an error and 0 otherwise
*/
int launch_eval(struct fifo **tmp1, struct filo **tmp2,
                struct filo **tmp3, struct filo **stack2);

void output_loop(struct fifo **tmp1, struct filo **tmp2,
                 struct filo **tmp3, struct filo **stack2);
#endif /* !CONVERSION_H */
