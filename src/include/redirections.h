#ifndef REDIRECTIONS_H
# define REDIRECTIONS_H

# include "ast.h"

enum redir
{
  NOREDIR,
  OUT_REDIR,        /*  >   */
  APPEND_REDIR,     /*  >>  */
  EXTEND_REDIR,     /*  >|  */
  DUPOUT_REDIR,     /*  >&  */

  IN_REDIR,         /*  <   */
  DUPIN_REDIR,      /*  <&  */
  HD_REDIR,         /*  <<  */
  HDNO_REDIR,       /*  <<- */
  DIAMOND_REDIR,    /*  <>  */
};

/**
** @struct outputs
** @brief Structure that contains every redirections
** @author Kevin WASER
** @param outputs int array that contains every redirections.
** Redirection i goes from index [2*i] to [2*i + 1]
** @param nb_outputs Number of element in the array
*/
typedef struct outputs
{
  int *outputs;
  int nb_outputs;
} s_outputs;

/**
** @fn s_outputs *get_redirection(struct ast *ast)
** @brief Returns the outputs structure that contains every redirections
** @author Kevin WASER
** @param ast AST structure of the REDIRECTION node
** @return Returns the outputs structure that contains every redirections
*/
s_outputs *get_redirection(struct ast *ast);

/**
** @fn void outputs(s_outputs *outputs)
** @brief Free and close all file descriptors in the outputs structure
** @author Kevin WASER
** @param outputs Outputs structure to free
** @return Returns nothing
*/
void free_outputs(s_outputs *outputs);

#endif /* !REDIRECTIONS_H */
