#ifndef TOKENS_H
# define TOKENS_H

enum token
{
  NUMBER    = 1,
  PLUS      = 2,
  MINUS     = 3,
  MUL       = 4,
  DIV       = 5,
  MOD       = 6,
  EXP       = 7,
  AND       = 8,
  OR        = 9,
  BXOR      = 10,
  BOR       = 11,
  BAND      = 12,
  NOT       = 13,
  BNOT      = 14,
  MINUSUN   = 15,
};

#endif /* !TOKENS_H */
