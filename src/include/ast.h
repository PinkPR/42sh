#ifndef AST_H
# define AST_H

# include <stdlib.h>

# include "rules.h"

struct node
{
  enum rule rule;
  enum token token;
  char *token_value;
};

struct ast
{
  struct node *node;
  struct ast **sons;
  int sons_nb;
};

extern struct tokens g_toks;

/**
** \fn void ast_add_son(struct ast *ast, enum rule rule, enum token token,
**                      char *token_value)
** \author Pierre-Olivier DI GIUSEPPE
** \brief Adds a node as a new son of an AST struct
** \param ast ast to use as root of the new son
** \param rule rule of the node
** \param token token of the node
** \param token_value token_value of the node
*/
void ast_add_son(struct ast *ast, enum rule rule, enum token token,
                 char *token_value);

/**
** \fn int add_check_ast(struct ast *ast, struct ast *son);
** \author Pierre-Olivier DI GIUSEPPE
** \brief Adds an ast as a new son of the given root ast
** \param ast root ast
** \param son sons ast
** \return returns 1 if son was correctly added, 0 otherwise.
*/
int add_check_ast(struct ast *ast, struct ast *son);

/**
** \fn int eat_token(enum token waited, struct ast *ast, enum rule rule)
** \author Pierre-Olivier DI GIUSEPPE
** \brief eats the current token if equal to the token waited and automatically
**  adds it as a leaf of the ast.
** \param waited token to compare with the current token
** \param ast ast to be considered as root ast of the son newly created
** \param rule rule to add to the node if needed(set rule at zero if not
**  needed)
** \return returns 1 if sons was correctly added, 0 if current and waited
**  tokens are different.
*/
int eat_token(enum token waited, struct ast *ast,
              enum rule rule);
/**
** \fn struct ast *new_ast_rule(enum rule rule)
** \author Pierre-Olivier DI GIUSEPPE
** \brief creates a new ast from a rule
** \param rule rule to be used to fill the rule field of the ast node
** \return returns a pointer on the ast newly created
*/
struct ast *new_ast_rule(enum rule rule);

/**
** \fn struct ast *ast_new(void)
** \author Pierre-Olivier DI GIUSEPPE
** \brief creates a new ast from the rule INPUT
** \return returns a pointer on the ast newly created
*/
struct ast *ast_new(void);

/**
** \fn void free_node(struct node *node)
** \author Pierre-Olivier DI GIUSEPPE
** \brief frees a node
** \param node node to be free'd
*/
void free_node(struct node *node);

/**
** \fn void ast_free(ast *ast)
** \author Pierre-Olivier DI GIUSEPPE
** \brief frees an ast recursively
** \param ast ast to be free'd
*/
void ast_free(struct ast *ast);

/**
** \fn strut node *new_node(enum rule rule, enum token token,
**  char *token_value)
** \author Pierre-Olivier DI GIUSEPPE
** \brief creates a new node from a rule, a token and a token value
** \param rule rule to be set for the node
** \param token token to be set for the node
** \param token_value string to be set as the token_value for the node
** \return returns a pointer on the node newly created
*/
struct node *new_node(enum rule rule, enum token token,
                      char *token_value);

#endif /* !AST_H */
