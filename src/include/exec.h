#ifndef EXEC_H
# define EXEC_H

# include <stdio.h>
# include "ast.h"
# include "rules.h"

extern int g_continue;

void sigint_cancel(int num);

/**
** \fn execute_ast(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes ast rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_ast(struct ast *ast);

/**
** \fn execute_input(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes input rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_input(struct ast *ast);

/**
** \fn execute_list(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes list rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_list(struct ast *ast);

/**
** \fn execute_and_or(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes and_or rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_and_or(struct ast *ast);

/**
** \fn execute_pipeline(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes pipeline rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_pipeline(struct ast *ast);

/**
** \fn execute_else_clause(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes else clause rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_else_clause(struct ast *ast);

/**
** \fn execute_simple_commands(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes simple commands rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_simple_commands(struct ast *ast);

/**
** \fn execute_piped_command(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes piped command rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_piped_command(struct ast *ast);

/**
** \fn execute_command(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes command rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_command(struct ast *ast);

/**
** \fn execute_piped_commands(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes piped commands rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_piped_commands(struct ast *ast);

/**
** \fn execute_shell_command(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes shell command rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_shell_command(struct ast *ast);

/**
** \fn execute_func_dec(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes function declaration rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_func_dec(struct ast *ast);

/**
** \fn execute_rule_for(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes for rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_rule_for(struct ast *ast);

/**
** \fn execute_rule_if(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes if rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_rule_if(struct ast *ast);

/**
** \fn execute_rule_while(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes while rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_rule_while(struct ast *ast);

/**
** \fn execute_rule_until(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes until rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_rule_until(struct ast *ast);

/**
** \fn execute_compound_list(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes compound list rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_compound_list(struct ast *ast);

/**
** \fn execute_do_group(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes do_group rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_do_group(struct ast *ast);

/**
** \fn execute_rule_case(struct ast *ast)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes case rule
** \param ast ast to be executed
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_rule_case(struct ast *ast);

/**
** \fn execute_case_item(struct ast *ast, ref)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes case_item rule
** \param ast ast to be executed
** \param ref reference string to be used for comparisons
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_case_item(struct ast *ast, char *ref);

/**
** \fn execute_case_clause(struct ast *ast, char *ref)
** \author digius_p, dupont_t, fazile_h, tsakir_t, waser_k
** \brief executes case_clause rule
** \param ast ast to be executed
** \param ref reference string to be used for comparisons
** \return returns 0 if everything was ok, 1 otherwise
*/
int execute_case_clause(struct ast *ast, char *ref);

#endif /* !EXEC_H */
