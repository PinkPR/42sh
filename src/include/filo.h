#ifndef FILO_H
# define FILO_H

# include <stdlib.h>
# include "tokens.h"

struct filo
{
  enum token token;
  int value;
  struct filo *next;
};

struct filo *filo_add(struct filo *filo, enum token token, int value);

struct filo *filo_pop(struct filo **filo);

#endif /* !FILO_H */
