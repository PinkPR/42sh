#ifndef LEXER_H
# define LEXER_H

# include <string.h>

# include "ast.h"
# include "rules.h"
# include "str_tools.h"

/**
** @fn struct node *tokenize(char **str, enum rule rule);
** @brief Return the next token in the string 'str', in the context 'rule'.
**
** Tokenize gets the next token in the string pointed by str and increase the
** pointer by the number of read characters. Rule will help the lexer to get
** the right token depending on the context.
**
** @author Kevin WASER
** @param str Pointer to the string that will be lexed
** @param rule Rule that will set the context
** @return Returns the node with the correct token, token_value, and rule.
*/
struct node *tokenize(char **str, enum rule rule);

/**
** @fn char *get_word(char *str, int *nb)
** @brief Returns the next word in the string
** @author Kevin WASER
** @param str String stream
** @param nb Pointer to the number of read characters
** @return Returns a pointer to the next word
*/
char *get_word(char *str, int *nb);

/**
** @fn char *get_assignement(char *str, int *nb)
** @brief Returns the next assignement word `WORD=WORD'
** @author Kevin WASER
** @param str String stream
** @param nb Pointer to the number of read characters
** @return Returns a pointer to the next assignement word
*/
char *get_assignement(char *str, int *nb);

/**
** @fn int is_assign(char *str)
** @brief Returns 1 if the next string is an assignement word
** @author Kevin WASER
** @param str String stream
** @return Returns 1 if the next string is an assignement word, else 0
*/
int is_assign(char *str);

/**
** @fn int is_ionumber(char *str, int *d)
** @brief Returns 1 if the next string is an IONUMBER
** @author Kevin WASER
** @param str String stream
** @param d Pointer to the number of read characters
** @return Returns 1 if the next string is an IONUMBER, else 0
*/
int is_ionumber(char *str, int *d);

#endif /* !LEXER_H */
