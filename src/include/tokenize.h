#ifndef TOKENIZE_H
# define TOKENIZE_H

# include <stdlib.h>

# include "ast.h"
# include "rules.h"
# include "lexer.h"

struct tokens
{
  char **str;
  enum token expected_token;
  enum rule rule;
  enum rule current_rule;
  struct node *token1;
  struct node *token2;
};

/**
** \fn enum token get_token(void)
** \brief returns the current token
** \return returns the current token
*/
enum token get_token(void);

/**
** \fn enum token get_next_token(void)
** \brief returns the next token
** \return returns the nexttoken
*/
enum token get_next_token(void);

/**
** \fn enum rule get_rule(void)
** \author Pierre-Olivier DI GIUSEPPE
** \brief gets the current rule
** \return returns an enum rule value according to the current rule
*/
enum rule get_rule(void);

/**
** \fn enum rule get_rule(void)
** \author Kevin WASER
** \brief gets the current rule
** \return returns an enum rule value according to the current rule
*/
enum rule get_current_rule(void);

/**
** \fn void retokenize(enum rule rule)
** \author Pierre-Olivier DI GIUSEPPE
** \brief consume the current token and switch automatically current and
**  next tokens to get new ones
** \param rule current rule, given for lexer stuff
*/
void retokenize(enum rule rule);

/**
** \fn void init_tokens(char **str, enum rule rule)
** \author Pierre-Olivier DI GIUSEPPE
** \brief inits the g_toks global var
** \param str pointer on the str to be parsed
** \param rule rule to be used to init the parser
*/
void init_tokens(char **str, enum rule rule);

/**
** \fn void free_toks(void)
** \author Pierre-Olivier DI GIUSEPPE
** \brief frees the g_toks globa var
*/
void free_toks(void);

/**
** \fn struct ast *add_token_ast(struct ast *ast, enum rule rule)
** \author Pierre-Olivier DI GIUSEPPE
** \brief adds the current token to the ast
** \param ast ast to be used as the root ast of the node newly created
** \param rule current rule, used by the lexer
** \return returns a pointer on the ast newly created
*/
struct ast *add_token_ast(struct ast *ast, enum rule rule);

#endif /* !TOKENIZE_H */
