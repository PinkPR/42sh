#ifndef FIFO_H
# define FIFO_H

# include <stdlib.h>
# include "tokens.h"

struct fifo
{
  enum token token;
  int value;
  struct fifo *next;
};

struct fifo *fifo_add(struct fifo *fifo, enum token token, int value);

struct fifo *fifo_pop(struct fifo **fifo);

#endif /* !FIFO_H */
