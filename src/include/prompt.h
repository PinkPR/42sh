#ifndef PROMPT_H
# define PROMPT_H

# include "environ.h"
# include <time.h>
# include <string.h>
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>

void first_check(char *str, int *i, char *ret, int *pos);
void add_character(char *ret, char c, int *pos);
void add_two_characters(char *ret, char c, char c2, int *pos);

/**
** \fn char *expanded_PS1_prompt(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Expands the basic prompt variable PS1.
** \return Returns the string resulting from the expansion of PS1.
*/
char *expanded_PS1_prompt(void);

/**
** \fn char *expanded_PS2_prompt(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Expands the basic prompt variable PS2.
** \return Returns the string resulting from the expansion of PS2.
*/
char *expanded_PS2_prompt(void);

#endif /* !PROMPT_H */
