#ifndef MINIGETOPT_H
# define MINIGETOPT_H

typedef struct
{
  char     sht_name[2];
  char    *name;
  char    *val;
  int      set;
  int      has_val;
} s_arg;

typedef struct arg_l s_arg_l;
struct arg_l
{
  s_arg   *arg;
  s_arg_l *next;
};

/**
** \fn s_arg_l *init_opt(char *opt)
** \author Thomas Tsakiris
** \brief Initialise list of valid options
** \param opt List of space separated options in this format : [*][a][|abcd]
*/
s_arg_l *init_opt(char *opt);

/**
** \fn void arg_l_del(s_arg_l *arg_l)
** \author Thomas Tsakiris
** \brief Recursively free an arg list
** \param l The list to free
*/
void arg_l_del(s_arg_l *arg_l);

/**
** \fn s_arg_l *arg_parse(s_arg_l *l, int argc, char **argv)
** \author Thomas Tsakiris
** \brief Parse argv to compute option state and arg
** \param l The list to write in
** \param argc arg count
** \param argv arg array
*/
s_arg_l *arg_parse(s_arg_l *l, int argc, char **argv);

/**
** \fn int arg_is_set(s_arg_l *l, char *name)
** \author Thomas Tsakiris
** \brief Return if an option was invoked
** \param l the list where options are stored
** \param name short or long name of the option to look at
*/
int arg_is_set(s_arg_l *l, char *name);

/**
** \fn char *arg_get(s_arg_l *l, char *name)
** \author Thomas Tsakiris
** \brief Return the argument givent to an option
** \param l the list where options are stored
** \param name short or long name of the option to look at
*/
char *arg_get(s_arg_l *l, char *name);

/**
** \fn void shopt_set(s_arg_l *l, int argc, char **argv, int *i)
** \author Thomas Tsakiris
** \brief Recursively free an arg list
** \param arg_l The list to update
** \param argc arg count
** \param argv arg array
** \param i the current pos in argv
*/
void shopt_set(s_arg_l *l, int argc, char **argv, int *i);

void shopt_init(void);

void shopt_print(void);

void shopt_shell_print(void);

int shopt_print_shopt(char *shopt);

#endif /* !MINIGETOPT_H */
