#ifndef EXPAND_H
# define EXPAND_H

# include <stdio.h>
# include "ast.h"
# include "rules.h"

/**
** @fn char *expand_variables(char *str);
** @brief Expand all variables and remove all unused quotes from string.
** @author Kevin WASER
** @param str String to expand
** @return Returns the new string malloc'd
*/
char *expand_variables(char *str);

/**
** @fn char *expand(char *str);
** @brief Execute all expands on String.
** @author Kevin WASER
** @param str String to expand
** @return Returns the new string malloc'd
*/
char *expand(char *str);

/**
** @fn char *expand_arith(char *str);
** @brief Expands all arithmetics patterns '$(( ))' and evaluates the calculus.
** @author Kevin WASER
** @param str String to expand
** @return Returns to new string malloc'd
*/
char *expand_arith(char *str);

/**
** @fn char *replace(char *str, char *replace, char *ins);
** @brief Replaces the pattern `replace' by the string `ins' in the string str.
** @author Kevin WASER
** @param str String to search in
** @param replace String to replace
** @param ins String to insert instead of replace string
** @return Returns the new string with replacements malloc'd
*/
char *replace(char *str, char *replace, char *ins);

/**
** @fn char *expand_tildes(char *str);
** @brief Expands '~' '~+' and '~-' in the string.
** @author Kevin WASER
** @param str String to expand
** @return Returns the new string malloc'd
*/
char *expand_tildes(char *str);


/**
** @fn void path_expand(char *path);
** @brief Allocate a list of expanded paths
** @author Thomas TSAKIRIS
** @param path String to expand
*/
void path_expand(char *path);

/**
** @fn void path_expand_r(char *dir, char *path);
** @brief Recursively scan path in dir and fill the path list when end is 1
** @author Thomas TSAKIRIS
** @param dir String representing the directory to scan
** @param path String to expand
*/
void path_expand_r(char *dir, char *path);

/**
** @fn void autocomplete(char *str);
** @brief Allocate a list of expanded paths
** @author Thomas TSAKIRIS
** @param str String to complete
*/
void autocomplete(char *str);

/**
** @fn char *expand_backslash(char *str);
** @author Kevin WASER
** @brief Convert all backslash to '' pattern to use litteral way of characters
** @param str String to expand
** @return Returns the new expanded string
*/
char *expand_backslash(char *str);

char *expand_subshell(char *str);
char *insert_str(char *str, int *ind, char *ins, int *size);
void clear_autocomp(void);

#endif /* !EXPAND_H */
