#ifndef RAISE_ERROR_H
# define RAISE_ERROR_H

/**
** \fn void *rase_error(enum token exepected, struct ast *ast)
** \author Pierre-Olivier DI GIUSEPPE
** \brief raise an error according to the current token and the token expected,
**  and frees the ast
** \param expected token expected
** \param ast ast to be free'd
** \return returns NULL
*/
void *raise_error(enum token expected, struct ast *ast);

#endif /* !RAISE_ERROR_H */
