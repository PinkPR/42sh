#ifndef READLINE_H
# define READLINE_H

# include <termios.h>
# include <termcap.h>
# include <term.h>
# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include <signal.h>
# include <stdio.h>
# include <sys/wait.h>
# include "path.h"

/**
** @struct readl;
** @brief Structure used to store metadata on the typed string.
** @param base_attr Stores the shell's base attributes to restore them later.
** @param command The string containing the command to be executed.
** @param pos The position of the cursor in the string.
** @param len The length of the string.
** @param max_len The maximum length that has been allocated for the string.
** @param cur_line The line on which the cursor is.
** @param max_line The height of the inputed text.
** @param prompt The string containing the prompt to display.
** @param prompt_len The length of the prompt.
** @param prompt_height The height of the prompt.
** @param col The amount of columns per line in the terminal.
** @param notified True if the user has been told some jobs still exist.
** @param displayed True if autocomplete choices have been displayed.
** @param current_tab The current word autocompleted.
*/

typedef struct readl
{
  struct termios base_attr;
  char *command;
  int pos;
  int len;
  int max_len;
  int cur_line;
  int max_line;
  char *prompt;
  int prompt_len;
  int prompt_height;
  int col;
  int notified;
  int displayed;
  s_path_l *current_tab;
} s_readl;

/**
** @var extern s_readl *g_term;
** @brief A global variable of type s_real*.
**
** Used to store metadata concerning the typed string.
*/

extern s_readl *g_term;

/**
** \fn s_readl *readline(int incomplete)
** \author Tanguy DUPONT DE DINECHIN
** \brief Reads input, treats special keys, displays the result after each key.
** \param incomplete Whether or not we are working with the PS2 prompt.
** \return Returns the structure containing the typed string.
*/
s_readl *readline(int incomplete);

/**
** \fn int check_key(char c, int col_change)
** \author Tanguy DUPONT DE DINECHIN
** \brief Redirects to the appropriate function based on input.
** \param c The inputed character that will be checked.
** \param col_change 1 if the width of the terminal has been changed, else 0.
** \return Returns 1 if the input was a newline, 0 if not.
*/
int check_key(char c, int col_change);

/**
** \fn int ctrl_modifier(char c)
** \author Tanguy DUPONT DE DINECHIN
** \brief Redirects to the appropriate function based on input.
** \param c The inputed character that will be checked.
** \return Returns 1 if the input was a CTRL-*, 0 if not.
*/
int ctrl_modifier(char c);

/**
** \fn void curs_right(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Moves the cursor to the right, wrapping around if necessary.
*/
void curs_right(void);

/**
** \fn void curs_left(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Moves the cursor to the left, wrapping around if necessary.
*/
void curs_left(void);

/**
** \fn int my_putchar(int ch)
** \author Tanguy DUPONT DE DINECHIN
** \brief Writes the character to standard output, required by tputs.
** \param ch The character to write.
** \return Returns the result of the write operation.
*/
int my_putchar(int ch);

/**
** \fn void cursor_to_start(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Sets the cursor behind the prompt and updates g_term.
*/
void cursor_to_start(void);

/**
** \fn void cursor_to_prompt(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Sets the cursor after the prompt and updates g_term.
*/
void cursor_to_prompt(void);

/**
** \fn void display_and_position_cursor(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Draw the prompt, the string and the cursor according to g_term.
*/
void display_and_position_cursor(void);

/**
** \fn void opti_display_and_position_cursor(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Draws the string and the cursor according to g_term.
*/
void opti_display_and_position_cursor(void);

/**
** \fn void move_history_and_update(int dir)
** \author Tanguy DUPONT DE DINECHIN
** \brief Move in the history list and updates g_term.
** \param dir 1 to go up in the history list, -1 to go down.
*/
void move_history_and_update(int dir);

/**
** \fn void insert_char(char c)
** \author Tanguy DUPONT DE DINECHIN
** \brief Inserts a new char at the cursor's position and updates g_term.
** \param c The character to insert.
*/
void insert_char(char c);

/**
** \fn void sigint_handler(int num)
** \author Tanguy DUPONT DE DINECHIN
** \brief Clears the input, displays a prompt and updates g_term.
** \param num Required by signal() but unused.
*/
void sigint_handler(int num);

/**
** \fn void sigtstp_handler(int num)
** \author Tanguy DUPONT DE DINECHIN
** \brief Suspends current execution.
** \param num Required by signal() but unused.
*/
void sigtstp_handler(int num);

/**
** \fn void empty_signal(int num)
** \author Tanguy DUPONT DE DINECHIN
** \brief Used to trap signals, does nothing.
** \param num Required by signal() but unused.
*/
void empty_signal(int num);

/**
** \fn void cursor_to_last(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Sets the cursor to the end of the string and updates g_term.
*/
void cursor_to_last(void);

/**
** \fn int get_last_line_len(char *str)
** \author Tanguy DUPONT DE DINECHIN
** \brief Returns the length of the last line of the input.
** \param str The input as a string.
*/
int get_last_line_len(char *str);

/**
** \fn void del_char(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Right deletes at cursor position.
*/
void del_char(void);

/**
** \fn void delete_char(void)
** \author Tanguy DUPONT DE DINECHIN
** \brief Left deletes at cursor position.
*/
void delete_char(void);

/**
** \fn void autocomplete_start(char *str)
** \author Tanguy DUPONT DE DINECHIN
** \brief Attempts to autcomplete the current word you are at in str.
** \param str A shorter way to access the command line typed.
*/
void autocomplete_start(char *str);

/**
** \fn void reset_term(void)
** \brief Called at exit to clean memory.
*/
void reset_term(void);

#endif /* !READLINE_H */
