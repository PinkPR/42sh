#include "check_rules.h"
#include "astprinter.h"
#include "exec.h"
#include "readline.h"
#include "environ.h"
#include "history.h"
#include "main.h"
#include "builtin.h"
#include "jobs.h"
#include "str_tools.h"
#include "expand.h"
#include "fnmatch.h"

struct main g_main;

void free_g_main(void)
{
  if (g_main.ast)
  {
    arg_l_del(g_main.opts);
    arg_l_del(g_main.shopts);
    ast_free(g_main.ast);
    close_history();
    bltn_list_free(g_bltn_list);
    free_envir();
    free(g_main.command);
    g_main.ast = NULL;
  }
}

int build_ast_and_run(char *str, int is_c)
{
  int res = 0;
  if (str)
  {
    init_tokens(&str, INPUT);
    struct ast *ast = check_all();
    g_main.ast = ast;
    free_toks();
    if (ast)
    {
      if (arg_is_set(g_main.opts, "ast-print"))
        ast_print(ast);
      g_continue = 1;
      res = execute_ast(ast);
      if (res == -1)
        res = is_c;
      st_add_var(strdup("?"), itoa(res));
      ast_free(ast);
    }
  }
  return res;
}

static void execute_ressource_files(void)
{
  int fd = open("/etc/42shrc", O_RDONLY);
  if (fd > 0)
  {
    read_and_execute_from_fd(fd);
    close(fd);
  }
  char *home = getenv("HOME");
  char *name = ".42shrc";
  int len1 = strlen(home);
  int len2 = strlen(name);
  char *ret = malloc(sizeof (char) * (len1 + len2 + 2));
  for (int i = 0; i < len1; ++i)
    ret[i] = home[i];
  ret[len1] = '/';
  for (int i = 0; i < len2; ++i)
    ret[len1 + 1 + i] = name[i];
  ret[len1 + len2 + 1] = '\0';
  fd = open(ret, O_RDONLY);
  if (fd > 0)
  {
    read_and_execute_from_fd(fd);
    close(fd);
  }
  free(ret);
}

static void extend_command(s_readl *in)
{
  if (g_main.PS2 == 0)
    g_main.pos = 0;
  g_main.PS2 = 0;
  for (int i = 0; in->command[i]; ++i)
  {
    g_main.command[g_main.pos] = in->command[i];
    g_main.pos++;
    if (g_main.pos >= g_main.max_len)
    {
      g_main.max_len *= 2;
      g_main.command = realloc(g_main.command, g_main.max_len);
    }
  }
  g_main.command[g_main.pos] = '\0';
}

static int check_command_integrity(void)
{
  char current = 0;
  int pos = 0;
  while (g_main.command[pos])
  {
    char c = g_main.command[pos];
    if (c == current && pos > 0 && g_main.command[pos - 1] != '\\')
      current = 0;
    else if (current == 0 && pos > 0 && g_main.command[pos - 1] != '\\')
    {
      if (c == '"' || c == '`' || c == '\'')
        current = c;
    }
    pos++;
  }
  if (g_main.command[pos - 2] == '\\')
  {
    g_main.command[pos - 2] = ' ';
    g_main.pos -= 2;
    return 0;
  }
  if (current != 0)
    return 0;
  return 1;
}

static void execute_tty(void)
{
  g_main.PS2 = 0;
  g_main.pos = 0;
  g_main.max_len = 1024;
  g_main.command = malloc(sizeof (char) * g_main.max_len);
  atexit(reset_term);
  while (1)
  {
    signal(SIGTSTP, SIG_IGN);
    signal(SIGINT, sigint_handler);
    g_main.ast = NULL;
    s_readl *in = readline(g_main.PS2);
    signal(SIGINT, empty_signal);
    signal(SIGTSTP, sigtstp_handler);
    extend_command(in);
    g_history->h_cur = NULL;
    add_to_history(g_main.command);
    char *tmp = g_main.command;
    if (check_command_integrity())
      chop_and_launch(g_main.command);
    else
      g_main.PS2 = 1;
    g_main.command = tmp;
    free(in->command);
    free(in);
  }
}

static void manage_options(void)
{
  if (arg_is_set(g_main.opts, "version"))
  {
    fprintf(stdout, "Version 1.0\n");
    exit(0);
  }
  if (!arg_is_set(g_main.opts, "norc"))
    execute_ressource_files();
}

int main(int argc, char *argv[], char *environ[])
{
  atexit(clear_cat);
  atexit(clear_autocomp);
  init_g_bltn_list();
  shopt_init();
  int res = 0;
  g_main.opts = arg_parse(init_opt("*c O |version |norc |ast-print |histfile"),
      argc, argv);
  init_envir(environ);
  init_history();
  manage_options();
  if (arg_is_set(g_main.opts, "c"))
    res = build_ast_and_run(arg_get(g_main.opts, "c"), 1);
  else
  {
    init_jobs();
    if (isatty(STDIN_FILENO))
      execute_tty();
    else
      res = read_and_execute_from_fd(STDIN_FILENO);
  }
  close_history();
  free_envir();
  arg_l_del(g_main.opts);
  arg_l_del(g_main.shopts);
  return res;
}
