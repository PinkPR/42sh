#include "jobs.h"

static int get_size_temp(char *str)
{
  int i = 0;
  int nb = 2;
  while (str[i])
  {
    if (str[i] == '&')
      nb++;
    i++;
  }
  return nb;
}

static int get_q(char *str, int i, int q)
{
  char c = str[i];
  if (i > 0 && str[i - 1] == '\\')
    return q;
  if (c == '\'')
    q = (q == 0) ? 2 : (q == 2) ? 0 : q;
  if (c == '"')
    q = (q == 0) ? 1 : (q == 1) ? 0 : q;
  return q;
}

static int get_lvl(char c, int q, int lvl)
{
  if (!q && c == '(')
    lvl++;
  if (!q && c == ')')
    lvl--;
  return lvl;
}

static void terminate_and_realloc(char **cmds, int *nb, int *j)
{
  cmds[*nb][(*j)++] = 0;
  cmds[*nb] = realloc(cmds[*nb], sizeof (char) * (*j + 2));
  (*nb)++;
  *j = 0;
}

char **get_cmd(char *str)
{
  char **cmds = malloc(sizeof (char *) * get_size_temp(str));
  int nb = 0;
  int lvl = 0;
  int q = 0;
  int i = 0;
  int j = 0;
  while (1)
  {
    q = get_q(str, i, q);
    lvl = get_lvl(str[i], q, lvl);
    if (!j)
      cmds[nb] = malloc(sizeof (char) * (strlen(str) + 1));
    if ((!lvl && !q && str[i] == '&') || !str[i])
    {
      terminate_and_realloc(cmds, &nb, &j);
      if (!str[i])
        break;
    }
    else
      cmds[nb][j++] = str[i];
    i++;
  }
  cmds[nb] = NULL;
  return cmds;
}

s_job *get_job(pid_t pgid)
{
  s_job *pos = g_control->jobs;
  while (pos)
  {
    if (pos->pgid == pgid)
      return pos;
    pos = pos->next;
  }
  return NULL;
}
