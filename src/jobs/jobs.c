#include "jobs.h"
#include "main.h"
#include "readline.h"

s_control *g_control = NULL;

static void accept_interruptions(int yes)
{
  if (yes)
  {
    signal(SIGINT, SIG_DFL);
    signal(SIGQUIT, SIG_DFL);
    signal(SIGTTIN, SIG_DFL);
    signal(SIGTTOU, SIG_DFL);
  }
  else
  {
    signal(SIGINT, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
  }
}

static void sigchld_handler(int num)
{
  num++;
  do_job_notification();
}

void init_jobs(void)
{
  signal(SIGCHLD, sigchld_handler);
  g_control = malloc(sizeof (s_control));
  g_control->is_tty = isatty(STDIN_FILENO);
  g_control->jobs = NULL;
  g_control->cmds = NULL;
  g_control->can_fork = 1;
  if (g_control->is_tty)
  {
    while (tcgetpgrp(STDIN_FILENO) != (g_control->shell_pgid = getpgrp()))
      kill(g_control->shell_pgid, SIGTTIN);
    accept_interruptions(0);
    g_control->shell_pgid = getpid();
    if (setpgid(g_control->shell_pgid, g_control->shell_pgid) < 0)
      perror("Shell could not be placed in its own process group");
    tcsetpgrp(STDIN_FILENO, g_control->shell_pgid);
    tcgetattr(STDIN_FILENO, &(g_control->shell_tmodes));
  }
}

static void redirect_streams(s_fd fd)
{
  if (fd.in != STDIN_FILENO)
  {
    dup2(fd.in, STDIN_FILENO);
    close(fd.in);
  }
  if (fd.out != STDOUT_FILENO)
  {
    dup2(fd.out, STDOUT_FILENO);
    close(fd.out);
  }
  if (fd.err != STDERR_FILENO)
  {
    dup2(fd.err, STDERR_FILENO);
    close(fd.err);
  }
}

static void launch_process(s_job *job, pid_t pgid, s_fd fd, int fg)
{
  pid_t pid;
  if (g_control->is_tty)
  {
    pid = getpid();
    if (pgid == 0)
      pgid = pid;
    setpgid(pid, pgid);
    if (fg)
      tcsetpgrp(STDIN_FILENO, pgid);
    accept_interruptions(1);
  }
  redirect_streams(fd);
  build_ast_and_run(job->command, 2);
  exit(0);
}

static void finish_launch(s_job *job, int fg)
{
  if (fg == 0)
    format_job_info(job, "launched");
  if (g_control->is_tty == 0)
    wait_job(job);
  else if (fg)
    job_to_foreground(job, 0);
  else
    job_to_background(job, 0);
}

static s_fd create_fd(int in, int out, int err)
{
  s_fd fd;
  fd.in = in;
  fd.out = out;
  fd.err = err;
  return fd;
}

static void cleanup(int in, int out, s_job *job)
{
  if (in != job->in)
    close(in);
  if (out != job->out)
    close(out);
}

void launch_job(s_job *job, int fg)
{
  pid_t pid;
  int mypipe[2];
  int in = job->in;
  int out = job->out;
  pid = fork();
  if (pid == 0)
  {
    g_control->can_fork = 0;
    launch_process(job, job->pgid, create_fd(in, out, job->err), fg);
  }
  else
  {
    job->pid = pid;
    if (g_control->is_tty)
    {
      if (job->pgid == 0)
        job->pgid = pid;
      setpgid(pid, job->pgid);
    }
  }
  cleanup(in, out, job);
  in = mypipe[0];
  finish_launch(job, fg);
}
