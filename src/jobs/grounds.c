#include "jobs.h"
#include "str_tools.h"
#include "environ.h"
#include "main.h"

static int job_still_listed(s_job *job)
{
  if (g_control->jobs == job)
    return 1;
  else
  {
    s_job *pos = g_control->jobs;
    while (pos->next != job)
      pos = pos->next;
    if (pos->next == NULL)
      return 0;
    return 1;
  }
  return 0;
}

void job_to_foreground(s_job *job, int cont)
{
  tcsetpgrp(STDIN_FILENO, job->pgid);
  if (cont && job->stopped)
  {
    job->stopped = 0;
    job->completed = 0;
    job->notified = 0;
    tcsetattr(STDIN_FILENO, TCSADRAIN, &(job->tmodes));
    if (kill(job->pgid, SIGCONT) < 0)
      perror("Error sending SIGCONT in job_to_foreground()");
  }
  do_job_notification();
  int status;
  pid_t pid = 0;
  while (pid == 0 && job->stopped == 0)
    pid = waitpid(job->pid, &status, WNOHANG | WSTOPPED);
  if (job_still_listed(job))
  {
    job->stopped = 1;
    tcgetattr(STDIN_FILENO, &(job->tmodes));
  }
  do_job_notification();
  tcsetpgrp(STDIN_FILENO, g_control->shell_pgid);
  tcsetattr(STDIN_FILENO, TCSADRAIN, &(g_control->shell_tmodes));
}

void job_to_background(s_job *job, int cont)
{
  st_add_var(strdup("!"), itoa(job->pid));
  if (cont)
  {
    if (kill(job->pgid, SIGCONT) < 0)
      perror("Error sending SIGCONT in job_to_background()");
    format_job_info(job, "continued");
    job->stopped = 0;
    job->completed = 0;
  }
  do_job_notification();
}

int check_jobs(void)
{
  s_job *job = g_control->jobs;
  while (job)
  {
    if (job->stopped == 1)
    {
      printf("%d is still suspended, CTRL-D to exit forcibly\n", job->pid);
      return 1;
    }
    printf("%d is still running, CTRL-D to exit forcibly\n", job->pid);
    return 1;
  }
  return 0;
}
