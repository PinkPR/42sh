#include "jobs.h"
#include "main.h"

void free_job(s_job *job)
{
  if (g_control->jobs == job)
    g_control->jobs = job->next;
  else
  {
    s_job *pos = g_control->jobs;
    while (pos->next != job)
      pos = pos->next;
    pos->next = job->next;
  }
  free(job->command);
  free(job);
}

void continue_job(s_job *job, int foreground)
{
  job->stopped = 0;
  job->notified = 0;
  if (foreground)
    job_to_foreground(job, 1);
  else
    job_to_background(job, 1);
}

static void add_to_job_list(s_job *job)
{
  job->next = NULL;
  job->notified = 0;
  job->pgid = 0;
  job->pid = 0;
  job->in = 0;
  job->out = 1;
  job->err = 2;
  job->status = 0;
  job->completed = 0;
  job->stopped = 0;
  if (g_control->jobs == NULL)
    g_control->jobs = job;
  else
  {
    s_job *pos = g_control->jobs;
    while (pos->next)
      pos = pos->next;
    pos->next = job;
  }
}

void add_new_job(int pid, char *command)
{
  s_job *job = malloc(sizeof (s_job));
  job->command = strdup(command);
  job->next = NULL;
  job->notified = 1;
  job->pgid = pid;
  job->pid = pid;
  job->in = 0;
  job->out = 1;
  job->err = 2;
  job->status = 0;
  job->completed = 0;
  job->stopped = 1;
  tcgetattr(STDIN_FILENO, &(job->tmodes));
  if (g_control->jobs == NULL)
    g_control->jobs = job;
  else
  {
    s_job *pos = g_control->jobs;
    while (pos->next)
      pos = pos->next;
    pos->next = job;
  }
}

static void launch_job_main(char *command, int fg)
{
  if (fg)
    build_ast_and_run(command, 2);
  else
  {
    int len = strlen(command);
    s_job *job = malloc(sizeof (s_job));
    job->command = malloc(sizeof (char) * (len + 1));
    memcpy(job->command, command, sizeof (char) * (len + 1));
    add_to_job_list(job);
    launch_job(job, 0);
  }
}

void free_cmds(void)
{
  if (g_control->cmds)
  {
    for (int i = 0; g_control->cmds[i]; ++i)
      free(g_control->cmds[i]);
    free(g_control->cmds);
    g_control->cmds = NULL;
  }
}

void chop_and_launch(char *str)
{
  g_control->cmds = get_cmd(str);
  for (int i = 0; g_control->cmds[i]; ++i)
  {
    if (g_control->cmds[i + 1])
      launch_job_main(g_control->cmds[i], 0);
    else
      launch_job_main(g_control->cmds[i], 1);
  }
  free_cmds();
}
