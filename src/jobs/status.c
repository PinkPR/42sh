#include "jobs.h"

static int mark_process_status(pid_t pid, int status)
{
  s_job *job;
  if (pid > 0)
  {
    for (job = g_control->jobs; job; job = job->next)
      if (job->pid == pid)
      {
        job->status = status;
        if (WIFSTOPPED(status))
          job->stopped = 1;
        if (WIFEXITED(status))
        {
          job->completed = 1;
          int d = WTERMSIG(job->status);
          if (WIFSIGNALED(status))
            fprintf(stderr, "%d: Terminated by signal %d.\n", pid, d);
        }
        return 0;
      }
    return -1;
  }
  else
    return -1;
}

static pid_t wait_job_pid(int *status)
{
  s_job *job;
  for (job = g_control->jobs; job; job = job->next)
  {
    pid_t pid = waitpid(job->pid, status, WUNTRACED | WNOHANG);
    if (pid > 0)
      return pid;
  }
  return 0;
}

static void update_status(void)
{
  int status;
  pid_t pid = wait_job_pid(&status);
  while (!mark_process_status(pid, status))
    pid = wait_job_pid(&status);
}

static int job_stopped_or_completed(s_job *job)
{
  return job->completed || job->stopped;
}

void wait_job(s_job *job)
{
  int status;
  pid_t pid = waitpid(WAIT_ANY, &status, WUNTRACED | WSTOPPED);
  while (!mark_process_status(pid, status) && !job_stopped_or_completed(job))
    pid = waitpid(WAIT_ANY, &status, WUNTRACED | WSTOPPED);
}

void format_job_info(s_job *job, const char *status)
{
  long int ld = job->pgid;
  if (job->command[strlen(job->command) - 1] != '\n')
    fprintf(stderr, "%ld (%s): %s\n", ld, status, job->command);
  else
    fprintf(stderr, "%ld (%s): %s", ld, status, job->command);
}

void do_job_notification(void)
{
  s_job *job;
  s_job *next_job;
  update_status();
  for (job = g_control->jobs; job; job = next_job)
  {
    next_job = job->next;
    if (job->completed || kill(job->pid, 0) == -1)
    {
      format_job_info(job, "completed");
      free_job(job);
    }
  }
}
