#include "fnmatch.h"
#include <sys/types.h>
#include <dirent.h>
#include "path.h"
#include "expand.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

s_path_l *g_patharray = NULL;

void clear_autocomp(void)
{
  s_path_l *pos = g_patharray;
  while (pos)
  {
    s_path_l *next = pos->next;
    free(pos->path);
    free(pos);
    pos = next;
  }
  g_patharray = NULL;
}

static void add_path(char *path)
{
  s_path_l *elt = malloc(sizeof (s_path_l));
  if (elt == 0)
    return;
  elt->path = path;
  elt->next = g_patharray;
  g_patharray = elt;
}

static void list_dir(DIR *d, char *dir, char *path, int i)
{
  int end = path[i] == '\0';
  path[i] = '\0';
  while (1)
  {
    struct dirent *entry;

    entry = readdir(d);
    if (!entry)
      break;
    if ((my_fnmatch(path, entry->d_name) == 0))
    {
      char *newpath =
        dir[0] ? custom_cat(custom_cat(dir, "/"), entry->d_name) :
        custom_cat(".", entry->d_name);
      if (end)
        add_path(strdup(newpath));
      else
      {
        if (newpath[0] == '.')
          path_expand_r(newpath + 1, path + i + 1);
        else
          path_expand_r(newpath, path + i + 1);
      }
    }
  }
}

void path_expand_r(char *dir, char *path)
{
  DIR *d;
  int i = 0;
  while (path[i] != '\0' && (path[i] != '/'
        || (path[i] == '/' && path[i + 1] == '/')))
    ++i;
  d = opendir(dir[0] ? dir : ".");
  if (!d)
    return;
  list_dir(d, dir, path, i);
  closedir(d);
}

void path_expand(char *path)
{
  clear_autocomp();
  if (path[0] == '/')
    path_expand_r("/", path + 1);
  else if (path[0] == '.' && path[1] == '/')
    path_expand_r("./", path + 2);
  else
    path_expand_r("", path);
}

void autocomplete(char *str)
{
  char *pattern = custom_cat(str, "*");
  path_expand(pattern);
}
