#include <string.h>
#include "eval.h"
#include "expand.h"
#include "str_tools.h"

static char *get_eval(char *newstr, char *str, int *i, int *j)
{
  int a = 0;
  int b = 0;
  while ((str[*i + a - 1] != ')' || str[*i + a] != ')') && str[*i + a])
    a++;
  if (str[*i + a - 1] != ')')
    a--;
  str[*i + a - 1] = 0;
  int nb = evalexpr(str + *i);
  char *ins = itoa(nb);
  while (ins[b])
  {
    newstr[*j] = ins[b];
    (*j)++;
    b++;
  }
  free(ins);
  str[*i + a - 1] = ')';
  (*i) += a;
  return newstr;
}


char *expand_arith(char *str)
{
  char *newstr = malloc(sizeof (char) * (strlen(str) + 1));
  int i = 0;
  int j = 0;
  while (str[i])
  {
    if (str[i] == '$' && str[i + 1] == '(' && str[i + 2] == '(')
    {
      i += 3;
      newstr = get_eval(newstr, str, &i, &j);
    }
    else
      newstr[j++] = str[i];
    i++;
  }
  free(str);
  newstr[j] = 0;
  return newstr;
}
