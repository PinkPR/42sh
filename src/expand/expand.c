#include <string.h>
#include <time.h>
#include "str_tools.h"
#include "expand.h"
#include "environ.h"

static int is_special(char c)
{
  return c == '?' || c == '#' || c == '$' || c == '@' || c == '*' || c == '!';
}

static int is_delim(char c)
{
  return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_'
          || (c >= '0' && c <= '9') || c == '~'
          || c == '{' || c == '}';
}

static char *get_varname(char *str, int *ind)
{
  char *name = malloc(sizeof (char) * (strlen(str) + 1));
  int i = 0;
  int j = 0;
  if (is_special(str[i]))
    name[j++] = str[i++];
  else
  {
    if (str[i] == '{')
      i++;
    while (is_delim(str[i]) && str[i] != '}')
    {
      name[j++] = str[i];
      i++;
    }
    if (str[i] == '}')
      i++;
  }
  name[j++] = 0;
  name = realloc(name, sizeof (char) * j);
  *ind += i;
  return name;
}

char *insert_str(char *str, int *ind, char *ins, int *size)
{
  if (!ins)
    return str;
  int len = strlen(ins);
  int i = 0;
  int a = 0;
  *size += strlen(ins);
  str = realloc(str, sizeof (char) * (*size));
  while (a < len)
    str[*ind + i++] = ins[a++];
  *ind += len;
  return str;
}

static int get_expand(char *str, int i, int *expand)
{
  if (*expand >= 0 && str[i] == '"')
  {
    *expand = (*expand == 2) ? 1 : 2;
    return 0;
  }
  else if (*expand <= 1 && str[i] == '\'')
  {
    *expand *= -1;
    return 0;
  }
  return 1;
}

static int clean_spaces(char *str, int i)
{
  while (str[i] && str[i] == ' ')
    i++;
  return i;
}


static void set_random(char *str)
{
  struct timespec sp;
  clock_gettime(CLOCK_MONOTONIC, &sp);
  srand(sp.tv_nsec);
  st_add_var(strdup("RANDOM"), itoa(rand() % (32767)));
  free(str);
}

static char *get_var_clean(char *name, int expand)
{
  char *var = get_var(name);
  if (!var)
    return NULL;
  int j = strlen(var) - 1;
  while (var[j] == ' ' && expand == 1)
    j--;
  var[++j] = 0;
  return var;
}

char *expand_variables(char *str)
{
  int len = strlen(str);
  char *newstr = malloc(sizeof (char) * (len + 1));
  int i = 0;
  int j = 0;
  int expand = 1;
  while (str[i])
  {
    if (j == 0)
      i = clean_spaces(str, i);
    if (!get_expand(str, i, &expand))
      expand += 0;
    else if (expand >= 0 && str[i] == '$'
             && (is_delim(str[i + 1]) || is_special(str[i + 1])))
    {
      char *name = get_varname(str + i + 1, &i);
      newstr = insert_str(newstr, &j, get_var_clean(name, expand), &len);
      free(name);
    }
    else
      newstr[j++] = str[i];
    i++;
  }
  newstr[j] = 0;
  set_random(str);
  return newstr;
}

char *expand(char *str)
{
  return (expand_arith(
      expand_variables(expand_tildes(expand_backslash(
            expand_subshell(strdup(str)))))));
}
