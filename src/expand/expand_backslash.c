#include <string.h>
#include "str_tools.h"
#include "expand.h"

static int get_expand(char *str, int i, int *expand)
{
  if (*expand >= 0 && str[i] == '"')
  {
    *expand = (*expand == 2) ? 1 : 2;
    return 0;
  }
  else if (*expand <= 1 && str[i] == '\'')
  {
    *expand *= -1;
    return 0;
  }
  return 1;
}

static int get_temp_size(char *str)
{
  int len = strlen(str);
  int i = 0;
  while (str[i])
  {
    if (str[i] == '\\')
      len++;
    i++;
  }
  return len;
}

char *expand_backslash(char *str)
{
  int len = get_temp_size(str);
  char *newstr = malloc(sizeof (char) * (len + 1));
  int i = 0;
  int j = 0;
  int expand = 0;
  while (str[i])
  {
    if (!get_expand(str, i, &expand))
      expand += 0;
    if (expand == 0 && str[i] == '\\')
    {
      char c = '\'';
      if (str[i + 1] == c)
        c = '"';
      newstr[j++] = c;
      newstr[j++] = str[++i];
      newstr[j++] = c;
      i++;
    }
    else
      newstr[j++] = str[i++];
  }
  newstr[j] = 0;
  free(str);
  return newstr;
}

