#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "expand.h"
#include "history.h"
#include "builtin.h"
#include "main.h"

static char *getn(char *str, int *i, char end)
{
  int count = 1;
  *i += 1;
  if (end == ')')
    *i += 1;
  char *newstr = strdup(str + *i);
  int j = 0;
  while (newstr[j])
  {
    if (newstr[j] == '(')
      count++;
    if (newstr[j] == end && (j == 0 || newstr[j - 1] != '\\'))
    {
      count--;
      if (count == 0)
        break;
    }
    (*i)++;
    j++;
  }
  newstr[j] = 0;
  return newstr;
}

static void close_link(int *link)
{
  close(link[0]);
  close(link[1]);
}

static void remove_eol(char *buffer)
{
  int i = 0;
  while (buffer[i])
  {
    if (buffer[i] == '\n')
      buffer[i] = ' ';
    i++;
  }
  buffer[i - 1] = 0;
}


static char *getss(char *command)
{
  int link[2];
  pipe(link);
  int pid = fork();
  if (pid == 0)
  {
    dup2(link[1], STDOUT_FILENO);
    ast_free(g_main.ast);
    int ret = build_ast_and_run(command, 2);
    free(command);
    close(link[1]);
    close(link[0]);
    exit(ret);
  }
  char *buffer = malloc(sizeof (char) * 4096);
  int count = read(link[0], buffer, 4096);
  buffer = realloc(buffer, sizeof (char) * (count + 1));
  buffer[count] = 0;
  remove_eol(buffer);
  close_link(link);
  free(command);
  return buffer;
}

char *expand_subshell(char *str)
{
  int len = strlen(str) + 1;
  char *newstr = malloc(sizeof (char) * len);
  int i = 0;
  int j = 0;
  while (str[i])
  {
    if (str[i] == '$' && str[i + 1] == '(' && str[i + 2] != '('
        && (i == 0 || str[i - 1] != '\\'))
      newstr = insert_str(newstr, &j, getss(getn(str, &i, ')')), &len);
    else if (str[i] == '`' && (i == 0 || str[i - 1] != '\\'))
      newstr = insert_str(newstr, &j, getss(getn(str, &i, '`')), &len);
    else
      newstr[j++] = str[i];
    i++;
  }
  free(str);
  newstr[j] = 0;
  return newstr;
}
