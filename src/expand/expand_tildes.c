#include <string.h>
#include "expand.h"
#include "environ.h"

static int is_special(char c)
{
  return c == '?' || c == '#' || c == '$' || c == '@' || c == '*' || c == '!';
}

static int is_delim(char c)
{
  return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_'
          || (c >= '0' && c <= '9') || c == '~' || is_special(c));
}

static int mystrcmp(char *str, int ind, char *pat)
{
  int i = 0;
  while (pat[i])
  {
    if (!str[ind + i] || str[ind + i] != pat[i])
      return 0;
    i++;
  }
  return (ind == 0 || !is_delim(str[ind - 1])) && !is_delim(str[ind + i]);
}

char *replace(char *str, char *replace, char *ins)
{
  int nb = strlen(ins);
  char *newstr = malloc(sizeof (char) * (strlen(str) + nb));
  int i = 0;
  int j = 0;
  int a = 0;
  int exp = 1;
  while (str[i])
  {
    if (str[i] == '\'')
      exp *= -1;
    if (exp >= 1 && !a && mystrcmp(str, i, replace))
    {
      i += strlen(replace) - 1;
      while (ins[a])
        newstr[j++] = ins[a++];
    }
    else
      newstr[j++] = str[i];
    if (str[i] == replace[a])
      a++;
    i++;
  }
  newstr[j] = 0;
  return newstr;
}

char *expand_tildes(char *str)
{
  char *newstr = replace(str, "~-", "$OLDPWD");
  char *newstr2 = replace(newstr, "~+", "$PWD");
  free(newstr);
  char *newstr3 = replace(newstr2, "~", "$HOME");
  free(newstr2);
  free(str);
  return newstr3;
}
